from django.db.models import Q
from rest_framework.response import Response
from general_view_set import GeneralModelViewSet
from .models import *
from .serializers import *


##############################       Modelos Basicos    ################################################
# UnidadMedidaViewSet
# clase encarga de generar la vista del modelo UnidadMedida
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class UnidadMedidaViewSet(GeneralModelViewSet):
    model_db = UnidadMedida
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = UnidadMedida.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = UnidadMedidaSerializer
    seccion = 'Unidades Medidas'


# TipoUsoViewSet
# clase encarga de generar la vista del modelo TipoUso
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class TipoUsoViewSet(GeneralModelViewSet):
    model_db = TipoUso
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = TipoUso.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = TipoUsoSerializer
    seccion = 'Tipos Usos'


# MicroCuencaViewSet
# clase encarga de generar la vista del modelo Microcuenca
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class MicroCuencaViewSet(GeneralModelViewSet):
    model_db = MicroCuenca
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = MicroCuenca.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = MicroCuentaSerializer
    seccion = 'Microcuencas'


# TipoVariableViewSet
# clase encarga de generar la vista del modelo TipoVariable
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class TipoVariableViewSet(GeneralModelViewSet):
    model_db = TipoVariable
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = TipoVariable.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = TipoVariableSerializer
    seccion = 'Tipos Variables'


# TecnologiaEstacionViewSet
# clase encarga de generar la vista del modelo Entidad
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class TecnologiaEstacionViewSet(GeneralModelViewSet):
    model_db = TecnologiaEstacion
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = TecnologiaEstacion.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = TecnologiaEstacionSerializer
    seccion = 'Tecnología Estaciones'


# CategoriaEstacionViewSet
# clase encarga de generar la vista del modelo CategoriaEstacion
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class CategoriaEstacionViewSet(GeneralModelViewSet):
    model_db = CategoriaEstacion
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = CategoriaEstacion.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = CategoriaEstacionSerializer
    seccion = 'Categoría Estaciones'


# PuntoGestionViewSet
# clase encarga de generar la vista del modelo entidades
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class PuntoGestionViewSet(GeneralModelViewSet):
    model_db = PuntoGestion
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = PuntoGestion.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = PuntoGestionSerializer
    seccion = 'Puntos Gestión'

    def listarTodosPuntosGestion(self):
        puntosGestion = PuntoGestion.objects.all()
        return puntosGestion

    def listarPuntoGestionPorId(self, id):
        puntoGestion = PuntoGestion.objects.get(id=id)
        return puntoGestion


# ModeloViewSet
# clase encarga de generar la vista del modelo Modelo
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class ModeloViewSet(GeneralModelViewSet):
    model_db = Modelo
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Modelo.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = ModeloSerializer
    seccion = 'Modelos'


# CategoriaSerieViewSet
# clase encarga de generar la vista del modelo CategoriaSerie
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class CategoriaSerieViewSet(GeneralModelViewSet):
    model_db = CategoriaSerie
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = CategoriaSerie.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = CategoriaSerieSerializer
    seccion = 'Categoría Series'


##############################     Modelos Maestro Detalle  ################################################
# LimiteViewSet
# clase encarga de generar la vista del modelo Limite
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class LimiteViewSet(GeneralModelViewSet):
    model_db = Limite
    detail_rows_db = False  # Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Limite.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = LimiteSerializer
    seccion = 'Limites'


# VariableViewSet
# clase encarga de generar la vista del modelo Variable
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class VariableViewSet(GeneralModelViewSet):
    model_db = Variable
    detail_rows_db = False  # Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Variable.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = VariableSerializer
    seccion = 'Variables'

    def listarVariables(self):
        queryset = Variable.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return serializer

    def buscarVariableporId(self, id):
        queryset = Variable.objects.get(id=id)
        return queryset.nombre

    def listarVariablesPotenciales(self):
        variables = Variable.objects.filter(Q(nombre="PR") | Q(nombre="PS") | Q(nombre="PWd"))
        serializerVariable = VariableSerializer(variables, many=True)
        return serializerVariable


class EstacionViewSet(GeneralModelViewSet):
    model_db = Estacion
    detail_rows_db = False  # Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Estacion.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = EstacionSerializer
    seccion = 'Estaciones'


# VariableReglaOperacionViewSet
# clase encarga de generar la vista del modelo VariableReglaoperacion
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class VariableReglaOperacionViewSet(GeneralModelViewSet):
    model_db = VariableReglaOperacion
    detail_rows_db = True  # Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = VariableReglaOperacion.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = VariableReglaOperacionSerializer
    seccion = 'Variables Reglas Operacion'

    def filterObjects(self, data):
        if (((self.model_db is None) or (not data)) and self.detail_rows_db is False):
            return self.model_db.objects.all()
        elif (self.detail_rows_db and (not data)):
            return []
        else:
            params = self.model_db.get_Filter(data)
            return self.model_db.objects.filter(**params)

    def listVariable(self, data):
        queryset = self.filterObjects(data)
        serializer = self.serializer_class(queryset, many=True)
        return serializer.data


class SeccionViewSet(GeneralModelViewSet):
    model_db = Seccion
    detail_rows_db = False  # Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Seccion.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = SeccionSerializer
    seccion = 'Secciones'


class MunicipioViewSet(GeneralModelViewSet):
    model_db = Municipio
    detail_rows_db = False  # Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Municipio.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = MunicipioSerializer
    seccion = 'Municipios'

    def list(self, request):
        queryset = Municipio.objects.all()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


# TipoCaudalViewSet
# clase encarga de generar la vista del modelo TipoCaudal
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class TipoCaudalViewSet(GeneralModelViewSet):
    model_db = TipoCaudal
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = TipoCaudal.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo TipoCaudal ordenado por nombre
    serializer_class = TipoCaudalSerializer
    seccion = 'Tipos Caudal'


# EstadoViewSet
# clase encarga de generar la vista del modelo Estado
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class EstadoViewSet(GeneralModelViewSet):
    model_db = Estado
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Estado.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo Estado ordenado por nombre
    serializer_class = EstadoSerializer
    seccion = 'Estados'

    def list(self):
        queryset = self.queryset
        serializer = self.serializer_class(queryset, many=True)
        return serializer.data

    def buscarNombreEstado(self, id):
        queryset = self.model_db.objects.filter(id=id)
        return queryset.nombre


# EntidadViewSet
# clase encarga de generar la vista del modelo entidades
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class EntidadViewSet(GeneralModelViewSet):
    model_db = Entidad
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Entidad.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = EntidadSerializer
    seccion = 'Entidades'
