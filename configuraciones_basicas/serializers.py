from .models import *
from rest_framework import serializers
##############################       Modelos Basicos    ################################################

# UnidadMedidaSerializer
# clase encargarda de serializar el modelo UnidadMedida
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class UnidadMedidaSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnidadMedida
        fields = UnidadMedida.get_Atributos()

# TipoUsoSerializer
# clase encargarda de serializar el modelo TipoUso
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class TipoUsoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoUso
        fields = TipoUso.get_Atributos()

# MicroCuencaSerializer
# clase encargarda de serializar el modelo Microcuencas
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class MicroCuentaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MicroCuenca
        fields = MicroCuenca.get_Atributos()

# TipoVariableSerializer
# clase encargarda de serializar el modelo TipoVariable
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class TipoVariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoVariable
        fields = TipoVariable.get_Atributos()

# TecnologiaEstacionSerializer
# clase encargarda de serializar el modelo TecnologiaEstacion
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class TecnologiaEstacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TecnologiaEstacion
        fields = TecnologiaEstacion.get_Atributos()

# CategoriaEstacionSerializer
# clase encargarda de serializar el modelo CategoriaEstacion
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class CategoriaEstacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriaEstacion
        fields = CategoriaEstacion.get_Atributos()

# DepartamentoSerializer
# clase encargarda de serializar el modelo Departamento
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class DepartamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departamento
        fields = Departamento.get_Atributos()

# PuntoGestionSerializer
# clase encargarda de serializar el modelo PuntoGestion
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class PuntoGestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PuntoGestion
        fields = PuntoGestion.get_Atributos()

# ModeloSerializer
# clase encargarda de serializar el modelo Modelo
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class ModeloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Modelo
        fields = Modelo.get_Atributos()

# CategoriaSerieSerializer
# clase encargarda de serializar el modelo CategoriaSerie
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class CategoriaSerieSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriaSerie
        fields = CategoriaSerie.get_Atributos()


##############################     Modelos Maestro Detalle  ################################################
# LimiteSerializer
# clase encargarda de serializar el modelo Limite
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class LimiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Limite
        fields = Limite.get_Atributos()

# VariableSerializer
# clase encargarda de serializar el modelo Variable
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variable
        fields = Variable.get_Atributos()

# EstacionSerializer
# clase encargarda de serializar el modelo Estacion
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class EstacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estacion
        fields = Estacion.get_Atributos()

# VariableReglaOperacion
# clase encargarda de serializar el modelo VariableReglaOperacion
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class VariableReglaOperacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VariableReglaOperacion
        fields = VariableReglaOperacion.get_Atributos()

# SeccionSerializer
# clase encargarda de serializar el modelo Seccion
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class SeccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seccion
        fields = Seccion.get_Atributos()

# MunicipioSerializer
# clase encargarda de serializar el modelo Municipio
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class MunicipioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Municipio
        fields = Municipio.get_Atributos()

# TipoCaudalSerializer
# clase encargarda de serializar el modelo TipoCaudal
# @Federico Estupiñan  @date 07/02/2020
class TipoCaudalSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoCaudal
        fields = TipoCaudal.get_Atributos()

# EstadoSerializer
# clase encargarda de serializar el modelo Estado
# @Federico Estupiñan  @date 07/02/2020
class EstadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estado
        fields = Estado.get_Atributos()


# TrimestreMediaMovilSerializer
# clase encargarda de serializar el modelo TrimestreMediaMovil
# @Mauricio Fonseca  @date 09/03/2020
class TrimestreMediaMovilSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrimestreMediaMovil
        fields = TrimestreMediaMovil.get_Atributos()

# EntidadSerializer
# clase encargarda de serializar el modelo Entidad
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class EntidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entidad
        fields = Entidad.get_Atributos()

