from django.core.validators import MaxValueValidator
from django.db import models
from rest_framework_jwt.settings import api_settings

JWT_PAYLOAD_HANDLER= api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER= api_settings.JWT_ENCODE_HANDLER
##############################       Modelos Basicos    ################################################

# Entidad
# clase encargada de definir las propiedades de la tabla entidades en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 30/01/2020
class Entidad(models.Model):
    id = models.AutoField(primary_key=True)  # @param primarykey id
    nombre = models.CharField(max_length=200, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'entidades' # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():           #@def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  #@return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data

# UnidadMedida
# clase encargada de definir las propiedades de la tabla unidad_medida en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class UnidadMedida(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=200, blank=False, null=False)  # @param string nombre
    simbolo = models.CharField(max_length=10, blank=False, null=False)  # @param string simbolo

    class Meta:
        db_table = 'unidad_medida'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre', 'simbolo'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# TipoUso
# clase encargada de definir las propiedades de la tabla tipos_usos en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class TipoUso(models.Model):
    id = models.AutoField(primary_key=True)  # @ auto increment id
    nombre = models.CharField(max_length=255, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'tipos_usos'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {}
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# MicroCuenca
# clase encargada de definir las propiedades de la tabla microcuencas en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class MicroCuenca(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=150, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'microcuencas'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# TipoVariable
# clase encargada de definir las propiedades de la tabla tipo_variable en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 30/01/2020
class TipoVariable(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=150, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'tipo_variable'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# TecnologiaEstacion
# clase encargada de definir las propiedades de la tabla Tecnologia_Estaciones en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class TecnologiaEstacion(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=50, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'tecnologia_estaciones'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# CategoraEstacion
# clase encargada de definir las propiedades de la tabla estaciones la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class CategoriaEstacion(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=50, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'categoria_estaciones'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# Departamento
# clase encargada de definir las propiedades de la tabla departamentos en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class Departamento(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=200, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'departamentos'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# PuntoGestion
# clase encargada de definir las propiedades de la tabla puntos_gestion en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class PuntoGestion(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=200, blank=False, null=False)  # @param string nombre
    latitud = models.CharField(max_length=50, blank=False, null=False)  # @param string latitud
    longitud = models.CharField(max_length=50, blank=False, null=False)  # @param string longitud
    altitud = models.CharField(max_length=50, blank=False, null=False)  # @param string altitud
    descripcion = models.CharField(max_length=500, blank=True)  # @param string descripcion

    class Meta:
        db_table = 'puntos_gestion'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre', 'latitud', 'longitud', 'altitud',
                 'descripcion'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# Modelos
# clase encargada de definir las propiedades de la tabla modelos en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class Modelo(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=100, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'modelos'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# CategorioSerie
# clase encargada de definir las propiedades de la tabla categorias_serie en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class CategoriaSerie(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=150, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'categorias_serie'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data

##############################     Modelos Maestro Detalle  ################################################

# Variable
# clase encargada de definir las propiedades de la tabla variables en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class Variable(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=200, blank=False, null=False)  # @param string nombre
    constante = models.IntegerField(validators=[MaxValueValidator(1)], blank=False,
                                    null=False)  # @param integer constante
    calculada = models.IntegerField(validators=[MaxValueValidator(1)], blank=False,
                                    null=False)  # @param integer calculada
    valor_defecto = models.CharField(max_length=50, blank=False, null=False)  # @param string nombre
    id_unidad_medida = models.ForeignKey(UnidadMedida, to_field='id', db_column='id_unidad_medida',
                                         on_delete=models.DO_NOTHING)  # @param foreignkey id_unidad_medida
    id_tipo_variable = models.ForeignKey(TipoVariable, to_field='id', db_column='id_tipo_variable',
                                         on_delete=models.DO_NOTHING)  # @param foreignkey id_tipo_variable

    class Meta:
        db_table = 'variables'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre', 'constante', 'calculada', 'valor_defecto', 'id_unidad_medida',
                 'id_tipo_variable'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# Municipio
# clase encargada de definir las propiedades de la tabla municipios en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class Municipio(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=200, blank=False, null=False)  # @param string nombre
    id_departamento = models.ForeignKey(Departamento, to_field='id', db_column='id_departamento',
                                        on_delete=models.DO_NOTHING)  # @param foreignkey id_departamento

    class Meta:
        db_table = 'municipios'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (
            ['id', 'nombre', 'id_departamento'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# Estacion
# clase encargada de definir las propiedades de la tabla estaciones la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 31/01/2020
class Estacion(models.Model):
    id = models.AutoField(primary_key=True)  # @param integer id
    codigo = models.CharField(max_length=20, blank=False, null=False)  # @param string codigo
    nombre = models.CharField(max_length=250, blank=False, null=False)  # @param string nombre
    latitud = models.CharField(max_length=50, blank=False, null=False)  # @param string latitud
    longitud = models.CharField(max_length=50, blank=False, null=False)  # @param string longitud
    altitud = models.CharField(max_length=50, blank=False, null=False)  # @param string altitud
    estado = models.IntegerField(validators=[MaxValueValidator(1)], blank=False, null=False)  # @param integer estado
    fecha_instalacion = models.DateField(blank=False, null=False)  # @param date fecha_instalacion
    fecha_suspension = models.DateField(blank=False, null=False)  # @param date fecha_suspencion
    fecha_inicial_datos = models.DateField(auto_now_add=True)  # @param date fecha_inicial_datos
    fecha_final_datos = models.DateField(auto_now=True)  # @param date fecha_final_datos
    id_entidad = models.ForeignKey(Entidad, to_field='id', db_column='id_entidad',
                                   on_delete=models.DO_NOTHING)  # @param foreignkey id_entidad
    id_categoria_estaciones = models.ForeignKey(CategoriaEstacion, to_field='id', db_column='id_categoria_estaciones',
                                                on_delete=models.DO_NOTHING)  # @param foreignkey id_categoria_estaciones
    id_tecnologia = models.ForeignKey(TecnologiaEstacion, to_field='id', db_column='id_tecnologia',
                                      on_delete=models.DO_NOTHING)  # @param foreignkey id_tecnologia
    id_municipio = models.ForeignKey(Municipio, to_field='id', db_column='id_municipio',
                                     on_delete=models.DO_NOTHING)  # @param foreignkey id_municipio

    class Meta:
        db_table = 'estaciones'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'codigo', 'nombre', 'latitud', 'longitud', 'altitud', 'estado', 'fecha_instalacion',
                 'fecha_suspension', 'fecha_inicial_datos', 'fecha_final_datos',
                 'id_entidad', 'id_categoria_estaciones',
                 'id_tecnologia',
                 'id_municipio'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# VariableReglaOperacion
# clase encargada de definir las propiedades de la tabla variables_reglas_operacion en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class VariableReglaOperacion(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    id_variable = models.ForeignKey(Variable, to_field='id', db_column='id_variable',
                                    on_delete=models.DO_NOTHING)  # @param foreignkey id_variable
    id_punto_gestion = models.ForeignKey(PuntoGestion, to_field='id', db_column='id_punto_gestion',
                                         on_delete=models.DO_NOTHING)  # @aram foreignkey id_punto_gestion

    class Meta:
        db_table = 'variables_reglas_operacion'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'id_variable',
                 'id_punto_gestion'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        datosTabla = VariableReglaOperacion.get_Atributos()
        data = {};
        for param in params:
            if param in datosTabla:
                if param == 'id_punto_gestion':
                    data[param] = params.get(param)
                else:
                    data[param + '__contains'] = params.get(param)
        return data


# Seccion
# clase encargada de definir las propiedades de la tabla secciones en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class Seccion(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=100, blank=False, null=False)  # @param string nombre
    id_punto_gestion = models.ForeignKey(PuntoGestion, to_field='id', db_column='id_punto_gestion',
                                         on_delete=models.DO_NOTHING)  # @param foreignkey id_punto_gestion

    class Meta:
        db_table = 'secciones'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre', 'id_punto_gestion'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# Limite
# clase encargada de definir las propiedades de la tabla limites en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 1/02/2020
class Limite(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=100, blank=False, null=False)  # @param string nombre
    valor = models.FloatField(blank=False, null=False)  # @param double valor
    constante = models.IntegerField(validators=[MaxValueValidator(1)], blank=False, null=False)
    id_variable = models.ForeignKey(Variable, to_field='id', db_column='id_variable',
                                    on_delete=models.DO_NOTHING)  # @param foreignkey id_variable
    id_punto_gestion = models.ForeignKey(PuntoGestion, to_field='id', db_column='id_punto_gestion',
                                         on_delete=models.DO_NOTHING)  # @param foreignkey id_punto_gestion
    id_tipo_uso = models.ForeignKey(TipoUso, to_field='id', db_column='id_tipo_uso',
                                    on_delete=models.DO_NOTHING)  # @param foreignkey id_tipo_uso

    class Meta:
        db_table = 'limites_constantes'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre', 'valor', 'constante', 'id_variable', 'id_punto_gestion','id_tipo_uso'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# TipoCaudal
# clase encargada de definir las propiedades de la tabla tipo_caudal en la base de datos
# @Federico Estupiñan  @date 07/02/2020
class TipoCaudal(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=150, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'tipo_caudal'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['nombre']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# Estado
# clase encargada de definir las propiedades de la tabla estados en la base de datos
# @Federico Estupiñan  @date 07/02/2020
class Estado(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=100, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'estados'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros


    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data

class TrimestreMediaMovil(models.Model):
    id = models.AutoField(primary_key=True)  # @param auto increment id
    nombre = models.CharField(max_length=3, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'trimestre_media_movil'
        ordering = ['id']

    def __str__(self):
        return self.nombre

    @staticmethod
    def get_Atributos():
        return (['id', 'nombre'])

    @staticmethod
    def get_Filter(params):
        data = {}
        for params in params:
            data[params + '__contains'] = params.get(params)
        return data




