from django.apps import AppConfig


class ConfiguracionesBasicasConfig(AppConfig):
    name = 'configuraciones_basicas'
