from datetime import datetime, timedelta
from random import random

import numpy
from faker import Faker
fake = Faker()

from configuraciones_basicas.models import CategoriaSerie
from algoritmos.models import *
from mesa_tecnica.models import *
from configuraciones_basicas.models import *
from mesa_tecnica.views import *
from configuraciones_basicas.views import *

def delete_categoriaSerie():
    CategoriaSerie.objects.all().delete()
    print("Datos Eliminados")

def seed_categoriaSerie(num_entries=10):
    count = 0

    for _ in range(num_entries):
        nombre = fake.name()
        Cs = CategoriaSerie(
            nombre = nombre
        )
        Cs.save()
        count += 1
        percent_complete = count/num_entries*100
        print (nombre)
    print()

def seed_SerieClimatica(num_entries, id_mesaTecnica, id_categoriaSerie):
    count = 0
    fecha_inicio = "2015-01-01"
    date_object = datetime.strptime(fecha_inicio, '%Y-%m-%d')
    fecha_fin = date_object + timedelta(days=365)

    mesaTecnica = MesaTecnica.objects.get(id=id_mesaTecnica)
    categoriaSerie = CategoriaSerie.objects.get(id=id_categoriaSerie)
    for _ in range(num_entries):
        SC = SerieClimatica(
            fecha_inicio = date_object,
            fecha_fin = fecha_fin,
            id_mesa_tecnica = mesaTecnica,
            id_categoria_clima = categoriaSerie
        )
        SC.save()

        print(mesaTecnica.id)
        print(categoriaSerie.id)
        print(date_object)
        print(fecha_fin)

        date_object = date_object + timedelta(days=365)
        fecha_fin = date_object + timedelta(days=365)

def seed_PeriodoClimaticoEscenario(num_entries, id_periodoClimatico, id_escenario):
    periodoClimatico = SerieClimatica.objects.get(id=id_periodoClimatico)
    escenario = Escenario.objects.get(id=id_escenario)
    for _ in range(num_entries):
        PCE = PeriodoClimaticoEscenario(
            ejecutado = 1,
            id_periodo_climatico = periodoClimatico,
            id_escenario = escenario
        )
        PCE.save()

        print(periodoClimatico.id)
        print(escenario.id)

def seed_PuntoGestion(num_entries=1):
    count = 0
    for _ in range(num_entries):
        PG = PuntoGestion(
            nombre = "Punto de gestión "+ str(count),
            latitud = fake.latitude(),
            longitud = fake.longitude(),
            altitud = 2500,
            descripcion = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)
        )
        PG.save()
        count += 1
        print("Punto de Gestión "+ str(count)+ " creado")

def seed_ValoresPotenciales(num_entries, id_punto_gestion, id_periodo_climatico, id_variable, fecha_inicial):
    fecha_inicio = fecha_inicial
    date_object = datetime.strptime(fecha_inicio, '%Y-%m-%d')

    puntoGestion = PuntoGestion.objects.get(id=id_punto_gestion)
    periodoClimaticoEscenario = PeriodoClimaticoEscenario.objects.get(id=id_periodo_climatico)
    variable = Variable.objects.get(id=id_variable)
    for _ in range(num_entries):
        VP = ValoresPotencial(
            fecha = date_object,
            valor = numpy.random.uniform(-4, 4),
            id_punto_gestion = puntoGestion,
            id_periodo_climatico_escenario = periodoClimaticoEscenario,
            id_variable = variable
        )
        VP.save()
        date_object = date_object + timedelta(days=1)

    print("Seeder Finalizado")

def seed_variableReglaOperacion(id_variable, puntos_gestion):
    count = 0
    for i in range(puntos_gestion):
        i=1+count
        puntoGestion = PuntoGestion.objects.get(id=i)
        variables = Variable.objects.get(id=id_variable)
        VRO = VariableReglaOperacion(
            id_variable = variables,
            id_punto_gestion = puntoGestion
        )
        VRO.save()
        print(VRO)
        count+=1

def seed_codigoReglasOperacion():
    variablesReglasOperacion = VariableReglaOperacion.objects.all()
    serializerVariablesReglasOperacion = VariableReglaOperacionSerializer(variablesReglasOperacion, many=True)
    for i in serializerVariablesReglasOperacion.data:
        variableRO = VariableReglaOperacion.objects.get(id=i["id"])
        CRO = CodigoReglasOperacion(
            codigo = "RO_1",
            descripcion = fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None),
            escenario_base = True,
            id_variable_regla_operacion = variableRO
        )
        CRO.save()
    print("Seeder finalizado")



def seed_valoresReglasOperacion(dias, mes):
    codigosReglasOperacion = CodigoReglasOperacion.objects.all()
    serializerCodigosRO = CodigoReglasOperacionSerializer(codigosReglasOperacion, many=True)
    count = 1
    for i in range(dias):
        for j in serializerCodigosRO.data:
            codigoRO = CodigoReglasOperacion.objects.get(id=j["id"])
            ROI = ValoresReglasOperacion(
                valor = str(numpy.random.uniform(1, 5)),
                dia = count,
                mes = mes,
                id_codigo_regla_operacion = codigoRO
            )
            ROI.save()
        count+=1
    print("Seeder Reglas Operacion Inicial Finalizado")

def seed_outputvaloresras(num_entries, id_seccion, id_periodo_climatico, id_variable, fecha_inicial):
    fecha_inicio = fecha_inicial
    date_object = datetime.strptime(fecha_inicio, '%Y-%m-%d')

    seccion = Seccion.objects.get(id=id_seccion)
    periodoClimaticoEscenario = PeriodoClimaticoEscenario.objects.get(id=id_periodo_climatico)
    variable = Variable.objects.get(id=id_variable)
    for _ in range(num_entries):
        VP = ValoresPotencial(
            fecha = date_object,
            valor = numpy.random.uniform(-4, 4),
            id_seccion = seccion,
            id_periodo_climatico_escenario = periodoClimaticoEscenario,
            id_variable = variable
        )
        VP.save()
        date_object = date_object + timedelta(days=1)

    print("Seeder Finalizado")

def seed_limites(id_variable):
    variable = Variable.objects.get(id=id_variable)
    for i in range(1, 8):
        puntoGestion = PuntoGestion.objects.get(id=i)
        tipo_uso =  TipoUso.objects.get(id=1)
        LIMITE = Limite(
            nombre = variable.nombre,
            valor = numpy.random.uniform(5.1, 5),
            constante = True,
            id_variable = variable,
            id_punto_gestion = puntoGestion,
            id_tipo_uso = tipo_uso
        )
        LIMITE.save()
    print("Seeder Finalizado")

def seed_limitesOSI(id_variable, valor):
    variable = Variable.objects.get(id=id_variable)
    for i in range(1, 8):
        puntoGestion = PuntoGestion.objects.get(id=i)
        tipo_uso =  TipoUso.objects.get(id=1)
        LIMITE = Limite(
            nombre = variable.nombre,
            valor = valor,
            constante = True,
            id_variable = variable,
            id_punto_gestion = puntoGestion,
            id_tipo_uso = tipo_uso
        )
        LIMITE.save()
    print("Seeder Finalizado")


def seed_valoresOsi(id_escenario):
    variable = Variable.objects.get(nombre="OSI")
    escenario = Escenario.objects.get(id=id_escenario)
    for i in range(1, 8):
        puntoGestion = PuntoGestion.objects.get(id=i)
        for j in range(1, 5):
            categoriaOsi = CategoriaOsi.objects.get(id=j)
            VOSI = ValoresOsi(
                valor = 25,
                id_categoria_osi=categoriaOsi,
                id_punto_gestion=puntoGestion,
                id_escenario=escenario,
                id_variable=variable
            )
            VOSI.save()
    print("Seeder finalizado")
