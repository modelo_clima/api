import json

from django.shortcuts import get_object_or_404
from django.test import TestCase, Client

from configuraciones_basicas.models import *
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

# initialize the APIClient app
from configuraciones_basicas.serializers import *


class TipoVariableTestCase(APITestCase):
    pk = 100;

    def setUp(self):
        TipoVariable.objects.create(nombre='A1')
        TipoVariable.objects.create(nombre='A2')
        TipoVariable.objects.create(nombre='A3')
        TipoVariable.objects.create(nombre='A4')

    def test_list(self):
        # get API response
        response = self.client.get('/api/tipovariable/', format='json')
        # get data from db
        rowsDB = TipoVariable.objects.all()
        serializer = TipoVariableSerializer(rowsDB, many=True)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_filter(self):
        # get API response
        response = self.client.get('/api/tipovariable/?nombre=A1', format='json')
        # get data from db
        rowsDB = TipoVariable.objects.filter(nombre__contains='A1')
        serializer = TipoVariableSerializer(rowsDB, many=True)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        rowsDB = TipoVariable.objects.get(nombre='A1')
        response = self.client.get('/api/tipovariable/' + str(rowsDB.id) + '/', format='json')
        serializer = TipoVariableSerializer(rowsDB)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail_faild(self):
        response = self.client.get('/api/tipovariable/' + str(self.pk) + '/', format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create(self):
        data = {'nombre': 'A5'}
        response = self.client.post('/api/tipovariable/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        rowsDB = TipoVariable.objects.get(nombre='A1')
        data = {'nombre': 'A1_Modificado'}
        response = self.client.put('/api/tipovariable/' + str(rowsDB.id) + '/', data)
        rowsDB = TipoVariable.objects.get(nombre='A1_Modificado')
        serializer = TipoVariableSerializer(rowsDB)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_faild(self):
        data = {'nombre': 'A1_Modificado'}
        response = self.client.put('/api/tipovariable/' + str(self.pk) + '/', data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete(self):
        rowsDB = TipoVariable.objects.get(nombre='A1')
        response = self.client.delete('/api/tipovariable/' + str(rowsDB.id) + '/')
        serializer = TipoVariableSerializer(rowsDB)
        self.assertEqual(response.json(), {'detail': 'Registro Eliminado.'})
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    def test_delete_faild(self):
        response = self.client.delete('/api/tipovariable/' + str(self.pk) + '/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class TipoUsoTestCase(APITestCase):
    pk = 100;

    def setUp(self):
        TipoUso.objects.create(nombre='A1')
        TipoUso.objects.create(nombre='A2')
        TipoUso.objects.create(nombre='A3')
        TipoUso.objects.create(nombre='A4')

    def test_list(self):
        # get API response
        response = self.client.get('/api/tipouso/', format='json')
        # get data from db
        rowsDB = TipoUso.objects.all()
        serializer = TipoUsoSerializer(rowsDB, many=True)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_filter(self):
        # get API response
        response = self.client.get('/api/tipouso/?nombre=A1', format='json')
        # get data from db
        rowsDB = TipoUso.objects.filter(nombre__contains='A1')
        serializer = TipoUsoSerializer(rowsDB, many=True)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail(self):
        rowsDB = TipoUso.objects.get(nombre='A1')
        response = self.client.get('/api/tipouso/' + str(rowsDB.id) + '/', format='json')
        serializer = TipoUsoSerializer(rowsDB)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_detail_faild(self):
        response = self.client.get('/api/tipouso/' + str(self.pk) + '/', format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create(self):
        data = {'nombre': 'A5'}
        response = self.client.post('/api/tipouso/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        rowsDB = TipoUso.objects.get(nombre='A1')
        data = {'nombre': 'A1_Modificado'}
        response = self.client.put('/api/tipouso/' + str(rowsDB.id) + '/', data)
        rowsDB = TipoUso.objects.get(nombre='A1_Modificado')
        serializer = TipoUsoSerializer(rowsDB)
        self.assertEqual(response.json(), serializer.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_faild(self):
        data = {'nombre': 'A1_Modificado'}
        response = self.client.put('/api/tipouso/' + str(self.pk) + '/', data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete(self):
        rowsDB = TipoUso.objects.get(nombre='A1')
        response = self.client.delete('/api/tipouso/' + str(rowsDB.id) + '/')
        serializer = TipoUsoSerializer(rowsDB)
        self.assertEqual(response.json(), {'detail': 'Registro Eliminado.'})
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    def test_delete_faild(self):
        response = self.client.delete('/api/tipouso/' + str(self.pk) + '/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
