
--
-- Base de datos: bio_db
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla actas_mesa_tecnica
--

CREATE SEQUENCE actas_mesa_tecnica_id_seq; 
CREATE TABLE actas_mesa_tecnica (
  id int NOT NULL DEFAULT NEXTVAL('actas_mesa_tecnica_id_seq'),
  nombre varchar(150) NOT NULL,
  descripcion varchar(500) DEFAULT NULL,
  fecha_creacion timestamp(0) NOT NULL,
  adjunto varchar(100) NOT NULL ,
  id_mesa_tecnica int NOT NULL
); 
ALTER SEQUENCE actas_mesa_tecnica_id_seq owned BY actas_mesa_tecnica.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla categorias_serie
--

CREATE SEQUENCE categorias_serie_id_seq; 
CREATE TABLE categorias_serie (
  id int NOT NULL DEFAULT NEXTVAL('categorias_serie_id_seq'),
  nombre varchar(150) NOT NULL
); 
ALTER SEQUENCE categorias_serie_id_seq owned BY categorias_serie.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla categoria_estaciones
--

CREATE SEQUENCE categoria_estaciones_id_seq; 
CREATE TABLE categoria_estaciones (
  id int NOT NULL DEFAULT NEXTVAL('categoria_estaciones_id_seq'),
  nombre varchar(50) NOT NULL
); 
ALTER SEQUENCE categoria_estaciones_id_seq owned BY categoria_estaciones.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla caudales_antropicos
--

CREATE SEQUENCE caudales_antropicos_id_seq; 
CREATE TABLE caudales_antropicos (
  id int NOT NULL DEFAULT NEXTVAL('caudales_antropicos_id_seq'),
  dia int NOT NULL,
  valor double precision NOT NULL,
  id_tipo_caudal_variable int NOT NULL,
  id_punto_gestion int NOT NULL
); 
ALTER SEQUENCE caudales_antropicos_id_seq owned BY caudales_antropicos.id;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla datos_hms
--

CREATE SEQUENCE datos_hms_id_seq; 
CREATE TABLE datos_hms (
  id int NOT NULL DEFAULT NEXTVAL('datos_hms_id_seq'),
  fecha date NOT NULL,
  valor double precision NOT NULL,
  id_variable int NOT NULL,
  id_microcuenca int NOT NULL
); 
ALTER SEQUENCE datos_hms_id_seq owned BY datos_hms.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla datos_validados
--

CREATE SEQUENCE datos_validados_id_seq; 
CREATE TABLE datos_validados (
  id int NOT NULL DEFAULT NEXTVAL('datos_validados_id_seq'),
  fecha date NOT NULL,
  valor double precision NOT NULL,
  id_variable int NOT NULL,
  id_estacion int NOT NULL
); 
ALTER SEQUENCE datos_validados_id_seq owned BY datos_validados.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla departamentos
--

CREATE SEQUENCE departamentos_id_seq; 
CREATE TABLE departamentos (
  id int NOT NULL DEFAULT NEXTVAL('departamentos_id_seq'),
  nombre varchar(150) NOT NULL
); 
ALTER SEQUENCE departamentos_id_seq owned BY departamentos.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla django_admin_log
--

CREATE SEQUENCE django_admin_log_id_seq;
CREATE TABLE django_admin_log (
  id int NOT NULL DEFAULT NEXTVAL('django_admin_log_id_seq'),
  action_time varchar(150) NOT NULL,
  object_id varchar(150) NOT NULL,
  object_repr varchar(150) NOT NULL,
  action_flag varchar(150) NOT NULL,
  change_message varchar(150) NOT NULL,
  content_type_id varchar(150) NOT NULL,
  user_id int NOT NULL

);
ALTER SEQUENCE django_admin_log_id_seq owned BY django_admin_log.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla entidades
--

CREATE SEQUENCE entidades_id_seq; 
CREATE TABLE entidades (
  id int NOT NULL DEFAULT NEXTVAL('entidades_id_seq'),
  nombre varchar(200) NOT NULL
); 
ALTER SEQUENCE entidades_id_seq owned BY entidades.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla escenarios
--

CREATE SEQUENCE escenarios_id_seq; 
CREATE TABLE escenarios (
  id int NOT NULL DEFAULT NEXTVAL('escenarios_id_seq'),
  codigo varchar(100) NOT NULL,
  descripcion varchar(500) DEFAULT NULL,
  fecha_aprobacion date DEFAULT NULL,
  base smallint NOT NULL DEFAULT '0',
  fecha_creacion date DEFAULT NULL,
  id_mesa_tecnica int NOT NULL
); 
ALTER SEQUENCE escenarios_id_seq owned BY escenarios.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla estaciones
--

CREATE SEQUENCE estaciones_id_seq; 
CREATE TABLE estaciones (
  id int NOT NULL DEFAULT NEXTVAL('estaciones_id_seq'),
  codigo varchar(20) NOT NULL,
  nombre varchar(250) NOT NULL,
  latitud varchar(50) DEFAULT NULL,
  longitud varchar(50) DEFAULT NULL,
  altitud varchar(50) DEFAULT NULL,
  estado smallint NOT NULL DEFAULT '0' ,
  fecha_instalacion date NOT NULL,
  fecha_suspension date DEFAULT NULL,
  fecha_inicial_datos date NOT NULL,
  fecha_final_datos date DEFAULT NULL,
  id_entidad int NOT NULL,
  id_categoria_estaciones int NOT NULL ,
  id_tecnologia int NOT NULL,
  id_municipio int NOT NULL
); 
ALTER SEQUENCE estaciones_id_seq owned BY estaciones.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla estados
--

CREATE SEQUENCE estados_id_seq; 
CREATE TABLE estados (
  id int NOT NULL DEFAULT NEXTVAL('estados_id_seq'),
  nombre varchar(100) NOT NULL
); 
ALTER SEQUENCE estados_id_seq owned BY estados.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla formatos_modelo
--

CREATE SEQUENCE formatos_modelo_id_seq; 
CREATE TABLE formatos_modelo (
  id int NOT NULL DEFAULT NEXTVAL('formatos_modelo_id_seq'),
  id_modelo int NOT NULL,
  id_variable int NOT NULL,
  columna varchar(5) NOT NULL,
  fila varchar(5) NOT NULL,
  tipo_formato smallint NOT NULL DEFAULT '0' 
); 
ALTER SEQUENCE formatos_modelo_id_seq owned BY formatos_modelo.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla historicos_llenados_interpolados
--

CREATE SEQUENCE historicos_llenados_interpolados_id_seq; 
CREATE TABLE historicos_llenados_interpolados (
  id int NOT NULL DEFAULT NEXTVAL('historicos_llenados_interpolados_id_seq'),
  fecha date NOT NULL,
  valor double precision NOT NULL,
  id_microcuenca int NOT NULL,
  id_variable int NOT NULL
); 
ALTER SEQUENCE historicos_llenados_interpolados_id_seq owned BY historicos_llenados_interpolados.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla limites_constantes
--

CREATE SEQUENCE limites_constantes_id_seq; 
CREATE TABLE limites_constantes (
  id int NOT NULL DEFAULT NEXTVAL('limites_constantes_id_seq'),
  nombre varchar(100) NOT NULL,
  valor double precision NOT NULL,
  constante smallint DEFAULT '0',
  id_variable int NOT NULL,
  id_punto_gestion int DEFAULT NULL,
  id_tipo_uso int DEFAULT NULL
); 
ALTER SEQUENCE limites_constantes_id_seq owned BY limites_constantes.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla mesa_tecnica
--

CREATE SEQUENCE mesa_tecnica_id_seq; 
CREATE TABLE mesa_tecnica (
  id int NOT NULL DEFAULT NEXTVAL('mesa_tecnica_id_seq'),
  codigo varchar(150) NOT NULL,
  fecha_creacion timestamp(0) NOT NULL,
  fecha_cierre timestamp(0) DEFAULT NULL,
  fecha_inicio_simulacion date NOT NULL,
  fecha_fin_simulacion date NOT NULL,
  descripcion varchar(500) DEFAULT NULL,
  id_tipo_mesa_tecnica int NOT NULL,
  id_mesa_tecnica_principal int DEFAULT NULL
); 
ALTER SEQUENCE mesa_tecnica_id_seq owned BY mesa_tecnica.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla microcuencas
--

CREATE SEQUENCE microcuencas_id_seq; 
CREATE TABLE microcuencas (
  id int NOT NULL DEFAULT NEXTVAL('microcuencas_id_seq'),
  nombre varchar(150) NOT NULL
); 
ALTER SEQUENCE microcuencas_id_seq owned BY microcuencas.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla modelos
--

CREATE SEQUENCE modelos_id_seq; 
CREATE TABLE modelos (
  id int NOT NULL DEFAULT NEXTVAL('modelos_id_seq'),
  nombre varchar(100) NOT NULL
); 
ALTER SEQUENCE modelos_id_seq owned BY modelos.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla municipios
--

CREATE SEQUENCE municipios_id_seq; 
CREATE TABLE municipios (
  id int NOT NULL DEFAULT NEXTVAL('municipios_id_seq'),
  nombre varchar(150) NOT NULL,
  id_departamento int NOT NULL
); 
ALTER SEQUENCE municipios_id_seq owned BY municipios.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla output_hms
--

CREATE SEQUENCE output_hms_id_seq; 
CREATE TABLE output_hms (
  id int NOT NULL DEFAULT NEXTVAL('output_hms_id_seq'),
  fecha date NOT NULL,
  valor double precision NOT NULL,
  id_periodo_climatico_escenario int NOT NULL,
  id_variable int NOT NULL,
  id_microcuenca int NOT NULL
); 
ALTER SEQUENCE output_hms_id_seq owned BY output_hms.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla output_valores_python
--

CREATE SEQUENCE output_valores_python_id_seq; 
CREATE TABLE output_valores_python (
  id int NOT NULL DEFAULT NEXTVAL('output_valores_python_id_seq'),
  fecha date NOT NULL,
  valor double precision NOT NULL,
  id_periodo_climatico_escenario int NOT NULL,
  id_variable int NOT NULL,
  id_punto_gestion int NOT NULL
); 
ALTER SEQUENCE output_valores_python_id_seq owned BY output_valores_python.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla output_valores_ras
--

CREATE SEQUENCE output_valores_ras_id_seq; 
CREATE TABLE output_valores_ras (
  id int NOT NULL DEFAULT NEXTVAL('output_valores_ras_id_seq'),
  fecha date NOT NULL,
  valor double precision NOT NULL,
  id_periodo_climatico_escenario int NOT NULL,
  id_seccion int NOT NULL,
  id_variable int NOT NULL
); 
ALTER SEQUENCE output_valores_ras_id_seq owned BY output_valores_ras.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla periodo_climatico_escenarios
--

CREATE SEQUENCE periodo_climatico_escenarios_id_seq; 
CREATE TABLE periodo_climatico_escenarios (
  id int NOT NULL DEFAULT NEXTVAL('periodo_climatico_escenarios_id_seq'),
  ejecutado smallint NOT NULL DEFAULT '0' ,
  id_periodo_climatico int NOT NULL,
  id_escenario int NOT NULL
); 
ALTER SEQUENCE periodo_climatico_escenarios_id_seq owned BY periodo_climatico_escenarios.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla permisos_app
--

CREATE SEQUENCE permisos_app_id_seq; 
CREATE TABLE permisos_app (
  id int NOT NULL DEFAULT NEXTVAL('permisos_app_id_seq'),
  consultar boolean NOT NULL,
    crear boolean NOT NULL,
    modificar boolean NOT NULL,
    eliminar boolean NOT NULL,
  id_rol int NOT NULL,
  id_seccion_app int CHECK (id_seccion_app > 0) NOT NULL
); 
ALTER SEQUENCE permisos_app_id_seq owned BY permisos_app.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla puntos_gestion
--

CREATE SEQUENCE puntos_gestion_id_seq; 
CREATE TABLE puntos_gestion (
  id int NOT NULL DEFAULT NEXTVAL('puntos_gestion_id_seq'),
  nombre varchar(200) NOT NULL,
  latitud varchar(50) NOT NULL,
  longitud varchar(50) NOT NULL,
  altitud varchar(50) NOT NULL,
  descripcion varchar(500) DEFAULT NULL
); 
ALTER SEQUENCE puntos_gestion_id_seq owned BY puntos_gestion.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla reglas_operacion_escenarios
--

CREATE SEQUENCE reglas_operacion_escenarios_id_seq; 
CREATE TABLE reglas_operacion_escenarios (
  id int NOT NULL DEFAULT NEXTVAL('reglas_operacion_escenarios_id_seq'),
  id_escenario int NOT NULL,
  id_codigo_reglas_operacion int NOT NULL
); 
ALTER SEQUENCE reglas_operacion_escenarios_id_seq owned BY reglas_operacion_escenarios.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla codigos_reglas_operacion
--

CREATE SEQUENCE codigos_reglas_operacion_id_seq; 
CREATE TABLE codigos_reglas_operacion (
  id int NOT NULL DEFAULT NEXTVAL('codigos_reglas_operacion_id_seq'),
  codigo varchar(50) NOT NULL,
  descripcion varchar(500) DEFAULT NULL,
  escenario_base smallint DEFAULT '0',
  id_variable_regla_operacion int NOT NULL
); 
ALTER SEQUENCE codigos_reglas_operacion_id_seq owned BY codigos_reglas_operacion.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla valores_reglas_operacion
--

CREATE SEQUENCE valores_reglas_operacion_id_seq; 
CREATE TABLE valores_reglas_operacion (
  id int NOT NULL DEFAULT NEXTVAL('valores_reglas_operacion_id_seq'),
  valor varchar(50) NOT NULL,
  dia int NOT NULL,
  mes int NOT NULL,
  id_codigo_regla_operacion int NOT NULL
); 
ALTER SEQUENCE valores_reglas_operacion_id_seq owned BY valores_reglas_operacion.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla roles
--

CREATE SEQUENCE roles_id_seq; 
CREATE TABLE roles (
  id int NOT NULL DEFAULT NEXTVAL('roles_id_seq'),
  nombre varchar(100) NOT NULL
); 
ALTER SEQUENCE roles_id_seq owned BY roles.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla secciones
--

CREATE SEQUENCE secciones_id_seq; 
CREATE TABLE secciones (
  id int NOT NULL DEFAULT NEXTVAL('secciones_id_seq'),
  nombre varchar(100) NOT NULL,
  id_punto_gestion int NOT NULL
); 
ALTER SEQUENCE secciones_id_seq owned BY secciones.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla secciones_app
--

CREATE SEQUENCE secciones_app_id_seq; 
CREATE TABLE secciones_app (
  id int NOT NULL DEFAULT NEXTVAL('secciones_app_id_seq'),
  nombre varchar(45) NOT NULL,
  id_seccion_app int DEFAULT NULL
); 
ALTER SEQUENCE secciones_app_id_seq owned BY secciones_app.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla seguimiento_estados
--

CREATE SEQUENCE seguimiento_estados_id_seq; 
CREATE TABLE seguimiento_estados (
  id int NOT NULL DEFAULT NEXTVAL('seguimiento_estados_id_seq'),
  nombre_tabla varchar(100) NOT NULL,
  fecha_creacion date NOT NULL,
  asignador_por_sistema smallint DEFAULT '0',
  id_dato int NOT NULL,
  id_usuario int DEFAULT NULL,
  id_estado int NOT NULL
); 
ALTER SEQUENCE seguimiento_estados_id_seq owned BY seguimiento_estados.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla series_climaticas
--

CREATE SEQUENCE series_climaticas_id_seq; 
CREATE TABLE series_climaticas (
  id int NOT NULL DEFAULT NEXTVAL('series_climaticas_id_seq'),
  codigo varchar(50) NOT NULL,
  fecha_inicio date DEFAULT NULL,
  fecha_fin date DEFAULT NULL,
  id_mesa_tecnica int NOT NULL,
  id_categoria_clima int NOT NULL
); 
ALTER SEQUENCE series_climaticas_id_seq owned BY series_climaticas.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tecnologia_estaciones
--

CREATE SEQUENCE tecnologia_estaciones_id_seq; 
CREATE TABLE tecnologia_estaciones (
  id int NOT NULL DEFAULT NEXTVAL('tecnologia_estaciones_id_seq'),
  nombre varchar(50) NOT NULL
); 
ALTER SEQUENCE tecnologia_estaciones_id_seq owned BY tecnologia_estaciones.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tipo_caudal
--

CREATE SEQUENCE tipo_caudal_id_seq; 
CREATE TABLE tipo_caudal (
  id int NOT NULL DEFAULT NEXTVAL('tipo_caudal_id_seq'),
  nombre varchar(150) NOT NULL
); 
ALTER SEQUENCE tipo_caudal_id_seq owned BY tipo_caudal.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tipo_caudal_variable
--

CREATE SEQUENCE tipo_caudal_variable_id_seq; 
CREATE TABLE tipo_caudal_variable (
  id int NOT NULL DEFAULT NEXTVAL('tipo_caudal_variable_id_seq'),
  id_tipo_caudal int NOT NULL,
  id_variable int NOT NULL
); 
ALTER SEQUENCE tipo_caudal_variable_id_seq owned BY tipo_caudal_variable.id;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tipos_usos
--

CREATE SEQUENCE tipos_usos_id_seq; 
CREATE TABLE tipos_usos (
  id int NOT NULL DEFAULT NEXTVAL('tipos_usos_id_seq'),
  nombre varchar(255) NOT NULL
); 
ALTER SEQUENCE tipos_usos_id_seq owned BY tipos_usos.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tipo_mesa_tecnica
--

CREATE SEQUENCE tipo_mesa_tecnica_id_seq; 
CREATE TABLE tipo_mesa_tecnica (
  id int NOT NULL DEFAULT NEXTVAL('tipo_mesa_tecnica_id_seq'),
  nombre varchar(45) NOT NULL
); 
ALTER SEQUENCE tipo_mesa_tecnica_id_seq owned BY tipo_mesa_tecnica.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tipo_variable
--

CREATE SEQUENCE tipo_variable_id_seq; 
CREATE TABLE tipo_variable (
  id int NOT NULL DEFAULT NEXTVAL('tipo_variable_id_seq'),
  nombre varchar(150) NOT NULL 
); 
ALTER SEQUENCE tipo_variable_id_seq owned BY tipo_variable.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla trimestre_media_movil
--

CREATE SEQUENCE trimestre_media_movil_id_seq; 
CREATE TABLE trimestre_media_movil (
  id int NOT NULL DEFAULT NEXTVAL('trimestre_media_movil_id_seq'),
  nombre varchar(3) NOT NULL 
); 
ALTER SEQUENCE trimestre_media_movil_id_seq owned BY trimestre_media_movil.id;

-- --------------------------------------------------------
--
-- Estructura de tabla para la tabla unidad_medida
--

CREATE SEQUENCE unidad_medida_id_seq; 
CREATE TABLE unidad_medida (
  id int NOT NULL DEFAULT NEXTVAL('unidad_medida_id_seq'),
  nombre varchar(200) NOT NULL ,
  simbolo varchar(10) DEFAULT NULL 
); 
ALTER SEQUENCE unidad_medida_id_seq owned BY unidad_medida.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla administracion_basica_user
--

CREATE SEQUENCE administracion_basica_user_id_seq; 
CREATE TABLE administracion_basica_user (
  id int NOT NULL DEFAULT NEXTVAL('administracion_basica_user_id_seq'),
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  email character varying(255) NOT NULL,
  username character varying(255) NOT NULL,
  password character varying(500) NOT NULL,
  last_login timestamp with time zone,
  estado boolean NOT NULL,
  active boolean NOT NULL,
  staff boolean NOT NULL,
  admin boolean NOT NULL,
  timestamp timestamp with time zone NOT NULL,
  id_entidad integer NOT NULL,
  id_rol integer NOT NULL
); 
ALTER SEQUENCE administracion_basica_user_id_seq owned BY administracion_basica_user.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla valores_media_movil
--

CREATE SEQUENCE valores_media_movil_id_seq; 
CREATE TABLE valores_media_movil (
  id int NOT NULL DEFAULT NEXTVAL('valores_media_movil_id_seq'),
  mes int NOT NULL,
  temporada int NOT NULL ,
  valor double precision NOT NULL,
  id_variable int NOT NULL,
  id_estacion int DEFAULT NULL,
  id_trimestre_media_movil int NOT NULL
); 
ALTER SEQUENCE valores_media_movil_id_seq owned BY valores_media_movil.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla valores_potenciales
--

CREATE SEQUENCE valores_potenciales_id_seq; 
CREATE TABLE valores_potenciales (
  id int NOT NULL DEFAULT NEXTVAL('valores_potenciales_id_seq'),
  fecha date NOT NULL,
  valor double precision NOT NULL,
  id_punto_gestion int NOT NULL,
  id_periodo_climatico_escenario int NOT NULL,
  id_variable int NOT NULL
); 
ALTER SEQUENCE valores_potenciales_id_seq owned BY valores_potenciales.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla variables
--

CREATE SEQUENCE variables_id_seq; 
CREATE TABLE variables (
  id int NOT NULL DEFAULT NEXTVAL('variables_id_seq'),
  nombre varchar(100) NOT NULL ,
  constante smallint NOT NULL DEFAULT '0',
  calculada smallint NOT NULL DEFAULT '0',
  valor_defecto varchar(50) DEFAULT NULL,
  id_unidad_medida int NOT NULL,
  id_tipo_variable int DEFAULT NULL
); 
ALTER SEQUENCE variables_id_seq owned BY variables.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla variables_reglas_operacion
--

CREATE SEQUENCE variables_reglas_operacion_id_seq; 
CREATE TABLE variables_reglas_operacion (
  id int NOT NULL DEFAULT NEXTVAL('variables_reglas_operacion_id_seq'),
  id_variable int NOT NULL,
  id_punto_gestion int NOT NULL
); 
ALTER SEQUENCE variables_reglas_operacion_id_seq owned BY variables_reglas_operacion.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla variables_reglas_operacion
--

CREATE SEQUENCE categorias_osi_id_seq; 
CREATE TABLE categorias_osi (
  id int NOT NULL DEFAULT NEXTVAL('categorias_osi_id_seq'),
  nombre varchar(100) NOT NULL,
  descripcion varchar(500) DEFAULT NULL
); 
ALTER SEQUENCE categorias_osi_id_seq owned BY categorias_osi.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla variables_reglas_operacion
--

CREATE SEQUENCE valores_osi_id_seq; 
CREATE TABLE valores_osi (
  id int NOT NULL DEFAULT NEXTVAL('valores_osi_id_seq'),
  valor double precision NOT NULL,
  id_categoria_osi int NOT NULL,
  id_punto_gestion int NOT NULL,
  id_escenario int NOT NULL,
  id_variable int NOT NULL
); 
ALTER SEQUENCE valores_osi_id_seq owned BY valores_osi.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla variables_salinidad
--

CREATE SEQUENCE variables_salinidad_id_seq; 
CREATE TABLE variables_salinidad (
  id int NOT NULL DEFAULT NEXTVAL('variables_salinidad_id_seq'),
  valor double precision NOT NULL,
  id_variable int NOT NULL,
  id_caudal_antropico int NOT NULL
); 
ALTER SEQUENCE variables_salinidad_id_seq owned BY variables_salinidad.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla urls_app_frontend
--

CREATE SEQUENCE urls_app_frontend_id_seq; 
CREATE TABLE urls_app_frontend (
  id int NOT NULL DEFAULT NEXTVAL('urls_app_frontend_id_seq'),
  url varchar(100) NOT NULL ,
  menu smallint DEFAULT '0',
  id_seccion_app int CHECK (id_seccion_app > 0) NOT NULL
); 
ALTER SEQUENCE urls_app_frontend_id_seq owned BY urls_app_frontend.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla alertas
--

CREATE SEQUENCE alertas_id_seq; 
CREATE TABLE alertas (
  id int NOT NULL DEFAULT NEXTVAL('alertas_id_seq'),
  codigo varchar(45) NOT NULL ,
  descripcion varchar(500) NOT NULL
); 
ALTER SEQUENCE alertas_id_seq owned BY alertas.id;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla alertas_usuario
--

CREATE SEQUENCE alertas_usuario_id_seq; 
CREATE TABLE alertas_usuario (
  id int NOT NULL DEFAULT NEXTVAL('alertas_usuario_id_seq'),
  id_alerta int CHECK (id_alerta > 0) NOT NULL,
  id_usuario int CHECK (id_usuario > 0) NOT NULL
); 
ALTER SEQUENCE alertas_usuario_id_seq owned BY alertas.id;

-- --------------------------------------------------------

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla actas_mesa_tecnica
--
ALTER TABLE actas_mesa_tecnica
  ADD PRIMARY KEY (id);
CREATE INDEX fk_MesaTecnica_ActasMesaTecnica_idx ON actas_mesa_tecnica (id_mesa_tecnica);

--
-- Indices de la tabla categorias_serie
--
ALTER TABLE categorias_serie
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla caudales_antropicos
--
ALTER TABLE caudales_antropicos
  ADD PRIMARY KEY (id);
CREATE INDEX fk_TipoCaudalVariable_CaudalesAntropicos_idx ON caudales_antropicos (id_tipo_caudal_variable);
CREATE INDEX fk_PuntoGestion_CaudalesAntropicos_idx ON caudales_antropicos (id_punto_gestion);
  
--
-- Indices de la tabla categoria_estaciones
--
ALTER TABLE categoria_estaciones
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla datos_hms
--
ALTER TABLE datos_hms
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_DatosHms_idx ON datos_hms (id_variable);
CREATE INDEX fk_Microcuencas_DatosHms_idx ON datos_hms (id_microcuenca);

--
-- Indices de la tabla datos_validados
--
ALTER TABLE datos_validados
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Estaciones_DatosValidados_idx ON datos_validados (id_estacion);
CREATE INDEX fk_Variables_DatosValidados_idx ON datos_validados (id_variable);

--
-- Indices de la tabla departamentos
--
ALTER TABLE departamentos
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla entidades
--
ALTER TABLE entidades
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla escenarios
--
ALTER TABLE escenarios
  ADD PRIMARY KEY (id),
  ADD CONSTRAINT codigo_escenarios_UNIQUE UNIQUE(codigo);
CREATE INDEX fk_MesaTecnica_Escenarios_idx ON escenarios (id_mesa_tecnica);

--
-- Indices de la tabla estaciones
--
ALTER TABLE estaciones
  ADD PRIMARY KEY (id),
  ADD CONSTRAINT codigo_estaciones_UNIQUE UNIQUE(codigo);
CREATE INDEX fk_Entidad_Estaciones_idx ON estaciones (id_entidad);
CREATE INDEX fk_CategoriaEstaciones_Estaciones_idx ON estaciones (id_categoria_estaciones);
CREATE INDEX fk_Tecnologias_Estaciones_idx ON estaciones (id_tecnologia);
CREATE INDEX fk_Municipios_Estaciones_idx ON estaciones (id_municipio);

--
-- Indices de la tabla escenarios
--
ALTER TABLE estados
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla formatos_modelo
--
ALTER TABLE formatos_modelo
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Modelo_FormatosModelo_idx ON formatos_modelo (id_modelo);
CREATE INDEX fk_Variables_FormatosModelo_idx ON formatos_modelo (id_variable);

--
-- Indices de la tabla historicos_llenados_interpolados
--
ALTER TABLE historicos_llenados_interpolados
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_HistoricosLlenadosInterpolados_idx ON historicos_llenados_interpolados (id_variable);
CREATE INDEX fk_Microcuencas_HistoricosLlenadosInterpolados_idx ON historicos_llenados_interpolados (id_microcuenca);

--
-- Indices de la tabla limites_constantes
--
ALTER TABLE limites_constantes
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_limites_constantes_idx ON limites_constantes (id_variable);
CREATE INDEX fk_PuntoGestion_limites_constantes_idx ON limites_constantes (id_punto_gestion);
CREATE INDEX fk_TiposUsos_limites_constantes_idx ON limites_constantes (id_tipo_uso);

--
-- Indices de la tabla mesa_tecnica
--
ALTER TABLE mesa_tecnica
  ADD PRIMARY KEY (id),
  ADD CONSTRAINT codigo_mesa_tecnica_UNIQUE UNIQUE(codigo);
CREATE INDEX fk_TipoMesaTecnica_MesaTecnica_idx ON mesa_tecnica (id_tipo_mesa_tecnica);
CREATE INDEX fk_MesaTecnicaGeneral_MesaTecnica_idx ON mesa_tecnica (id_mesa_tecnica_principal);

--
-- Indices de la tabla microcuencas
--
ALTER TABLE microcuencas
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla modelos
--
ALTER TABLE modelos
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla municipios
--
ALTER TABLE municipios
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Departamento_Municipio_idx ON municipios (id_departamento);

--
-- Indices de la tabla output_hms
--
ALTER TABLE output_hms
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_OutputHmls_idx ON output_hms (id_variable);
CREATE INDEX fk_Microcuencas_OutputHmls_idx ON output_hms (id_microcuenca);
CREATE INDEX fk_PeriodosClimaticosEscenarios_OutputHmls_idx ON output_hms (id_periodo_climatico_escenario);

--
-- Indices de la tabla output_valores_python
--
ALTER TABLE output_valores_python
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_OutputPython_idx ON output_valores_python (id_variable);
CREATE INDEX fk_PuntoGestion_ValoresPython_idx ON output_valores_python (id_punto_gestion);
CREATE INDEX fk_PerdiodoClimaticoEscenario_OutputPython_idx ON output_valores_python (id_periodo_climatico_escenario);

--
-- Indices de la tabla output_valores_ras
--
ALTER TABLE output_valores_ras
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_OutputRas_idx ON output_valores_ras (id_variable);
CREATE INDEX fk_Secciones_OutputRas_idx ON output_valores_ras (id_seccion);
CREATE INDEX fk_PeriodoClimaticoEscenario_OutputRas_idx ON output_valores_ras (id_periodo_climatico_escenario);

--
-- Indices de la tabla periodo_climatico_escenarios
--
ALTER TABLE periodo_climatico_escenarios
  ADD PRIMARY KEY (id);
CREATE INDEX fk_PeriodoClimatico_PeriodoClimaticoEscenario_idx ON periodo_climatico_escenarios (id_periodo_climatico);
CREATE INDEX fk_Escenarios_PeriodoClimaticoEscenario_idx ON periodo_climatico_escenarios (id_escenario);

--
-- Indices de la tabla permisos_app
--
ALTER TABLE permisos_app
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Roles_PermisosApp_idx ON permisos_app (id_rol);
CREATE INDEX fk_SeccionesApp_PermisosApp_idx ON permisos_app (id_seccion_app);

--
-- Indices de la tabla puntos_gestion
--
ALTER TABLE puntos_gestion
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla reglas_operacion_escenarios
--
ALTER TABLE reglas_operacion_escenarios
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Escenario_ReglasOperacionEscenario_idx ON reglas_operacion_escenarios (id_escenario);
CREATE INDEX fk_CodigosRO_ReglasOperacionEscenario_idx ON reglas_operacion_escenarios (id_codigo_reglas_operacion);

--
-- Indices de la tabla codigos_reglas_operacion
--
ALTER TABLE codigos_reglas_operacion
  ADD PRIMARY KEY (id);
CREATE INDEX fk_VariablesRO_CodigosReglasOperacion_idx ON codigos_reglas_operacion (id_variable_regla_operacion);

--
-- Indices de la tabla valores_reglas_operacion
--
ALTER TABLE valores_reglas_operacion
  ADD PRIMARY KEY (id);
CREATE INDEX fk_CodigosReglasOperacion_ValoresReglasOperacion_idx ON valores_reglas_operacion (id_codigo_regla_operacion);

--
-- Indices de la tabla roles
--
ALTER TABLE roles
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla secciones
--
ALTER TABLE secciones
  ADD PRIMARY KEY (id);
CREATE INDEX fk_PuntoGestion_Secciones_idx ON secciones (id_punto_gestion);

--
-- Indices de la tabla secciones_app
--
ALTER TABLE secciones_app
  ADD PRIMARY KEY (id);
CREATE INDEX fk_SeccionAppPadre_SeccionApp_idx ON secciones_app (id_seccion_app);


--
-- Indices de la tabla seguimiento_estados
--
ALTER TABLE seguimiento_estados
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Estado_SeguimientoEstado_idx ON seguimiento_estados (id_estado);
CREATE INDEX fk_Usuario_SeguimientoEstado_idx ON seguimiento_estados (id_usuario);
--
-- Indices de la tabla series_climaticas
--
ALTER TABLE series_climaticas
  ADD PRIMARY KEY (id);
CREATE INDEX fk_MesaTecnica_PeriodoClimatico_idx ON series_climaticas (id_mesa_tecnica);
CREATE INDEX fk_CategoriasClima_PeriodoClimatico_idx ON series_climaticas (id_categoria_clima);

--
-- Indices de la tabla tecnologia_estaciones
--
ALTER TABLE tecnologia_estaciones
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla tipo_caudal
--
ALTER TABLE tipo_caudal
  ADD PRIMARY KEY (id);
  
--
-- Indices de la tabla tipo_caudal_variable
--
ALTER TABLE tipo_caudal_variable
  ADD PRIMARY KEY (id);
CREATE INDEX fk_TipoCaudal_TipoCaudalVariable_idx ON tipo_caudal_variable (id_tipo_caudal);
CREATE INDEX fk_Variable_TipoCaudalVariable_idx ON tipo_caudal_variable (id_variable);
 
--
-- Indices de la tabla tipos_usos
--
ALTER TABLE tipos_usos
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla tipo_mesa_tecnica
--
ALTER TABLE tipo_mesa_tecnica
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla tipo_variable
--
ALTER TABLE tipo_variable
  ADD PRIMARY KEY (id);
  
--
-- Indices de la tabla trimestre_media_movil
--
ALTER TABLE trimestre_media_movil
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla unidad_medida
--
ALTER TABLE unidad_medida
  ADD PRIMARY KEY (id);
  
--
-- Indices de la tabla categorias_osi
--
ALTER TABLE categorias_osi
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla valores_osi
--
ALTER TABLE valores_osi
  ADD PRIMARY KEY (id);
CREATE INDEX fk_CategoriaOSI_ValoresOSI_idx ON valores_osi (id_categoria_osi);
CREATE INDEX fk_PuntoGestion_ValoresOSI_idx ON valores_osi (id_punto_gestion);
CREATE INDEX fk_Escenarios_ValoresOSI_idx ON valores_osi (id_escenario);
CREATE INDEX fk_Variables_ValoresOSI_idx ON valores_osi (id_variable);

--
-- Indices de la tabla administracion_basica_user
--
ALTER TABLE administracion_basica_user
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Roles_Usuarios_idx ON administracion_basica_user (id_rol);
CREATE INDEX fk_Entidades_Usuarios_idx ON administracion_basica_user (id_entidad);

--
-- Indices de la tabla valores_media_movil
--
ALTER TABLE valores_media_movil
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_ValoresMediaMovil_idx ON valores_media_movil (id_variable);
CREATE INDEX fk_Estaciones_ValoresMediaMovil_idx ON valores_media_movil (id_estacion);
CREATE INDEX fk_TrimestreMediaMovil_ValoresMediaMovil_idx ON valores_media_movil (id_trimestre_media_movil);


--
-- Indices de la tabla valores_potenciales
--
ALTER TABLE valores_potenciales
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_ValoresPotenciales_idx ON valores_potenciales (id_variable);
CREATE INDEX fk_PuntoGestion_Potenciales_idx ON valores_potenciales (id_punto_gestion);
CREATE INDEX fk_ValorPonencial_EscenarioTramo_idx ON valores_potenciales (id_periodo_climatico_escenario);

--
-- Indices de la tabla variables
--
ALTER TABLE variables
  ADD PRIMARY KEY (id);
CREATE INDEX fk_UnidadesMedida_Variables_idx ON variables (id_unidad_medida);
CREATE INDEX fk_TipoVariable_Variables_idx ON variables (id_tipo_variable);

--
-- Indices de la tabla variables_reglas_operacion
--
ALTER TABLE variables_reglas_operacion
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_VariablesReglasOperacion_idx ON variables_reglas_operacion (id_variable);
CREATE INDEX fk_PuntoGestion_VariablesReglasOperacion_idx ON variables_reglas_operacion (id_punto_gestion);

--
-- Indices de la tabla variables_salinidad
--
ALTER TABLE variables_salinidad
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Variables_VariablesSalinidad_idx ON variables_salinidad (id_variable);
CREATE INDEX fk_CaudalAntropico_VariablesSalinidad_idx ON variables_salinidad (id_caudal_antropico);


--
-- Indices de la tabla urls_app_frontend
--
ALTER TABLE urls_app_frontend
  ADD PRIMARY KEY (id);
CREATE INDEX fk_SeccionesApp_UrlsAppFrontend_idx ON urls_app_frontend (id_seccion_app);

--
-- Indices de la tabla alertas
--
ALTER TABLE alertas
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla alertas_usuario
--
ALTER TABLE alertas_usuario
  ADD PRIMARY KEY (id);
CREATE INDEX fk_Alertas_AlertasUsuario_idx ON alertas_usuario (id_alerta);
CREATE INDEX fk_Usuarios_AlertasUsuario_idx ON alertas_usuario (id_usuario);


--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla actas_mesa_tecnica
--
ALTER TABLE actas_mesa_tecnica
  ADD CONSTRAINT fk_MesaTecnica_ActasMesaTecnica FOREIGN KEY (id_mesa_tecnica) REFERENCES mesa_tecnica (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla caudales_antropicos
--
ALTER TABLE caudales_antropicos
  ADD CONSTRAINT fk_TipoCaudalVariable_CaudalesAntropicos FOREIGN KEY (id_tipo_caudal_variable) REFERENCES tipo_caudal_variable (id),
  ADD CONSTRAINT fk_PuntoGestion_CaudalesAntropicos FOREIGN KEY (id_punto_gestion) REFERENCES puntos_gestion (id);
  
--
-- Filtros para la tabla datos_hms
--
ALTER TABLE datos_hms
  ADD CONSTRAINT fk_Microcuencas_DatosHms FOREIGN KEY (id_microcuenca) REFERENCES microcuencas (id),
  ADD CONSTRAINT fk_Variables_DatosHms FOREIGN KEY (id_variable) REFERENCES variables (id);

--
-- Filtros para la tabla datos_validados
--
ALTER TABLE datos_validados
  ADD CONSTRAINT fk_Estaciones_DatosValidados FOREIGN KEY (id_estacion) REFERENCES estaciones (id),
  ADD CONSTRAINT fk_Variables_DatosValidados FOREIGN KEY (id_variable) REFERENCES variables (id);

--
-- Filtros para la tabla escenarios
--
ALTER TABLE escenarios
  ADD CONSTRAINT fk_Escenarios_MesaTecnica FOREIGN KEY (id_mesa_tecnica) REFERENCES mesa_tecnica (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla estaciones
--
ALTER TABLE estaciones
  ADD CONSTRAINT fk_CategoriaEstaciones_Estaciones FOREIGN KEY (id_categoria_estaciones) REFERENCES categoria_estaciones (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Entidad_Estaciones FOREIGN KEY (id_entidad) REFERENCES entidades (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Municipios_Estaciones FOREIGN KEY (id_municipio) REFERENCES municipios (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Tecnologias_Estaciones FOREIGN KEY (id_tecnologia) REFERENCES tecnologia_estaciones (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla formatos_modelo
--
ALTER TABLE formatos_modelo
  ADD CONSTRAINT fk_Modelo_FormatosModelo FOREIGN KEY (id_modelo) REFERENCES modelos (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_FormatosModelo FOREIGN KEY (id_variable) REFERENCES variables (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla historicos_llenados_interpolados
--
ALTER TABLE historicos_llenados_interpolados
  ADD CONSTRAINT fk_Microcuencas_HistoricosLlenadosInterpolados FOREIGN KEY (id_microcuenca) REFERENCES microcuencas (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_HistoricosLlenadosInterpolados FOREIGN KEY (id_variable) REFERENCES variables (id);

--
-- Filtros para la tabla limites_constantes
--
ALTER TABLE limites_constantes
  ADD CONSTRAINT fk_PuntoGestion_LimitesConstantes FOREIGN KEY (id_punto_gestion) REFERENCES puntos_gestion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_TiposUsos_LimitesConstantes FOREIGN KEY (id_tipo_uso) REFERENCES tipos_usos (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_LimitesConstantes FOREIGN KEY (id_variable) REFERENCES variables (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla mesa_tecnica
--
ALTER TABLE mesa_tecnica
  ADD CONSTRAINT fk_MesaTecnicaGeneral_MesaTecnica FOREIGN KEY (id_mesa_tecnica_principal) REFERENCES mesa_tecnica (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_TipoMesaTecnica_MesaTecnica FOREIGN KEY (id_tipo_mesa_tecnica) REFERENCES tipo_mesa_tecnica (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla municipios
--
ALTER TABLE municipios
  ADD CONSTRAINT fk_Departamento_Municipio FOREIGN KEY (id_departamento) REFERENCES departamentos (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla output_hms
--
ALTER TABLE output_hms
  ADD CONSTRAINT fk_Microcuencas_OutputHmls FOREIGN KEY (id_microcuenca) REFERENCES microcuencas (id),
  ADD CONSTRAINT fk_PeriodosClimaticosEscenarios_OutputHmls FOREIGN KEY (id_periodo_climatico_escenario) REFERENCES periodo_climatico_escenarios (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_OutputHmls FOREIGN KEY (id_variable) REFERENCES variables (id);

--
-- Filtros para la tabla output_valores_python
--
ALTER TABLE output_valores_python
  ADD CONSTRAINT fk_PerdiodoClimaticoEscenarios_OutputPython FOREIGN KEY (id_periodo_climatico_escenario) REFERENCES periodo_climatico_escenarios (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_PuntoGestion_ValoresPython FOREIGN KEY (id_punto_gestion) REFERENCES puntos_gestion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_OutputPython FOREIGN KEY (id_variable) REFERENCES variables (id);

--
-- Filtros para la tabla output_valores_ras
--
ALTER TABLE output_valores_ras
  ADD CONSTRAINT fk_PeriodoClimaticoEscenarios_OutputRas FOREIGN KEY (id_periodo_climatico_escenario) REFERENCES periodo_climatico_escenarios (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Secciones_OutputRas FOREIGN KEY (id_seccion) REFERENCES secciones (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_OutputRas FOREIGN KEY (id_variable) REFERENCES variables (id);

--
-- Filtros para la tabla periodo_climatico_escenarios
--
ALTER TABLE periodo_climatico_escenarios
  ADD CONSTRAINT fk_Escenarios_PeriodoClimaticoEscenario FOREIGN KEY (id_escenario) REFERENCES escenarios (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_PeriodoClimatico_PeriodoClimaticoEscenario FOREIGN KEY (id_periodo_climatico) REFERENCES series_climaticas (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla permisos_app
--
ALTER TABLE permisos_app
  ADD CONSTRAINT fk_Roles_PermisosApp FOREIGN KEY (id_rol) REFERENCES roles (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_SeccionesApp_PermisosApp FOREIGN KEY (id_seccion_app) REFERENCES secciones_app (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla reglas_operacion_escenarios
--
ALTER TABLE reglas_operacion_escenarios
  ADD CONSTRAINT fk_Escenario_ReglasOperacionEscenario FOREIGN KEY (id_escenario) REFERENCES escenarios (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_CodigosRO_ReglasOperacionEscenario FOREIGN KEY (id_codigo_reglas_operacion) REFERENCES codigos_reglas_operacion (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla codigos_reglas_operacion
--
ALTER TABLE codigos_reglas_operacion
  ADD CONSTRAINT fk_VariablesRO_CodigosReglasOperacion FOREIGN KEY (id_variable_regla_operacion) REFERENCES variables_reglas_operacion (id) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
--
-- Filtros para la tabla codigos_reglas_operacion
--
ALTER TABLE valores_reglas_operacion
  ADD CONSTRAINT fk_CodigosReglasOperacion_ValoresReglasOperacion FOREIGN KEY (id_codigo_regla_operacion) REFERENCES codigos_reglas_operacion (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla secciones
--
ALTER TABLE secciones
  ADD CONSTRAINT fk_PuntoGestion_Secciones FOREIGN KEY (id_punto_gestion) REFERENCES puntos_gestion (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla secciones_app
--
ALTER TABLE secciones_app
  ADD CONSTRAINT fk_SeccionAppPadre_SeccionApp FOREIGN KEY (id_seccion_app) REFERENCES secciones_app (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla secciones_app
--
ALTER TABLE seguimiento_estados
  ADD CONSTRAINT fk_Estado_SeguimientoEstado FOREIGN KEY (id_estado) REFERENCES estados (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Usuario_SeguimientoEstado FOREIGN KEY (id_usuario) REFERENCES administracion_basica_user (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla series_climaticas
--
ALTER TABLE series_climaticas
  ADD CONSTRAINT fk_CategoriasClima_PeriodoClimatico FOREIGN KEY (id_categoria_clima) REFERENCES categorias_serie (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_MesaTecnica_PeriodoClimatico FOREIGN KEY (id_mesa_tecnica) REFERENCES mesa_tecnica (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla series_climaticas
--
ALTER TABLE tipo_caudal_variable
  ADD CONSTRAINT fk_TipoCaudal_TipoCaudalVariable FOREIGN KEY (id_tipo_caudal) REFERENCES tipo_caudal (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variable_TipoCaudalVariable FOREIGN KEY (id_variable) REFERENCES variables (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla administracion_basica_user
--
ALTER TABLE administracion_basica_user
  ADD CONSTRAINT fk_Entidades_Usuarios FOREIGN KEY (id_entidad) REFERENCES entidades (id),
  ADD CONSTRAINT fk_Roles_Usuarios FOREIGN KEY (id_rol) REFERENCES roles (id);

--
-- Filtros para la tabla valores_media_movil
--
ALTER TABLE valores_media_movil
  ADD CONSTRAINT fk_Variables_ValoresMediaMovil FOREIGN KEY (id_variable) REFERENCES variables (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Estaciones_ValoresMediaMovil FOREIGN KEY (id_estacion) REFERENCES estaciones (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_TrimestreMediaMovil_ValoresMediaMovil FOREIGN KEY (id_trimestre_media_movil) REFERENCES trimestre_media_movil (id) ON DELETE NO ACTION ON UPDATE NO ACTION;
  

--
-- Filtros para la tabla valores_potenciales
--
ALTER TABLE valores_potenciales
  ADD CONSTRAINT fk_PeriodoClimaticoEscenarios_ValoresPotenciales FOREIGN KEY (id_periodo_climatico_escenario) REFERENCES periodo_climatico_escenarios (id),
  ADD CONSTRAINT fk_PuntoGestion_ValoresPotenciales FOREIGN KEY (id_punto_gestion) REFERENCES puntos_gestion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_ValoresPotenciales FOREIGN KEY (id_variable) REFERENCES variables (id);

--
-- Filtros para la tabla variables
--
ALTER TABLE variables
  ADD CONSTRAINT fk_TipoVariable_Variables FOREIGN KEY (id_tipo_variable) REFERENCES tipo_variable (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_UnidadesMedida_Variables FOREIGN KEY (id_unidad_medida) REFERENCES unidad_medida (id);

--
-- Filtros para la tabla variables_reglas_operacion
--
ALTER TABLE variables_reglas_operacion
  ADD CONSTRAINT fk_PuntoGestion_VariablesReglasOperacion FOREIGN KEY (id_punto_gestion) REFERENCES puntos_gestion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_VariablesReglasOperacion FOREIGN KEY (id_variable) REFERENCES variables (id) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
--
-- Filtros para la tabla variables_salinidad
--
ALTER TABLE variables_salinidad
  ADD CONSTRAINT fk_Variables_VariablesSalinidad FOREIGN KEY (id_variable) REFERENCES variables (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_CaudalAntropico_VariablesSalinidad FOREIGN KEY (id_caudal_antropico) REFERENCES caudales_antropicos (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla valores_osi
--
ALTER TABLE valores_osi
  ADD CONSTRAINT fk_CategoriaOSI_ValoresOSI FOREIGN KEY (id_categoria_osi) REFERENCES categorias_osi (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_PuntoGestion_ValoresOSI FOREIGN KEY (id_punto_gestion) REFERENCES puntos_gestion (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Escenarios_ValoresOSI FOREIGN KEY (id_escenario) REFERENCES escenarios (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Variables_ValoresOSI FOREIGN KEY (id_variable) REFERENCES variables (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla urls_app_frontend
--
ALTER TABLE urls_app_frontend
  ADD CONSTRAINT fk_SeccionesApp_UrlsAppFrontend FOREIGN KEY (id_seccion_app) REFERENCES secciones_app (id) ON DELETE NO ACTION ON UPDATE NO ACTION;


--
-- Indices de la tabla alertas_usuario
--
ALTER TABLE alertas_usuario
  ADD CONSTRAINT fk_Alertas_AlertasUsuario FOREIGN KEY (id_alerta) REFERENCES alertas (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT fk_Usuarios_AlertasUsuario FOREIGN KEY (id_usuario) REFERENCES administracion_basica_user (id) ON DELETE NO ACTION ON UPDATE NO ACTION;

--------------------------------------------------


DROP USER IF EXISTS admin_bio_db;
CREATE USER admin_bio_db WITH LOGIN SUPERUSER CREATEDB CREATEROLE INHERIT REPLICATION CONNECTION LIMIT -1 PASSWORD 'admin_bio_db';
   
   
insert into roles (id, nombre) values 
(1,'Super Administrador');
SELECT setval('roles_id_seq', 2, true);

insert into secciones_app (id, nombre, id_seccion_app)values
(1,'Administración Basica',null),
(2,'Entidades',7),
(3,'Permisos App',1),
(4,'Roles',1),
(5,'Seccion App',1),
(6,'Usuarios',1),
(7,'Configuración',null),
(8,'Categoría Estaciones',7),
(9,'Categoría Series',7),
(10,'Estaciones',7),
(11,'Estados',7),
(12,'Limites',7),
(13,'Microcuencas',7),
(14,'Modelos',7),
(15,'Puntos Gestión',7),
(16,'Secciones',7),
(17,'Tecnología Estaciones',7),
(18,'Tipos Caudal',7),
(19,'Tipos Usos',7),
(20,'Tipos Variables',7),
(21,'Unidades Medidas',7),
(22,'Variables',7),
(23,'Variables Reglas Operación',7),
(24,'Datos de Gestión',null),
(25,'Datos Validados',24),
(26,'Modelo Clima',24),
(27,'Mesa Técnica Ambiental',null),
(28,'Gestión Mesa Técnica',27),
(29,'Aprobar Escenarios',27),
(30,'Informes',27),
(31,'Mesa Técnica Principal',null),
(32,'Gestión Mesa Técnica',31),
(33,'Aprobar Escenarios',31),
(34,'Informes',31),
(35,'Ayuda',null);
SELECT setval('secciones_app_id_seq', 36, true);

insert into urls_app_frontend (id_seccion_app, url, menu)values

(4,'roles', 1),
(4,'rol/crear', 0),
(4,'rol/modificar', 0),

(6,'usuarios', 1),
(6,'usuario/crear', 0),
(6,'usuario/modificar', 0),

(2,'entidades', 1),
(2,'entidad/crear', 0),
(2,'entidad/modificar', 0),

(8,'categoriasestaciones',1),
(8,'categoriaestacion/crear', 0),
(8,'categoriaestacion/modificar', 0),

(9,'categoriasseries',1),
(9,'categoriaserie/crear', 0),
(9,'categoriaserie/modificar', 0),

(10,'estaciones',1),
(10,'estacion/crear', 0),
(10,'estacion/modificar', 0),

(11,'estados',1),
(11,'estado/crear', 0),
(11,'estado/modificar', 0),

(12,'limites',1),
(12,'limite/crear', 0),
(12,'limite/modificar', 0),

(13,'microcuencas',1),
(13,'microcuenca/crear', 0),
(13,'microcuenca/modificar', 0),

(14,'modelos',1),
(14,'modelo/crear', 0),
(14,'modelo/modificar', 0),

(15,'puntosgestion',1),
(15,'puntogestion/crear', 0),
(15,'puntogestion/modificar', 0),

(16,'secciones',1),
(16,'seccion/crear', 0),
(16,'seccion/modificar', 0),

(17,'tecnologiasestaciones',1),
(17,'tecnologiaestacion/crear', 0),
(17,'tecnologiaestacion/modificar', 0),

(18,'tiposcaudal',1),
(18,'tipocaudal/crear', 0),
(18,'tipocaudal/modificar', 0),

(19,'tiposusos',1),
(19,'tipouso/crear', 0),
(19,'tipouso/modificar', 0),

(20,'tiposvariables',1),
(20,'tipovariable/crear', 0),
(20,'tipovariable/modificar', 0),

(21,'unidadesmedidas',1),
(21,'unidadmedida/crear', 0),
(21,'unidadmedida/modificar', 0),

(22,'variables',1),
(22,'variable/crear', 0),
(22,'variable/modificar', 0),

(25,'datosvalidados',1),
(26,'modeloclima', 1),

(28,'mesatecnicaba',1),
(28,'mesatecnicaba/gestionmesatecnicaa', 0),
(28,'mesatecnicaba/aprobarescenarioa', 0),
(28,'mesatecnicaba/informesa', 0),

(32,'mesatecnicaprin',1),
(32,'mesatecnicaprin/gestionmesatecnicap', 0),
(32,'mesatecnicaprin/aprobarescenariop', 0),
(32,'mesatecnicaprin/informesp', 0),

(35,'Ayuda',1);

insert into permisos_app (id, consultar, crear, modificar, eliminar, id_rol, id_seccion_app) values
(1,true,true,true,true,1,1),
(2,true,true,true,true,1,2),
(3,true,true,true,true,1,3),
(4,true,true,true,true,1,4),
(6,true,true,true,true,1,6),
(7,true,true,true,true,1,7),
(5,true,true,true,true,1,5),
(8,true,true,true,true,1,8),
(9,true,true,true,true,1,9),
(10,true,true,true,true,1,10),
(11,true,true,true,true,1,11),
(12,true,true,true,true,1,12),
(13,true,true,true,true,1,13),
(14,true,true,true,true,1,14),
(15,true,true,true,true,1,15),
(16,true,true,true,true,1,16),
(17,true,true,true,true,1,17),
(18,true,true,true,true,1,18),
(19,true,true,true,true,1,19),
(20,true,true,true,true,1,20),
(21,true,true,true,true,1,21),
(22,true,true,true,true,1,22),
(23,true,true,true,true,1,23),
(24,true,true,true,true,1,24),
(25,true,true,true,true,1,25),
(26,true,true,true,true,1,26),
(27,true,true,true,true,1,27),
(28,true,true,true,true,1,28),
(29,true,true,true,true,1,29),
(30,true,true,true,true,1,30),
(31,true,true,true,true,1,31),
(32,true,true,true,true,1,32),
(33,true,true,true,true,1,33),
(34,true,true,true,true,1,34),
(35,true,true,true,true,1,35);
SELECT setval('permisos_app_id_seq', 36, true);

insert into entidades (id, nombre) values
(1, 'Uniboyaca');
SELECT setval('entidades_id_seq', 2, true);

insert into administracion_basica_user (id, password, last_login, email, username, first_name, last_name, estado, active, staff, admin, "timestamp", id_entidad, id_rol) values
(1, 'pbkdf2_sha256$180000$MuYzQHqVCCqM$pPN6tzW25cRGPCrbzKY9JhvWYM4neQmPyzKq25Owi6c=',null, 'admin@gmail.com', 'admin', 'Administrador','', true, true, true, true, '2020-03-25 08:24:55.908316-05', 1, 1);
SELECT setval('administracion_basica_user_id_seq', 2, true);
---Contraseña 123 username admin

insert into tipo_mesa_tecnica(nombre) values
('Principal'),
('Ambiental');

INSERT INTO estados ("nombre") VALUES
('Creado'),
('Cerrado'),
('Activo'),
('Aprobado'),
('Ejecutado'),
('En Ejecucion'),
('Inactivo'),
('No Aprobado'),
('Visualizar');

insert into trimestre_media_movil (nombre) values
('DEF'),
('EFM'),
('FMA'),
('MAM'),
('AMJ'),
('MJJ'),
('JJA'),
('JAS'),
('ASO'),
('SON'),
('OND'),
('NDE');
  
COMMIT;
