from lib2to3.pgen2.parse import ParseError
from wsgiref.util import FileWrapper

from django.db.models import Q
from django.http import HttpResponse, FileResponse
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.parsers import JSONParser, FileUploadParser
from rest_framework.request import Empty
from rest_framework.response import Response

from administracion_basica.permisos_api import PermisosApi
from .models import *
from .serializers import *
from configuraciones_basicas.serializers import *
from general_view_set import GeneralModelViewSet

#Datetime imports
import datetime
from datetime import datetime
from datetime import date
from datetime import timedelta
import time

#SMTP imports
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

from django.db import IntegrityError



###########################################################################################################################################
######################################################### MESAS TECNICAS ##################################################################
# TipoMesaTecnica View
# clase encarga de generar la vista del modelo TipoMesaTecnica
# @Jose Oliverio Patiño Castaño  @date 29/01/2020
class TipoMesaTecnicaViewSet(GeneralModelViewSet):
    model_db = TipoMesaTecnica
    queryset = TipoMesaTecnica.objects.all()
    serializer_class = TipoMesaTecnicaSerializer
    seccion = 'Unidades Medida'
    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def destroy(self, request,pk=None):
        return Response({'detail':'El Tipo Mesa Tecnica no se puede eliminar...'},
                        status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

# MesaTecnica View
# clase encarga de generar la vista del modelo MesaTecnica
# @Jose Oliverio Patiño Castaño  @date 29/01/2020
class MesaTecnicaViewSet(GeneralModelViewSet):
    model_db = MesaTecnica
    detail_rows_db = True
    queryset = MesaTecnica.objects.all()
    serializer_class = MesaTecnicaSerializer

    # @Luis Mauricio Mosquera Cifuentes @Date: 25/02/2020
    def list(self, request):
        if PermisosApi.apiAutorizacion(request.user, self.seccion,'consultar')==False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion

        if not 'id_tipo_mesa_tecnica' in request.query_params:
            return Response({'detail': 'Debe seleccionar un tipo de mesa tecnica'},
                            status=status.HTTP_400_BAD_REQUEST)
        elif not 'asociar' in request.query_params:
            return Response({'detail': 'Debe indicar si esta asociando una MTBA a una MTP'},
                                        status=status.HTTP_400_BAD_REQUEST)
        queryset = self.filterObjects(request).order_by('fecha_creacion').reverse() #Falta adicionar el order by
        serializer = self.serializer_class(queryset, many=True)
        if serializer.data != "":
            inst = SeguimientoEstadosViewSet
            for data in serializer.data:
                estado = inst.buscarEstadoPorMesaTecnica(inst, data['id'])
                data["id_estado"] = estado.id
                data['estado'] = estado.nombre
                if data['id_tipo_mesa_tecnica'] == 1:
                    #Se busca si hay una MTBA asociada a una MTP
                    mt_asoc = self.buscarMTBAporMTP(data['id'])
                    data['id_mt_asociada'] = mt_asoc.id
                else:
                    mt_asoc = self.buscarMTporID(data['id_mesa_tecnica_principal'])
                if mt_asoc != None:
                    data['codigo_mt_asociada'] = mt_asoc.codigo
            datos1 = sorted(serializer.data, key=lambda x : x['estado'], reverse=False) #Devuelve todas las mesas tecnicas ordenadas por estado
        #Se valida si se van asociar una mesa tecnica bio ambiental a una mesa tecnica principal
        if request.query_params['asociar'] == 'SI':
            datos = []
            for item in serializer.data:
                if item['id_estado'] == 2 and item['id_mesa_tecnica_principal'] == None: #Se valida que el estado sea Cerrado y no este asociada a una MTP la MTBA a listar
                    datos.append(item)
            return Response(datos)
        else:
            return Response(datos1)

    def create(self, request): #Recibe los datos de fecha_inicio_simulacion, id_tipo_mesa_tecnica y descripcion
        usuario = 1
        if usuario == 0:
            return Response({'detail':'No se encuentra el usuario...'},
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
        elif request.data['id_tipo_mesa_tecnica'] == 2:
            fecha_simulacion = request.data['fecha_inicio_simulacion']
            date_object = datetime.strptime(fecha_simulacion, '%Y-%m-%d')
            if date_object.day > 1:
                return Response({'detail':'La fecha del periodo a simular no corresponde al primer dia del mes seleccionado....'},
                                status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
            elif self.buscarFechaSimulacion(fecha_simulacion, request.data['id_tipo_mesa_tecnica']):
                return Response({'detail': 'Periodo a simular ya existe...'},
                                status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
            #llamar funcion calcularFechaFinSimulacion
            fecha_fin = self.calcularFechaFinSimulacion()
        else:
            if self.buscarMTPActivas():
                return Response({'detail':'Hay una MTP activa, por ende, no puede crear mas Mesas Tecnicas Principales...'},
                                status=status.HTTP_417_EXPECTATION_FAILED)
            mtba = self.buscarMTporID(request.data['id_mt_asociada'])
            request.data['fecha_inicio_simulacion'] = mtba.fecha_inicio_simulacion
            fecha_fin = mtba.fecha_fin_simulacion
            request.data['descripcion'] = mtba.descripcion

        #Llamar función asignarCodigoMesaTecnica
        codigo = self.asignarCodigoMesaTecnica()
        #llamar funcion asignarFechaActual
        fecha_creacion = time.strftime("%Y-%m-%dT%H:%M:%S")

        data = self.nuevaMT(codigo, fecha_creacion, request.data['fecha_inicio_simulacion'], fecha_fin, request.data['descripcion'], request.data['id_tipo_mesa_tecnica'])
        # asignar serializer
        serializer = self.serializer_class(data=data)  # request.datos son los datos de la petición
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo
            mt = self.buscarMTporCodigo(codigo)  # Se busca el id de la mesa tecnica que se acaba de crear
            instanciaSeguimientoEstado = SeguimientoEstadosViewSet
            context = instanciaSeguimientoEstado.definirContexto(SeguimientoEstadosViewSet, mt.id, 1, 0, 'mesa_tecnica')
            if instanciaSeguimientoEstado.nuevoSeguimientoEstado(context, usuario):  # Se envia el context creado anteriormente y el id del usuario
                if request.data['id_tipo_mesa_tecnica'] == 2:
                    inst = EscenarioViewSet()  # Instancia clase Escenario
                    dataEscenario = {'id_mesa_tecnica': mt.id, 'base': 1, 'descripcion': 'Escenario base',
                                     'usuario': usuario}  # Se envia el id de la mt y 1 si es para Escenario Base
                    inst.createEscenario(dataEscenario)  # Crear Escenario Base
                else:
                    mtba.id_mesa_tecnica_principal = mt
                    mtba.save()
                    print(mtba)
                context['id_estado'] = 3
                instanciaSeguimientoEstado.nuevoSeguimientoEstado(context, usuario)
                self.enviarCorreo(request.data['id_tipo_mesa_tecnica'], codigo)  # Llamar función enviarCorreo
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
            else:
                return Response(serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def buscarMTPActivas(self):
        queryset = MesaTecnica.objects.filter(id_tipo_mesa_tecnica=1)
        serializer = self.serializer_class(queryset, many=True)

        if serializer.data != "":
            inst = SeguimientoEstadosViewSet
            for data in serializer.data:
                estado = inst.buscarEstadoPorMesaTecnica(inst, data['id'])
                if estado.id == 1 or estado.id == 3:
                    return  True
        return  False

    def nuevaMT(self, codigo, fecha_creacion, fecha_inicio_simulacion, fecha_fin_simulacion, descripcion, id_tipo_mesa_tecnica):
        return {'codigo': codigo, 'fecha_creacion':fecha_creacion,
                'fecha_inicio_simulacion':fecha_inicio_simulacion, 'fecha_fin_simulacion':fecha_fin_simulacion,
                'descripcion':descripcion, 'id_tipo_mesa_tecnica':id_tipo_mesa_tecnica}

    def retrieve(self, request, pk=None):
        data = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(data)
        inst = SeguimientoEstadosViewSet
        est = inst.buscarEstadoPorMesaTecnica(inst, serializer.data['id'])
        datos = {}
        for item in serializer.data:
            datos[item] = serializer.data[item]
        datos['id_estado'] = est.id
        datos['estado'] = est.nombre
        if datos['id_tipo_mesa_tecnica'] == 1:
            mt_asoc = self.buscarMTBAporMTP(data.id)
        else:
            mt_asoc = self.buscarMTporID(datos['id_mesa_tecnica_principal'])
        if mt_asoc != None:
            datos['id_mt_asociada'] = mt_asoc.id
            datos['codigo_mt_asociada'] = mt_asoc.codigo

        actas=[]
        escenarios=[]
        #if datos['id_tipo_mesa_tecnica'] == 1:
        #    datosEscenariosMTA = Escenario.objects.filter(id_mesa_tecnica= mt_asoc.id)
        #    escenarioSerializer = EscenarioSerializer(datosEscenariosMTA, many=True)
        #    escenariosView = EscenarioViewSet()
        #    escenariosDB = escenariosView.indexEscenarios(escenarioSerializer.data)
        #    for item in escenariosDB:
        #        if item['id_estado'] == 9: # Se valida que el estado sea Visualizado
        #            escenarios.append(item)


        #datosEscenarios = Escenario.objects.filter(id_mesa_tecnica=pk)
        #escenarioSerializer = EscenarioSerializer(datosEscenarios, many=True)
        instanciaEscenarios = EscenarioViewSet()
        escenarios = instanciaEscenarios.listarEscenarios(MesaTecnicaViewSet, data)
        return Response({'info': datos, 'escenarios': escenarios, 'actas':actas})

    # @Luis Mauricio Mosquera Cifuentes @date 1/03/2020
    def buscarFechaSimulacion(self, fecha_inicio_simulacion, id_tipo_mesa_tecnica):
        queryset = MesaTecnica.objects.filter(fecha_inicio_simulacion=fecha_inicio_simulacion, id_tipo_mesa_tecnica=id_tipo_mesa_tecnica)
        serializer = self.serializer_class(queryset, many=True)
        cont = 0
        if serializer.data:
            for mt in serializer.data:
                inst = SeguimientoEstadosViewSet
                estad = inst.buscarEstadoPorMesaTecnica(SeguimientoEstadosViewSet, mt['id'])
                if estad.id == 1 or estad.id == 2 or estad.id == 3: #Se valida si el estado es Creado, Cerrado o Activo
                    cont = 1
        if cont == 1:
            return True
        else:
            return False

    # @Luis Mauricio Mosquera Cifuentes @date 25/02/2020
    def buscarMTporCodigo(self, codigo):
        try:
            queryset = MesaTecnica.objects.get(codigo=codigo)
        except MesaTecnica.DoesNotExist:
            queryset = None
        return queryset

    # @Luis Mauricio Mosquera Cifuentes @date 4/04/2020
    def buscarMTBAporMTP(self, id_mtp):
        try:
            queryset = MesaTecnica.objects.get(id_mesa_tecnica_principal=id_mtp)
        except MesaTecnica.DoesNotExist:
            queryset = None
        return queryset

    def buscarMTporID(self, id):
        try:
            queryset = MesaTecnica.objects.get(id=id)
        except MesaTecnica.DoesNotExist:
            queryset = None
        return queryset

    def buscarMesaTecnicaPorEscenario(self, escenario):
        mesaTecnica = MesaTecnica.objects.get(escenario__id=escenario)
        return mesaTecnica

    def calcularFechaFinSimulacion(self):
        fecha_inicio = self.request.data['fecha_inicio_simulacion']  # obtener fecha_inicio de simulación para calcular fecha fin
        date_object = datetime.strptime(fecha_inicio, '%Y-%m-%d')  # convertir la fecha en un objeto datetime
        fecha_fin = date_object + timedelta(days=364)  # sumar los 364 días de la fecha inicio para calcular la fecha fin de simulación
        return fecha_fin.__format__('%Y-%m-%d')  # asignar dato calculado al request de la fecha fin

    def asignarCodigoMesaTecnica(self):
        # Verificar si es mesa técnica ambiental o mesa técnica principal
        #Si no se envía el id_mesa_tecnica_principal, quiere decir que es una mesa tecnica Bio ambiental
        if self.request.data['id_tipo_mesa_tecnica'] == 1:
            cantidad_registros = self.queryset.filter(codigo__contains='MTP_').count()
            cod = "MTP_" + str(cantidad_registros)
        else:
            cantidad_registros = self.queryset.filter(codigo__contains='MTBA_').count()
            # Asignar código MTBA con base en la cantidad de registros
            cod = "MTBA_" + str(cantidad_registros)
        return cod

    def enviarCorreo(self, id_tipo_mesa_tecnica, codigo):
        #Crear instancia objeto del mensaje
        msg = MIMEMultipart()

        mensaje = "La Mesa Técnica "
        if id_tipo_mesa_tecnica == 1:
            mensaje += "Principal "
        else:
            mensaje += "Bio Ambiental "
        mensaje += codigo + " ha sido creada, a continuación se ejecutará el proceso de simulación"

        #Configurar los parámetros del mensaje
        password = "Top82567"
        msg['From'] = "jopatino@uniboyaca.edu.co"
        msg['To'] = "luimosquera@uniboyaca.edu.co"
        msg['Subject'] = "MESA TÉCNICA CREADA"

        #Agregar el cuerpo del mensaje
        msg.attach(MIMEText(mensaje, 'html'))

        #crear e iniciar servidor
        server = smtplib.SMTP('smtp.gmail.com: 587')
        server.starttls()

        #Credenciales de logueo para enviar el email
        server.login(msg['From'], password)

        #Enviar el mensaje vía servidor
        server.sendmail(msg['From'], msg['To'], msg.as_string())
        #Cerrar servidor
        server.quit()

    def listarMesaTecnicaInforme(self, id_mesa_tecnica):
        # Consulto los datos de la mesa técnica y los agrego al diccionario data
        mesaTecnica = MesaTecnica.objects.get(id=id_mesa_tecnica)
        data = {"mesa_tecnica": mesaTecnica.codigo, "fecha_creacion": mesaTecnica.fecha_creacion,
                "descripcion": mesaTecnica.descripcion,
                "fecha_inicio_simulacion": mesaTecnica.fecha_inicio_simulacion,
                "fecha_fin_simulacion": mesaTecnica.fecha_fin_simulacion}
        return data

    def update(self, request, pk=None, *args, **kwargs):
        usuario = 1
        if usuario > 0:
            partial = kwargs.pop('partial', False)
            data = get_object_or_404(self.queryset, pk=pk)

            inst = SeguimientoEstadosViewSet
            dataEstado = inst.buscarEstadoPorMesaTecnica(inst, pk) #Se busca el estado actual de la MT para poder realizar su modificacion
            result = ""
            if request.data['id_estado'] > 1 and request.data['id_estado'] != dataEstado:
                datosValidar = {'id_estado': dataEstado.id, 'pk': pk, 'usuario': usuario}
                result = self.validarEscenarioEjecucion(datosValidar) #Validar escenarios en ejecucion
                if request.data['id_estado'] == 2:
                    request.data['fecha_cierre'] = time.strftime("%Y-%m-%dT%H:%M:%S")

            serializer = self.serializer_class(data, data=request.data, partial=partial)
            if result == "" or result == "ok":
                if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
                    serializer.save()  # .save() guarda el registro en el modulo
                    return Response(serializer.data,
                                    status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
                else:
                    return Response(serializer.errors,
                                    status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
            else:
                return Response({"detail":result},
                                status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
        else:
            return Response({'detail':'No se encuentra el usuario...'},
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def validarEscenarioEjecucion(self, datosValidar):
        if self.request.data['id_estado'] == 2 or self.request.data['id_estado'] == 3 or self.request.data['id_estado'] == 7:
            instSeguimientoEstado = SeguimientoEstadosViewSet
            instEscenario = EscenarioViewSet

            if datosValidar['id_estado'] == 1 or datosValidar['id_estado'] == 3 or datosValidar['id_estado'] == 7: #Se verifica si se modifico el estado a la mesa tecnica o si el estado actual de la mt es inactiva o cerrada
                escenario = instEscenario.buscarEscenariosPorMesaTecnica(datosValidar['pk'])
                if self.buscarEscenariosNoVisualizar(escenario) and self.request.data['id_estado'] != 3:  # Se busca si todos los escenarios de la MT no estan en Visualizar para dejar la MT inactiva
                    context = instSeguimientoEstado.definirContexto(instSeguimientoEstado, datosValidar['pk'], 7, 0, 'mesa_tecnica')
                elif not self.buscarEstadoEscenarios(escenario):  # Se busca si no hay escenario de la MT en ejecucion
                    context = instSeguimientoEstado.definirContexto(instSeguimientoEstado, datosValidar['pk'], self.request.data['id_estado'], 0, 'mesa_tecnica')
                else:
                    return "Hay escenarios en ejecucion."
                instSeguimientoEstado.nuevoSeguimientoEstado(context, datosValidar['usuario'])
                return "ok"
            else:
                return 'Debido al estado actual de la Mesa Tecnica no puede modificar su estado.'
        else:
            return 'Solo puede inactivar o cerrar la Mesa Tecnica.'

    #Metodo que permite buscar si hay por lo menos 1 escenario de 1 mesa tecnica en especifica, en ejecucion
    def buscarEstadoEscenarios(self, escenarios):
        for esc in escenarios:
            instancia = SeguimientoEstadosViewSet
            estado = instancia.buscarEstadoPorEscenarios(instancia, esc['id'])
            if estado.id == 6:
                return True
        return False

    #Metodo que permite buscar si todas los escenarios de una mesa tecnica en especifico, estan en Visualizar
    def buscarEscenariosNoVisualizar(self, escenarios):
        for esc in escenarios:
            instancia = SeguimientoEstadosViewSet
            estado = instancia.buscarEstadoPorEscenarios(instancia, esc['id'])
            if estado.id != 9 and esc['base'] == 0:
                return True #Devuelve True si hay por lo menos 1 escenario en estado diferente a visualizar
        return False #Devuelve False si todos los escenarios estan en Visualizar

    def destroy(self, request,pk=None):
        return Response({'detail':'La Mesa Tecnica no se puede eliminar...'},
                        status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

###########################################################################################################################################
########################################################### ESCENARIOS ####################################################################
# Escenarios View
# clase encarga de generar la vista del modelo Escenarios
# @Jose Oliverio Patiño Castaño  @date 30/01/2020
class EscenarioViewSet(GeneralModelViewSet):
    model_db = Escenario
    queryset = Escenario.objects.all()
    detail_rows_db = True
    serializer_class = EscenarioSerializer

    def list(self, request):
        instanciaMt = MesaTecnicaViewSet
        mt = instanciaMt.buscarMTporID(instanciaMt, request.query_params['id_mesa_tecnica'])
        print(mt)
        dataEsc = self.listarEscenarios(instanciaMt, mt)
        return Response(dataEsc)

    def listarEscenarios(self, instMt, mt):
        datos1 = ''
        if mt.id_tipo_mesa_tecnica.id == 1:
            mt_asoc = instMt.buscarMTBAporMTP(instMt, mt.id)
            datos1 = self.queryIndexEscenario(mt_asoc.id)
        datosEsc = self.queryIndexEscenario(mt.id)
        datos = []
        if datos1 != '':
            for item in datos1:
                if item['id_estado'] == 9 or item['base'] == 1:  # Se valida que el estado sea Visualizado
                    datos.append(item)
            datos += datosEsc
        else:
            datos = datosEsc
        dataEsc = sorted(datos, key=lambda x: x['id_estado'],reverse=False)  # Devuelve todas las mesas tecnicas ordenadas por estado
        return dataEsc

    def queryIndexEscenario(self, id_mt):
        queryset = Escenario.objects.filter(id_mesa_tecnica=id_mt)
        serializer = self.serializer_class(queryset, many=True)
        datos = self.validarEscenario(serializer.data)
        return datos

    def validarEscenario(self, dataEscenario):
        if dataEscenario != "":
            inst = SeguimientoEstadosViewSet
            for data in dataEscenario:
                est = inst.buscarEstadoPorEscenarios(inst, data['id'])
                data["id_estado"] = est.id
                data['estado'] = est.nombre
        return dataEscenario

    def create(self, request): # Recibe id de la mt y 0 por ser un Sub Escenario
        usuario = 1
        if usuario > 0:
            context = {'id_mesa_tecnica':request.data['id_mesa_tecnica'], 'base':request.data['base'], 'usuario':usuario, 'descripcion':'Nuevo Escenario Creado'}
            resul = self.createEscenario(context)
            return Response(resul,
                            status=status.HTTP_201_CREATED)  # return Response(data) devuelve un error durante ejecucion
        else:
            return Response({'detail':'No se encuentra el usuario...'},
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def createEscenario(self, dataEscenario):# dataEscenario trae los parametros (id de la mt y 1 si es Escenario Base o 0 si es un Sub Escenario)
        mesa_tecnica = MesaTecnica.objects.get(id=dataEscenario['id_mesa_tecnica'])
        codigo = self.generarCodigoEscenario(mesa_tecnica)
        base = dataEscenario['base']
        fecha_creacion = time.strftime("%Y-%m-%d")
        id_mesa_tecnica = mesa_tecnica.id

        dataCreate = {'codigo': codigo, 'base': base, 'fecha_creacion': fecha_creacion,
                      'id_mesa_tecnica': id_mesa_tecnica, 'descripcion':dataEscenario['descripcion']}
        if dataEscenario['base'] == 1:
            dataEscenario['usuario'] = 0

        serializer = self.serializer_class(data=dataCreate)  # request.datos son los datos de la petición
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo
            contextEstado = {'codigoEscenario':codigo, 'asignador_por_sistema':dataEscenario['base'], 'id_usuario':dataEscenario['usuario'], 'id_estado':1}
            self.crearSeguimientoEstadoEscenario(contextEstado) #Se crea el seguimiento estado para este escenario base y que fue creado por el sistema
            id_escenario = self.buscarEscenariosPorCodigoEscenario(codigo)
            instReglasOperacion = ReglasOperacionEscenarioViewSet
            instReglasOperacion.crearReglasOperacionIniciales(ReglasOperacionEscenarioViewSet, id_escenario)
            return {'detail': 'Escenario Creado', 'id':serializer.data['id']}  # Devuelve un Ok como confirmacion de la operacion correcta
        else:
            return serializer.errors  #Devuelve un error durante ejecucion

    def retrieve(self, request, pk=None):
        data = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(data)
        inst = SeguimientoEstadosViewSet
        est = inst.buscarEstadoPorEscenarios(inst, serializer.data['id'])
        datos = {}
        instMT = MesaTecnicaViewSet
        for item in serializer.data:
            datos[item] = serializer.data[item]
        mtba = instMT.buscarMesaTecnicaPorEscenario(instMT, datos['id'])
        datos['fecha_inicio_simulacion'] = mtba.fecha_inicio_simulacion
        datos['fecha_fin_simulacion'] = mtba.fecha_fin_simulacion
        datos['id_estado'] = est.id
        datos['estado'] = est.nombre
        return Response(datos)

    def generarCodigoEscenario(self, mt):
        cantidad_registros = Escenario.objects.filter(id_mesa_tecnica=mt.id).count()
        codigoEscenario = mt.codigo + "_ESCE_" + str(cantidad_registros)
        return codigoEscenario

    # @Luis Mauricio Mosquera Cifuentes @Date: 07/03/2020
    def crearSeguimientoEstadoEscenario(self, context):
        dataEsc = self.buscarEscenariosPorCodigoEscenario(context['codigoEscenario']) #Se busca el id del Escenario
        inst = SeguimientoEstadosViewSet
        dataSeguimiento = inst.definirContexto(inst, dataEsc, context['id_estado'], context['asignador_por_sistema'], 'escenario')
        inst.nuevoSeguimientoEstado(dataSeguimiento,  context['id_usuario'])

    # @Luis Mauricio Mosquera Cifuentes @Date: 08/03/2020
    def update(self, request, pk=None, *args, **kwargs): #Recibe como parametros id_estado y descripcion
        usuario = 1
        if usuario > 0:
            partial = kwargs.pop('partial', False)
            data = get_object_or_404(self.queryset, pk=pk)
            serializer = self.serializer_class(data, data=request.data, partial=partial) #Actualizacion de los datos que ingresan nuevos del estado o descripcion
            id_estado = request.data['id_estado']
            if data.descripcion != 'Escenario base': #Se valida que no sea un Escenario Base el que se vaya a modificar
                instanciaSeguimientoEstado = SeguimientoEstadosViewSet
                estadoActual = instanciaSeguimientoEstado.buscarEstadoPorEscenarios(instanciaSeguimientoEstado, data.id)
                if estadoActual.id != 4 and estadoActual.id != 5 and estadoActual.id != 6 and estadoActual.id != 7 and estadoActual.id != 8 and estadoActual.id != 9:
                    if id_estado != 5 and id_estado != 6:  #Se verifica que el estado se modifico diferente a "En Ejecucion" o "Ejecutado"
                        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
                            serializer.save()  # .save() guarda el registro en el modulo
                            if id_estado == 7 or id_estado == 9:
                                contextEstado = {'codigoEscenario':data.codigo, 'asignador_por_sistema':0, 'id_usuario':usuario, 'id_estado':id_estado}
                                self.crearSeguimientoEstadoEscenario(contextEstado)
                            return Response({'detail':'Escenario modificado satisfactoriamente.'},
                                          status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
                        else:
                            return Response({'detail':'No puede modificar el estado a "Ejecutado" o a "En Ejecucion".'},
                                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
                    else:
                        return Response({"detail":"Solo puede modificar el escenario a: Inactivo o Visualizar"},
                                        status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
                else:
                    return Response({"detail":"No puede modificar debido a su estado actual."},
                                    status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
            else:
                return Response({"detail": "No puede modificar un Escenario Base"},
                                status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion
        else:
            return Response({'detail':'No se encuentra el usuario...'},
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    # @Luis Mauricio Mosquera Cifuentes @Date: 06/03/2020
    def buscarEscenariosPorMesaTecnica(id_mesa_tecnica):
        queryset = Escenario.objects.filter(id_mesa_tecnica=id_mesa_tecnica)
        serializer_class = EscenarioSerializer
        serializer = serializer_class(queryset, many=True)
        return serializer.data

    # @Luis Mauricio Mosquera Cifuentes @Date: 07/03/2020
    def buscarEscenariosPorCodigoEscenario(self, codigo):
        queryset = Escenario.objects.get(codigo=codigo)
        return queryset.id

    def destroy(self, request,pk=None):  # def destroy encargada de eliminar registro Unidad de medida en relacion con su id
        try:
            inst = SeguimientoEstadosViewSet
            estadoEscenario = inst.buscarEstadoPorEscenarios(inst, pk)
            if estadoEscenario.id != 4 and estadoEscenario.id != 5 and estadoEscenario.id != 6 and estadoEscenario.id != 7 and estadoEscenario.id != 9:
                data = get_object_or_404(self.queryset, pk=pk)
                if data.descripcion != "Escenario base":
                    data.delete()
                    return Response({"detail": "Registro Eliminado."}, status=status.HTTP_202_ACCEPTED)
                else:
                    return Response({"detail": "No puede eliminar un Escenario Base."},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"detail": "No se puede eliminar el el escenario por su estado actual."},
                                status=status.HTTP_400_BAD_REQUEST)
        except IntegrityError as ide:
            return Response({"detail": "No se puede eliminar el regitro porque esta relacionado. "},
                            status=status.HTTP_409_CONFLICT)

    # @Luis Mauricio Mosquera Cifuentes @Date:09/03/2020
    def buscarEscenarioBase(self, id_escenario):
        id_mt = self.buscarEscenarioPorId(self, id_escenario)
        queryset = Escenario.objects.get(id_mesa_tecnica=id_mt.id, descripcion='Escenario base') #Se busca el Escenario Base asociado a un Escenario especifico
        return queryset.id

    # @Luis Mauricio Mosquera Cifuentes @Date:09/03/2020
    def buscarEscenarioPorId(self, id_escenario): #Metodo en el cual se busca un escenario y se obtiene el id de la mesa tecnica a la que pertenece
        queryset = Escenario.objects.get(id=id_escenario)
        return queryset.id_mesa_tecnica

    def listarEscenarioPorId(self, id_escenario):
        escenario = Escenario.objects.get(id=id_escenario)
        return escenario

    def getEscenarioPorMesaTecnica(self, id_mesa_tecnica):
        queryset = Escenario.objects.filter(id_mesa_tecnica=id_mesa_tecnica)
        serializerEscenario = self.serializer_class(queryset, many=True)
        return serializerEscenario
###########################################################################################################################################
############################################################# ACTAS #######################################################################
# @Luis Mauricio Mosquera Cifuentes @date 25/02/2020
class DownloadActasViewSet(GeneralModelViewSet):
    model_db = ActasMesaTecnica
    #Función para descargar pdf relacionado con el acta
    def list(self, request):
        acta = ActasMesaTecnica.objects.get(id=request.query_params["id_acta"])
        short_report = open("media/"+str(acta.adjunto), 'rb')
        response = HttpResponse(FileResponse(short_report), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename='+str(acta.nombre)+".pdf"
        return response

class ActasMesaTecnicaViewSet(GeneralModelViewSet):
    model_db = ActasMesaTecnica
    queryset = ActasMesaTecnica.objects.all()
    serializer_class = ActasMesaTecnicaSerializer
    parser_class = (FileUploadParser,)
    #Listar actas
    def list(self, request):
        queryset = ActasMesaTecnica.objects.filter(id_mesa_tecnica=request.query_params["id_mesa_tecnica"])
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
    #Crear actas
    def create(self, request):
        self.generarNombreActa()
        serializer = self.serializer_class(data=request.data)  # request.datos son los datos de la petición
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def generarNombreActa(self):
        actas = ActasMesaTecnica.objects.filter(id_mesa_tecnica=self.request.data["id_mesa_tecnica"])
        mesaTecnica = MesaTecnica.objects.get(id=self.request.data["id_mesa_tecnica"])
        if actas:
            consecutivoActa = len(actas)+1
            self.request.data["nombre"]="Acta_"+str(consecutivoActa)+"_"+mesaTecnica.codigo
            return self.request.data["nombre"]
        else:
            self.request.data["nombre"] = "Acta_1_" + mesaTecnica.codigo
###########################################################################################################################################
########################################################### ESTADOS #######################################################################
# @Luis Mauricio Mosquera Cifuentes @date 25/02/2020
class SeguimientoEstadosViewSet(GeneralModelViewSet):
    serializer_class = SeguimientoEstadosSerializer
    model_db = SeguimientoEstados
    detail_rows_db = True
    queryset = model_db.objects.all()

    # @Luis Mauricio Mosquera Cifuentes @date: 25/02/2020
    def nuevoSeguimientoEstado(context, id_usuario):
        serializer_class = SeguimientoEstadosSerializer
        fecha_creacion = date.today() #Asignacion de la fecha de creacion
        context['fecha_creacion'] = fecha_creacion
        if id_usuario > 0:
            context['id_usuario'] = id_usuario
        serializer = serializer_class(data=context)
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo
            return True
        else:
            return False

    # @Luis Mauricio Mosquera Cifuentes @date: 03/03/2020
    def buscarSeguimientoporNombreTabla(nombre_tabla):
        serializer_class = SeguimientoEstadosSerializer
        queryset = SeguimientoEstados.objects.filter(nombre_tabla=nombre_tabla).order_by('fecha_creacion')
        serializer = serializer_class(queryset, many=True)
        return Response(serializer.data)

    # @Luis Mauricio Mosquera Cifuentes @date: 03/03/2020
    def getUltimosEstado(id_dato, nombre_tabla):
        queryset = SeguimientoEstados.objects.filter(id_dato=id_dato, nombre_tabla=nombre_tabla).last()
        return queryset

    # @Luis Mauricio Mosquera Cifuentes @date: 03/03/2020
    # Se valida el estado de una Mesa Tecnica o Escenario
    def buscarEstadoPorMesaTecnica(self, id_mesa_tenica):
        queryset = self.getUltimosEstado(id_mesa_tenica, 'mesa_tecnica')
        return queryset.id_estado

    # @Luis Mauricio Mosquera Cifuentes @date: 03/03/2020
    def buscarEstadoPorEscenarios(self, id_escenario):
        queryset = self.getUltimosEstado(id_escenario, 'escenario')
        return queryset.id_estado

    def definirContexto(self, id_dato, id_estado, asignador_por_sistema,  nombre_tabla):
        context = {'id_dato': id_dato, 'id_estado': id_estado, 'asignador_por_sistema': asignador_por_sistema, 'nombre_tabla': nombre_tabla}
        return context

    def destroy(self, request,pk=None):
        return Response({'detail':'Los Estados no se pueden eliminar...'},
                        status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

###########################################################################################################################################
############################################### REGLAS DE OPERACION #######################################################################
# @Luis Mauricio Mosquera Cifuentes @Date:18/03/2020
class CodigoReglasOperacionViewSet(GeneralModelViewSet):
    model_db = CodigoReglasOperacion
    queryset = CodigoReglasOperacion.objects.all()
    detail_rows_db = True
    serializer_class = CodigoReglasOperacionSerializer

    # @Jose Oliverio Patiño
    def list(self, request):  # Request recibe el id_escenario y el id_punto_interes
        puntosGestion = PuntoGestion.objects.all()
        nombre_variable=request.query_params['nombre_variable']
        id_escenario = request.query_params['id_escenario']
        datos = {}
        i = 0
        for item in puntosGestion:
            reglasOperacionEscenario = ReglaOperacionEscenario.objects.filter(
                id_codigo_reglas_operacion__id_variable_regla_operacion__id_punto_gestion=item.id,
                id_codigo_reglas_operacion__id_variable_regla_operacion__id_variable__nombre=nombre_variable,
                id_escenario=id_escenario)
            serializerReglasOperacionEscenario = ReglaOperacionEscenarioSerializer(reglasOperacionEscenario, many=True)


            #Consulta todos los grupos por punto de gestión X y variable X
            #Q(codigoreglasoperacion_set__id_escenario=request.data["id_escenario"]),
            datosRO = CodigoReglasOperacion.objects.filter(Q(
                id_variable_regla_operacion__id_variable__nombre=nombre_variable),Q(
                id_variable_regla_operacion__id_punto_gestion__id=item.id))
            serializer = self.serializer_class(datosRO, many=True)


            for itemCROE in serializerReglasOperacionEscenario.data:
                for itemCRO in serializer.data:
                    reglaOperacionEscenario = ReglaOperacionEscenario.objects.get(Q(
                        id_codigo_reglas_operacion__id_variable_regla_operacion__id_punto_gestion__id=item.id),
                        Q(id_codigo_reglas_operacion__id_variable_regla_operacion__id_variable__nombre=nombre_variable),
                        Q(id_escenario=id_escenario))
                    itemCRO["id_regla_operacion_escenario"]=reglaOperacionEscenario.id
                    if itemCRO["id"] == itemCROE["id_codigo_reglas_operacion"]:
                        itemCRO["selected"]=True
                    else:
                        itemCRO["selected"]=False
            datosVRO = VariableReglaOperacion.objects.filter(id_variable__nombre=nombre_variable,
                                                              id_punto_gestion__id=item.id)
            if datosVRO.exists():
                datosVRO = datosVRO[0]
                datos[i] = {}
                id_variable_regla_operacion = "id"
                datos[i][id_variable_regla_operacion]=datosVRO.id

                nombre_pg = "nombre_punto_gestion"
                datos[i][nombre_pg] = item.nombre

                codigos_reglas_operacion="codigos_reglas_operacion"
                datos[i][codigos_reglas_operacion]=serializer.data
                i+=1
        return Response(datos)

    # @Jose Oliverio Patiño
    def listarReglasOperacionInicial(self, request):
        queryset=CodigoReglasOperacion.objects.filter(escenario_base=True)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    # @Jose Oliverio Patiño
    def create(self, request):

        serializer = self.serializer_class(data=request.data)  # request.datos son los datos de la petición
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo
            self.asignarROporCodigo(serializer.data["id"])
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def asignarROporCodigo(self, id_Codigo_Reglas_Operacion):
        valoresRO = self.request.data["valores"]
        #Consultamos el id del código regla operación que acabamos de crear
        codigoReglaOperacion = CodigoReglasOperacion.objects.get(id=id_Codigo_Reglas_Operacion)
        #Hacemos un ciclo para duplicar los valores base en el grupo que acabamos de crear para una variable x de un punto de gestión x
        for item in valoresRO:
            VRO = ValoresReglasOperacion(
                valor=item["valor"],
                dia=item["dia"],
                mes=item["mes"],
                id_codigo_regla_operacion=codigoReglaOperacion
            )
            VRO.save()

# @Luis Mauricio Mosquera Cifuentes @Date:09/03/2020
class ReglasOperacionEscenarioViewSet(GeneralModelViewSet):
    model_db = ReglaOperacionEscenario
    queryset = ReglaOperacionEscenario.objects.all()
    detail_rows_db = True
    serializer_class = ReglaOperacionEscenarioSerializer

    #Jose Oliverio Patiño Castaño
    def crearReglasOperacionIniciales(self, id_escenario):
        isnCodigoReglaOperacion = CodigoReglasOperacionViewSet()
        reglasOperacionIniciales = isnCodigoReglaOperacion.listarReglasOperacionInicial(CodigoReglasOperacionViewSet)

        escenario = Escenario.objects.get(id=id_escenario)
        for i in reglasOperacionIniciales.data:
            idCodigoReglaOperacion = CodigoReglasOperacion.objects.get(id=i["id"])
            dataReglasOperacionEscenario = ReglaOperacionEscenario(
                id_escenario = escenario,
                id_codigo_reglas_operacion = idCodigoReglaOperacion
            )
            dataReglasOperacionEscenario.save()

    def create(self, request):
        reglasOperacionEscenarios = self.listarReglasOperacionporEscenario(request.data["id_escenario"])
        serializerROE = ReglaOperacionEscenarioSerializer(reglasOperacionEscenarios, many=True)
        escenario = Escenario.objects.get(id=request.data["id_escenario"])
        ids = []
        for item in serializerROE.data:
            ids.append(item["id_codigo_reglas_operacion"])

        count = 0
        for item2 in request.data["datos"]:
            if item2["id_codigo_reglas_operacion"]!=ids[count]:
                reglaOE = ReglaOperacionEscenario.objects.get(id=item2["id"])

                codigoReglaOperacion = CodigoReglasOperacion.objects.get(id=item2["id_codigo_reglas_operacion"])
                reglaOE.id_codigo_reglas_operacion = codigoReglaOperacion
                reglaOE.id_escenario = escenario
                reglaOE.save()
            count += 1

        reglasOperacionEscenarios2 = self.listarReglasOperacionporEscenario(request.data["id_escenario"])
        serializerROE2 = ReglaOperacionEscenarioSerializer(reglasOperacionEscenarios2, many=True)
        return Response(serializerROE2.data)


    def listarReglasOperacionporEscenario(self, id_escenario):
        reglasOperacionEscenario=ReglaOperacionEscenario.objects.filter(id_escenario=id_escenario)
        return reglasOperacionEscenario

    def destroy(self, request,pk=None):
        return Response({'detail':'Las Reglas de Operacion no se puede eliminar...'},
                        status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

# ReglaOperacionInicial View
# clase encarga de generar la vista del modelo ReglaOperacionInicial
# @Jose Oliverio Patiño Castaño  @date 03/02/2020

class ValoresReglasOperacionViewSet(GeneralModelViewSet):
    model_db = ValoresReglasOperacion
    queryset = ValoresReglasOperacion.objects.all()
    serializer_class = ValoresReglasOperacionSerializer

    def create(self, request):
        datos = []
        for i in request.data:
            valorReglaOperacion = ValoresReglasOperacion.objects.get(id=i["id"])
            valorReglaOperacion.valor = i["valor"]
            valorReglaOperacion.save()
            datos.append(i)
        return Response(datos)

    def list(self, request):
        import pandas as pd
        fecha = pd.to_datetime(request.query_params["fecha"])
        anio = fecha.year
        mes = fecha.month

        queryset = ValoresReglasOperacion.objects.filter(id_codigo_regla_operacion=request.query_params["id_codigo_regla_operacion"])
        serializer = self.serializer_class(queryset, many=True)
        for item in serializer.data:
            if mes==1:
                anioNew=anio
            else:
                if(item["mes"]<mes):
                    anioNew=anio+1
                else:
                    anioNew = anio
            item["fecha"] = str(anioNew)+"-"+str(item["mes"])+"-"+str(item["dia"])
            item["fecha"] = pd.to_datetime(item["fecha"], format="%Y/%m/%d")
            item["fechaiu"] = str(anioNew)+"-"+str(item["mes"])+"-"+str(item["dia"])

        datosOrdenados = sorted(serializer.data, key=lambda x: x['fecha'],
                        reverse=False)
        return Response(datosOrdenados)
