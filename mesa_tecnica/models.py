from django.db import models
from configuraciones_basicas.models import *

JWT_PAYLOAD_HANDLER= api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER= api_settings.JWT_ENCODE_HANDLER
######################################################################################################
############################################ ESTADOS #################################################
# Seguimiento a Estados
# @Luis Mauricio Mosquera Cifuentes @date 18/02/2020
# Clase donde e permite realizar un seguimiento a cada estado en cada mesa tecnica o escenario



class SeguimientoEstados(models.Model):
    id = models.AutoField(primary_key=True)
    id_dato = models.IntegerField(blank=False, null=False)
    id_estado = models.ForeignKey(Estado, to_field='id', db_column='id_estado', on_delete=models.DO_NOTHING)
    nombre_tabla = models.CharField(max_length=100, blank=False, null=False)
    fecha_creacion = models.DateField(blank=False, null=False)
    id_usuario = models.IntegerField(blank=True, null=True) #Se debe convertir a Foreign Key con la tabla de Usuario
    asignador_por_sistema = models.SmallIntegerField(blank=False, null=False) # Cuando el estado no es asignado por el sistema sino por el usuario, es 0; cuando el estado es asignado por el sistema, es 1.

    class Meta:
        db_table = 'seguimiento_estados'
        ordering = ['id', 'fecha_creacion', 'nombre_tabla']

    @staticmethod
    def get_Atributos():
        return (['id', 'id_dato', 'id_estado', 'nombre_tabla', 'fecha_creacion', 'id_usuario', 'asignador_por_sistema'])

    @staticmethod
    def get_Filter(params):
        data = {}
        for param in params:
            data[param + '_contains'] = params.get(param)
        return data


######################################################################################################
######################################### MESAS TECNICAS #############################################
# Tipo Mesa tecnica
# clase encargada de definir las propiedades de la tabla tipo_mesa_tecnica en la base de datos
# @Jose Oliverio Patiño Castaño  @date 29/01/2020
class TipoMesaTecnica(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=45, blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'tipo_mesa_tecnica'
        ordering = ['id']

    @staticmethod
    def get_Atributos():
        return (['id', 'nombre'])

    @staticmethod
    def get_Filter(params):
        data={};
        for params in params:
            data[params + '__contains'] = params.get(params)
        return data

# Mesa tecnica Bio Ambiental
# clase encargada de definir las propiedades de la tabla mesa_tecnica en la base de datos
# @Jose Oliverio Patiño Castaño  @date 29/01/2020

class MesaTecnica(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=150, blank=False, null=False)
    fecha_creacion = models.DateTimeField(null=False)
    fecha_cierre = models.DateTimeField(null=True)
    fecha_inicio_simulacion = models.DateField(blank=False, null=False)
    fecha_fin_simulacion = models.DateField(blank=False, null=False)
    descripcion = models.CharField(max_length=500, blank=False, null=False)
    id_tipo_mesa_tecnica = models.ForeignKey(TipoMesaTecnica, to_field='id', db_column='id_tipo_mesa_tecnica', on_delete=models.DO_NOTHING)
    id_mesa_tecnica_principal = models.ForeignKey('self', to_field='id', db_column='id_mesa_tecnica_principal', null=True, on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'mesa_tecnica'
        ordering = ['id']

    def __str__(self):
        return self.codigo

    @staticmethod
    def get_Atributos():
        return (['id', 'codigo', 'fecha_creacion', 'fecha_cierre', 'fecha_inicio_simulacion',
                 'fecha_fin_simulacion', 'descripcion', 'id_tipo_mesa_tecnica', 'id_mesa_tecnica_principal'])

    @staticmethod
    def get_Filter(params):
        datosTabla = MesaTecnica.get_Atributos()
        data = {};
        for param in params:
            if param in datosTabla:
                if param == 'id_tipo_mesa_tecnica':
                    data[param] = params.get(param)
                else:
                    data[param + '__contains'] = params.get(param)
        return data


######################################################################################################
############################################ ESCENARIOS ##############################################
# Escenarios
# clase encargada de definir las propiedades de la tabla escenarios en la base de datos
# @Jose Oliverio Patiño Castaño  @date 30/01/2020

class Escenario(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=45, blank=False, null=False)
    descripcion = models.TextField(max_length=500, null=True, blank=True)
    fecha_aprobacion = models.DateField(null=True)
    base = models.IntegerField(null=False, blank=False)
    fecha_creacion = models.DateField(null=True)
    id_mesa_tecnica = models.ForeignKey(MesaTecnica, to_field='id', db_column='id_mesa_tecnica', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'escenarios'

    @staticmethod
    def get_Atributos():
        return (['id', 'codigo', 'descripcion', 'fecha_aprobacion', 'base', 'fecha_creacion', 'id_mesa_tecnica'])

    @staticmethod
    def get_Filter(params):
        datosTabla = Escenario.get_Atributos()
        data = {};
        for param in params:
            if param in datosTabla:
                if param == 'id_mesa_tecnica':
                    data[param] = params.get(param)
                else:
                    data[param + '__contains'] = params.get(param)
        return data


######################################################################################################
################################ REGLAS DE OPERACION #################################################
# Codigo Reglas Operacion
# @Luis Mauricio Mosquera Cifuentes @Date: 18/03/2020
class CodigoReglasOperacion(models.Model):
    id = models.AutoField(primary_key = True)
    codigo = models.CharField(max_length=50, blank=False, null=False)
    descripcion = models.CharField(max_length=500, blank=True, null=True)
    escenario_base = models.IntegerField(blank=True, null=True)
    id_variable_regla_operacion = models.ForeignKey(VariableReglaOperacion, to_field='id', db_column='id_variable_regla_operacion', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'codigos_reglas_operacion'

    @staticmethod
    def get_Atributos():
        return (['id', 'codigo', 'descripcion', 'escenario_base', 'id_variable_regla_operacion'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# ReglaOperacionEscenario
# clase encargada de definir las propiedades de la tabla reglas_operacion_escenarios en la base de datos
# @Jose Oliverio Patiño Castaño  @date 31/01/2020
class ReglaOperacionEscenario(models.Model):
    id = models.AutoField(primary_key=True)
    id_codigo_reglas_operacion = models.ForeignKey(CodigoReglasOperacion, to_field='id', db_column='id_codigo_reglas_operacion', on_delete=models.DO_NOTHING, related_name='codigoreglasoperacion_set')
    id_escenario = models.ForeignKey(Escenario, to_field='id', db_column='id_escenario', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'reglas_operacion_escenarios'

    @staticmethod
    def get_Atributos():
        return (['id', 'id_codigo_reglas_operacion', 'id_escenario'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data

######################################################################################################
############################################ ACTAS ###################################################
# Actas
# @Luis Mauricio Mosquera Cifuentes @date 02/02/2020
# Registros que son generados como soportes para cada reunion de cada Mesa Tecnica
class ActasMesaTecnica(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=150, blank=False, null=False)
    descripcion = models.CharField(max_length=500, blank=False, null=False)
    fecha_creacion = models.DateTimeField(blank=False, null=False)
    adjunto = models.FileField(upload_to="actas/", null=True, blank=True)
    id_mesa_tecnica = models.ForeignKey(MesaTecnica, to_field='id', db_column='id_mesa_tecnica', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'actas_mesa_tecnica'
        ordering = ['nombre']

    @staticmethod
    def get_Atributos():
        return (['id', 'nombre', 'descripcion', 'fecha_creacion', 'adjunto', 'id_mesa_tecnica'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# ReglaOperacionInicial
# clase encargada de definir las propiedades de la tabla reglas_operacion_iniciales en la base de datos
# @Jose Oliverio Patiño Castaño  @date 31/01/2020

class ValoresReglasOperacion(models.Model):
    id = models.AutoField(primary_key=True)
    valor = models.CharField(max_length=50, blank=False, null=False)
    dia = models.IntegerField(blank=False, null=False)
    mes = models.IntegerField(blank=False, null=False)
    id_codigo_regla_operacion = models.ForeignKey(CodigoReglasOperacion, to_field='id', db_column='id_codigo_regla_operacion',
                                     on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'valores_reglas_operacion'

    def __str__(self):
        return self.valor

    @staticmethod
    def get_Atributos():
        return (['id', 'valor', 'dia', 'mes', 'id_codigo_regla_operacion'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data
