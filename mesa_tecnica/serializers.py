from .models import *
from rest_framework import serializers

import configuraciones_basicas


# tutorial 4 rest_framework Modelserializer

###########################################################################################################################################
######################################################### MESAS TECNICAS ##################################################################
# TipoMesaTecnicaSerializer
# clase encargarda de serializar el modelo TipoMesaTecnica
# @Jose Oliverio Patiño Castaño  @date 29/01/2020
class TipoMesaTecnicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoMesaTecnica
        fields = TipoMesaTecnica.get_Atributos()

# MesaTecnicaSerializer
# clase encargarda de serializar el modelo MesaTecnica
# @Jose Oliverio Patiño Castaño  @date 29/01/2020

class MesaTecnicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = MesaTecnica
        fields = MesaTecnica.get_Atributos()
        #fecha_creacion = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", read_only=True)
        #fecha_cierre = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", read_only=True)


######################################################################################################
############################################ ESCENARIOS #################################################
# EscenariosSerializer
# clase encargarda de serializar el modelo Escenarios
# @Jose Oliverio Patiño Castaño  @date 29/01/2020
class EscenarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Escenario
        fields = Escenario.get_Atributos()
        #fecha_creacion = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", read_only=True)


######################################################################################################
################################ REGLAS DE OPERACION #################################################
# ReglaOperacionEscenarioSerializer
# @Luis Mauricio Mosquera Cifuentes  @date 18/03/2020
class CodigoReglasOperacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CodigoReglasOperacion
        fields = CodigoReglasOperacion.get_Atributos()

# ReglaOperacionEscenarioSerializer
# clase encargarda de serializar el modelo ReglaOperacionEscenario
# @Jose Oliverio Patiño Castaño  @date 03/02/2020
class ReglaOperacionEscenarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReglaOperacionEscenario
        fields = ReglaOperacionEscenario.get_Atributos()


######################################################################################################
########################################## ESTADOS ###################################################
# Estados Serializer
# @Luis Mauricio Mosquera Cifuentes @Date: 01/03/2020
class EstadosSerializar(serializers.ModelSerializer):
    class Meta:
        model = configuraciones_basicas.models.Estado
        fields = configuraciones_basicas.models.Estado.get_Atributos()

# Seguimiento Estados Serializar
# clase encargarda de serializar el modelo Seguimiento Estados
# @Luis Mauricio Mosquera Cifuentes  @date 20/02/2020
class SeguimientoEstadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeguimientoEstados
        fields = SeguimientoEstados.get_Atributos()
        #fields = ('id_dato', 'id_estado', 'nombre_tabla', 'fecha_creacion', 'id_usuario', 'asignador_por_sistema') #No se colocan todos los atributos, porque en ocasiones no los lleva


######################################################################################################
############################################ ACTAS ###################################################
# Actas Mesa Tecnica Serializer
# clase encargarda de serializar el modelo ActasMesaTecnica
# @Luis Mauricio Mosquera Cifuentes  @date 03/02/2020
class ActasMesaTecnicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActasMesaTecnica
        fields = ActasMesaTecnica.get_Atributos()
        fecha_creacion = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%S", read_only=True)

# ReglaOperacionInicialSerializer
# clase encargarda de serializar el modelo ValoresReglasOperacion
# @Jose Oliverio Patiño Castaño  @date 03/02/2020
class ValoresReglasOperacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ValoresReglasOperacion
        fields = ValoresReglasOperacion.get_Atributos()
