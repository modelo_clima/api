import base64
import io
import math
from statistics import mean

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from algoritmos.models import DatosValidados, ValoresMediaMovil, Variable
from configuraciones_basicas.models import Estacion, TrimestreMediaMovil, UnidadMedida


class CalculosDatosValidados():

    @staticmethod
    def getDatosMensual(nom_variable, nom_estacion):
        datos = {}
        variable = Variable.objects.get(nombre=nom_variable)
        estacion = Estacion.objects.get(nombre=nom_estacion)
        datosValidados = DatosValidados.objects.filter(id_variable=variable.id, id_estacion=estacion.id)
        for item in datosValidados:
            mes = {item.fecha.month: {'acum': item.valor, 'dias': 1, 'prom': item.valor}}
            if item.fecha.year in datos:
                mes = datos[item.fecha.year]
                if item.fecha.month in datos[item.fecha.year]:
                    acum = mes[item.fecha.month]['acum'] + item.valor
                    dias = mes[item.fecha.month]['dias'] + 1
                    prom = acum / dias
                    mes[item.fecha.month] = {'acum': acum, 'dias': dias, 'prom': prom}
                else:
                    mes[item.fecha.month] = {'acum': item.valor, 'dias': 1, 'prom': item.valor}
            datos[item.fecha.year] = mes
        return datos

    @staticmethod
    def getArrayDatosMensual(nom_variable, nom_estacion):
        datosMensuales = CalculosDatosValidados.getDatosMensual(nom_variable, nom_estacion)
        datos = {}
        i = 0
        for item_year in datosMensuales:
            for item_month in datosMensuales[item_year]:
                tipo_valor = 'acum'
                if nom_variable == 'Temperatura':
                    tipo_valor = 'prom'
                valor = datosMensuales[item_year][item_month][tipo_valor];
                datos[i] = {'year': item_year, 'month': item_month, 'value': valor}
                i = i + 1
        return datos

    @staticmethod
    def getMediaMovil(nom_variable, nom_estacion):
        datos = {}
        datosMensuales = CalculosDatosValidados.getArrayDatosMensual(nom_variable, nom_estacion)
        i = 0
        while (i < len(datosMensuales)):
            prom = 0
            d = {}
            if 0 < i < (len(datosMensuales) - 1):
                valor = (datosMensuales[i - 1]['value'] + datosMensuales[i]['value'] + datosMensuales[i + 1]['value'])
                prom = valor / 3
                if (i + 1) == (len(datosMensuales) - 1) and nom_variable == 'Precipitación':
                    valor = (datosMensuales[i - 1]['value'] + datosMensuales[i]['value'] + datosMensuales[i + 1][
                        'value'])
                    prom = valor / 2
            elif i == 0 and nom_variable == 'Temperatura':
                valor = (datosMensuales[i]['value'] + datosMensuales[i + 1]['value'])
                prom = 0
            elif i == (len(datosMensuales) - 1):
                valor = (datosMensuales[i - 1]['value'] - datosMensuales[i]['value'])
                prom = 0
                break;
            datos[i] = {'year': datosMensuales[i]['year'], 'month': datosMensuales[i]['month'],
                        'media_movil': prom, 'datos': d}
            i = i + 1
        return datos

    @staticmethod
    def getMeses():
        meses = ['Ene', 'Febr', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Ocu', 'Nov', 'Dic']
        return meses

    @staticmethod
    def getGrafica(nom_variable, nom_estacion, periodo, fechas, simulacion):
        variable = Variable.objects.get(nombre=nom_variable)
        estacion = Estacion.objects.get(nombre=nom_estacion)
        unidaMedida = variable.id_unidad_medida
        tituloGrafico = nom_variable + ' media diaria durante el año de predicción ' + simulacion[0] + ' al ' + \
                        simulacion[1]
        label_x = "Temperatura promedio anual: "
        if not nom_variable == 'Temperatura':
            tituloGrafico = nom_variable + ' diaria durante el año de predicción ' + simulacion[0] + ' al ' + \
                            simulacion[1]
            label_x = "Precipitación acumulada anual: "
        labelEjeY = nom_variable + ' (' + unidaMedida.simbolo + ')'
        labelEjeX = 'Días'
        valores_diarios = []
        x = []  # tiempo (meses, días)
        y = []  # valor
        if periodo == 'mensual':
            labelEjeX = 'Meses'
            if nom_variable == 'Temperatura':
                tituloGrafico = nom_variable + ' promedio mensual durante el año de predicción ' + simulacion[
                    0] + ' al ' + simulacion[1]
            else:
                tituloGrafico = nom_variable + '  total mensual durante el año de predicción ' + simulacion[
                    0] + ' al ' + simulacion[1]
                labelEjeY = nom_variable + ' (mm/m)'
            meses = CalculosDatosValidados.getMeses()
            datosMensuales = CalculosDatosValidados.getArrayDatosMensual(nom_variable=nom_variable,
                                                                         nom_estacion=nom_estacion)

            for fecha in fechas:
                x.append([])
                y.append([])
                valores_diarios.append([])
                for index in datosMensuales:
                    item = datosMensuales[index]
                    if len(x[len(x) - 1]) == 12:
                        break
                    elif (int(item['year']) == fecha.year and int(item['month']) >= fecha.month) or (
                            int(item['year']) >= fecha.year):
                        mes = meses[int(item['month']) - 1]
                        y[len(y) - 1].append(item['value'])
                        x[len(x) - 1].append(len(x[len(x) - 1]) + 1)

                datosValidados = DatosValidados.objects.filter(id_variable=variable.id, id_estacion=estacion.id,
                                                               fecha__gte=fecha)
                for item in datosValidados:
                    if len(valores_diarios[len(valores_diarios) - 1]) == 365:
                        break
                    else:
                        valores_diarios[len(valores_diarios) - 1].append(item.valor)
            if len(x) == 0:
                return {'msg': 'Datos incompletos', 'estado': False}

        else:
            for fecha in fechas:
                x.append([])
                y.append([])
                valores_diarios.append([])
                datosValidados = DatosValidados.objects.filter(id_variable=variable.id, id_estacion=estacion.id,
                                                               fecha__gte=fecha)
                if len(datosValidados) == 0:
                    return {'msg': 'Datos incompletos', 'estado': False}
                for item in datosValidados:
                    if len(x[len(x) - 1]) == 365:
                        break
                    else:
                        y[len(y) - 1].append(item.valor)
                        x[len(x) - 1].append(len(x[len(x) - 1]) + 1)
                        valores_diarios[len(valores_diarios) - 1].append(item.valor)
            '''
            datosValidados = DatosValidados.objects.filter(id_variable=variable.id, id_estacion=estacion.id,
                                                           fecha__gte=fechaInicio, fecha__lte=mydate)
            '''

        colores = None
        if nom_variable == 'Temperatura':
            colores = ["#7D7900", "#545100", "#B2AC00"]
        else:
            colores = ["#00104F", "#001876", "#465076"]

        max_y = 0
        min_y = 0
        for i in range(0, len(y)):
            a = max(y[i])
            b = min(y[i])
            acm = round(sum(valores_diarios[i]), 3)
            prom = round(mean(valores_diarios[i]), 3)
            if max_y < a or i == 0:
                max_y = a
            if min_y > b or i == 0:
                min_y = b
            if not nom_variable == 'Temperatura':
                label_x = label_x + "Serie " + str(i + 1) + "=" + str(acm) + " "
            else:
                label_x = label_x + "Serie " + str(i + 1) + "=" + str(prom) + " "

        if not nom_variable == 'Temperatura':
            max_y = max_y + 2
            min_y = min_y - 2

        # Creamos los datos para representar en el gráfico
        # Creamos una figura y le dibujamos el gráfico
        fig = plt.figure(figsize=[12, 7])
        if periodo == 'mensual':
            # Creamos los ejes
            # axes = fig.add_axes([0.15, 0.15, 0.75, 0.5])  # [left, bottom, width, height]
            width_bar = 0.25
            x_bar = -width_bar
            for i in range(0, len(x)):
                fecha = fechas[i]
                color = colores[i]
                label = "Serie " + str(i + 1) + ": " + str(fecha.year)
                x_d = np.array(x[i]).astype('float')
                plt.bar(x_d + x_bar, y[i], width=width_bar, label=label, color=color)
                x_bar = x_bar + width_bar
            plt.xlabel(labelEjeX + "\n " + label_x)
            plt.ylabel(labelEjeY)
            plt.title(tituloGrafico)
            plt.legend(loc="upper right")
            plt.xlim(0, len(x[0]) + 1)
        else:
            fig, ax = plt.subplots(len(x), figsize=[12, 8])

            for i in range(0, len(x)):
                fecha = fechas[i]
                color = colores[i]
                label = "Serie " + str(i + 1) + ": " + str(fecha.year)
                plt.subplots_adjust(top=0.9)
                ax[i].plot(x[i], y[i], marker='.', linestyle='-', label=label, color=color)
                if i == (len(x) - 1):
                    ax[i].set(xlabel=labelEjeX + "\n " + label_x, ylabel=labelEjeY)
                else:
                    ax[i].set(ylabel=labelEjeY)
                ax[i].axis(ymin=min_y, ymax=max_y)
            fig.suptitle(tituloGrafico)
            fig.legend(loc="upper right")

        # Como enviaremos la imagen en bytes la guardaremos en un buffer
        buf = io.BytesIO()
        fig.savefig(buf, format="png")
        data = base64.b64encode(buf.getbuffer()).decode("ascii")
        fig.clear()
        # return f'<img src="data:image/png;base64,{data}" class="grafica"/>'
        return {'img': str(f"data:image/png;base64,{data}"), 'estado': True}

    @staticmethod
    def getGraficaONI(fechas):
        variable = Variable.objects.get(nombre='ONI')
        unidaMedida = variable.id_unidad_medida
        tituloGrafico = 'Gráfico series ONI'
        labelEjeY = unidaMedida.nombre + ' (' + unidaMedida.simbolo + ')'
        labelEjeX = 'Meses'
        x = []  # tiempo (meses, días)
        y = []  # valor

        for fecha in fechas:
            x.append([])
            y.append([])
            valoresMediaMovil = ValoresMediaMovil.objects.filter(id_variable=variable.id,
                                                                 temporada__gte=fecha.year, mes__gte=fecha.month)
            if len(valoresMediaMovil) == 0:
                return {'msg': 'Datos incompletos', 'estado': False}
            for item in valoresMediaMovil:
                if len(x[len(x) - 1]) == 12:
                    break
                else:
                    y[len(y) - 1].append(item.valor)
                    x[len(x) - 1].append(len(x[len(x) - 1]) + 1)

        # Creamos los datos para representar en el gráfico

        # Creamos una figura y le dibujamos el gráfico
        fig = plt.figure(figsize=[12, 7])
        # Creamos los ejes
        # axes = fig.add_axes([0.15, 0.15, 0.75, 0.5])  # [left, bottom, width, height]
        for i in range(0, len(x)):
            color = "g"
            if i == 1:
                color = "b"
            elif i > 0:
                color = "r"
            label = "Serie " + str(i + 1)
            plt.plot(x[i], y[i], linestyle='-', label=label, color=color)
        plt.xlabel(labelEjeX)
        plt.ylabel(labelEjeY)
        plt.title(tituloGrafico)
        plt.legend(loc="upper right")
        plt.xlim(1, len(x[0]))

        # Como enviaremos la imagen en bytes la guardaremos en un buffer
        buf = io.BytesIO()
        fig.savefig(buf, format="png")
        data = base64.b64encode(buf.getbuffer()).decode("ascii")
        fig.clear()
        # return f'<img src="data:image/png;base64,{data}" class="grafica"/>'
        return {'img': str(f"data:image/png;base64,{data}"), 'estado': True}

    @staticmethod
    def getGraficarDatos(id_variable, datosSerealizer):
        variable = Variable.objects.get(pk=id_variable)
        unidaMedida = variable.id_unidad_medida
        tituloGrafico = 'Gráfico diario de ' + variable.nombre
        labelEjeY = unidaMedida.nombre + ' (' + unidaMedida.simbolo + ')'
        labelEjeX = 'Días'
        x = []  # tiempo (meses, días)
        y = []  # valor
        for dato in datosSerealizer:
            x.append(len(x))
            y.append(dato['valor'])

        fig = plt.figure(figsize=[12, 7])
        plt.plot(x, y, linestyle='-', color="g")
        plt.xlabel(labelEjeX)
        plt.ylabel(labelEjeY)
        plt.title(tituloGrafico)
        plt.legend(loc="upper right")
        plt.xlim(0, len(x))

        # Como enviaremos la imagen en bytes la guardaremos en un buffer
        buf = io.BytesIO()
        fig.savefig(buf, format="png")
        data = base64.b64encode(buf.getbuffer()).decode("ascii")
        fig.clear()
        # return f'<img src="data:image/png;base64,{data}" class="grafica"/>'
        return {'img': str(f"data:image/png;base64,{data}"), 'estado': True}


class ModeloClima():

    def guardarMediaMovil(self, nom_variable, nom_estacion):
        variable = Variable.objects.get(nombre=nom_variable)
        estacion = Estacion.objects.get(nombre=nom_estacion)

        mediaMovil = CalculosDatosValidados.getMediaMovil(nom_variable, nom_estacion)
        for item in mediaMovil:
            datos_bd = ValoresMediaMovil.objects.filter(mes=mediaMovil[item]['month'],
                                                        temporada=mediaMovil[item]['year'],
                                                        id_variable=variable.id, id_estacion=estacion.id,
                                                        id_trimestre_media_movil=mediaMovil[item]['month'])
            trimestre = TrimestreMediaMovil.objects.get(pk=mediaMovil[item]['month'])
            valoresMediaMovil = ValoresMediaMovil()
            if (len(datos_bd) > 0):
                valoresMediaMovil = ValoresMediaMovil.objects.get(pk=datos_bd[0].id)
            else:
                valoresMediaMovil.mes = mediaMovil[item]['month']
                valoresMediaMovil.temporada = mediaMovil[item]['year']
                valoresMediaMovil.id_variable = variable
                valoresMediaMovil.id_estacion = estacion
                valoresMediaMovil.id_trimestre_media_movil = trimestre
            valoresMediaMovil.valor = mediaMovil[item]['media_movil']
            valoresMediaMovil.save()

    def getAnomalias(self, nom_variable, nom_estacion=None):
        valoresMediaMovil = None
        datosMMT = {}
        trimestres = TrimestreMediaMovil.objects.all()
        variable = Variable.objects.get(nombre=nom_variable)
        if (nom_estacion is not None):
            estacion = Estacion.objects.get(nombre=nom_estacion)
            valoresMediaMovil = ValoresMediaMovil.objects.filter(id_variable=variable.id, id_estacion=estacion.id)
        else:
            valoresMediaMovil = ValoresMediaMovil.objects.filter(id_variable=variable.id)

        for trimestre in trimestres:
            datosMMT[trimestre.id] = []

        i = 0
        for item in valoresMediaMovil:
            trimestre = item.id_trimestre_media_movil
            valor = valoresMediaMovil[i].valor
            if i == 0 and item.valor == 0:
                if nom_variable == 'Precipitación':
                    valor = '-'
                else:
                    i = i + 1
                    valor = valoresMediaMovil[i].valor

            datosMMT[trimestre.id].append({'year': item.temporada, 'valor': valor, 'prom': 0, 'anomalia': 0})
            i = i + 1
            if i >= len(valoresMediaMovil):
                break
        for trimestre in datosMMT:
            acum = 0
            numDatos = len(datosMMT[trimestre])
            for v in datosMMT[trimestre]:
                if str(v['valor']) == '-':
                    numDatos = numDatos - 1
                else:
                    acum = acum + v['valor']
            prom = acum / numDatos
            z = 0
            for v in datosMMT[trimestre]:
                valor = 0
                if not str(v['valor']) == '-':
                    if nom_variable == 'Precipitación':
                        valor = v['valor'] / prom
                    elif nom_variable == 'Temperatura':
                        valor = v['valor'] - prom
                    else:
                        valor = v['valor']
                datosMMT[trimestre][z]['anomalia'] = valor
                datosMMT[trimestre][z]['prom'] = prom
                z = z + 1
        return datosMMT

    def getArrayAnomalias(self, nom_variable, nom_estacion=None):
        anomalias = self.getAnomalias(nom_variable, nom_estacion)
        datos = {}
        datosYear = {}
        for trimestre in anomalias:
            for item in anomalias[trimestre]:
                v = {'year': item['year'], 'trimestre': trimestre, 'valor': item['valor'], 'prom': item['prom'],
                     'anomalia': item['anomalia']}
                if not item['year'] in datosYear:
                    datosYear[item['year']] = []
                datosYear[item['year']].append(v)
        index = 0
        for i in datosYear:
            for v in datosYear[i]:
                datos[index] = v
                index = index + 1
        return datos

    def getCalcularMesesDist(self, year_date, mont_date, nom_estacion):
        anomalias_t = self.getArrayAnomalias('Temperatura', nom_estacion)
        anomalias_p = self.getArrayAnomalias('Precipitación', nom_estacion)
        anomalias_oni = self.getArrayAnomalias('ONI')
        if len(anomalias_t) == 0 or len(anomalias_p) == 0 or len(anomalias_oni) == 0:
            raise ValueError('Datos incompletos')
        else:
            lastTemp = anomalias_t[len(anomalias_t) - 1]
            lastPrec = anomalias_p[len(anomalias_p) - 1]
            lastOni = anomalias_oni[len(anomalias_oni) - 1]
            if (lastTemp['year'] < year_date or (
                    lastTemp['year'] == year_date and lastTemp['trimestre'] < mont_date)) or (
                    lastPrec['year'] < year_date or (
                    lastPrec['year'] == year_date and lastPrec['trimestre'] < mont_date)) or (
                    lastOni['year'] < year_date or (
                    lastOni['year'] == year_date and lastOni['trimestre'] < mont_date)):
                raise ValueError('Datos incompletos')

        i = len(anomalias_oni) - 1
        meses = {'temperatura': {}, 'precipitacion': {}, 'oni': {}}
        while i >= 0:
            if anomalias_oni[i]['year'] == year_date and anomalias_oni[i]['trimestre'] == mont_date:
                trimestre_inicial_oni = i - 2
                y = 0
                for x in range(i - 5, i):
                    meses['temperatura'][y] = {'year': anomalias_t[x]['year'],
                                               'trimestre': anomalias_t[x]['trimestre'],
                                               'anomalia': anomalias_t[x]['anomalia']}
                    meses['precipitacion'][y] = {'year': anomalias_p[x]['year'],
                                                 'trimestre': anomalias_p[x]['trimestre'],
                                                 'anomalia': anomalias_p[x]['anomalia']}
                    y = y + 1
                y = 0
                for x in range(trimestre_inicial_oni - 5, trimestre_inicial_oni):
                    meses['oni'][y] = {'year': anomalias_oni[x]['year'], 'trimestre': anomalias_oni[x]['trimestre'],
                                       'anomalia': anomalias_oni[x]['valor']}
                    y = y + 1
                break
            i = i - 1
        if len(meses['temperatura']) == 0:
            raise ValueError('Datos incompletos')

        datos = {}
        for index in anomalias_oni:
            val_t = anomalias_t[index + 2]['anomalia']
            val_p = anomalias_p[index + 2]['anomalia']
            val_oni = anomalias_oni[index]['anomalia']

            raiz_a = math.sqrt(
                pow(val_p - meses['precipitacion'][4]['anomalia'], 2) + pow(val_oni - meses['oni'][4]['anomalia'],
                                                                            2) + pow(
                    val_t - meses['temperatura'][4]['anomalia'], 2))
            raiz_b = math.sqrt(
                pow(val_p - meses['precipitacion'][3]['anomalia'], 2) + pow(val_oni - meses['oni'][3]['anomalia'],
                                                                            2) + pow(
                    val_t - meses['temperatura'][3]['anomalia'], 2))
            raiz_c = math.sqrt(
                pow(val_p - meses['precipitacion'][2]['anomalia'], 2) + pow(val_oni - meses['oni'][2]['anomalia'],
                                                                            2) + pow(
                    val_t - meses['temperatura'][2]['anomalia'], 2))
            raiz_d = math.sqrt(
                pow(val_p - meses['precipitacion'][1]['anomalia'], 2) + pow(val_oni - meses['oni'][1]['anomalia'],
                                                                            2) + pow(
                    val_t - meses['temperatura'][1]['anomalia'], 2))
            raiz_e = math.sqrt(
                pow(val_p - meses['precipitacion'][0]['anomalia'], 2) + pow(val_oni - meses['oni'][0]['anomalia'],
                                                                            2) + pow(
                    val_t - meses['temperatura'][0]['anomalia'], 2))
            distancia = round(raiz_a + raiz_b + raiz_c + raiz_d + raiz_e, 2)

            datos[index] = {'year': anomalias_t[index]['year'],
                            'trimestre': anomalias_t[index]['trimestre'], 'temp': anomalias_t[index + 2]['anomalia'],
                            'prec': anomalias_p[index + 2]['anomalia'],
                            'oni': anomalias_oni[index]['anomalia'], 'distancia': distancia}
            if index == (i - 1):
                break

        return {'anomalias': datos, 'meses': meses}

    def getSeries(self, year_date, month_date, nom_estacion):
        trimestres = {}
        datos = self.getCalcularMesesDist(year_date, month_date, nom_estacion)

        for i in datos['meses']['oni']:
            trimestres[datos['meses']['oni'][i]['trimestre']] = datos['meses']['oni'][i]['trimestre']
        for i in datos['meses']['temperatura']:
            if not datos['meses']['temperatura'][i]['trimestre'] in trimestres:
                trimestres[datos['meses']['temperatura'][i]['trimestre']] = datos['meses']['temperatura'][i][
                    'trimestre']

        acumulados_meses_distancia = {}
        acumulado_mes = {'acumulado': 0, 'trimestres': {}}
        x = 0
        for anomalia in datos['anomalias']:
            datosAnomalias = datos['anomalias'][anomalia]
            if int(datosAnomalias['trimestre']) in trimestres:
                acumulado_mes['acumulado'] = acumulado_mes['acumulado'] + datosAnomalias['distancia']
                acumulado_mes['trimestres'][x] = {'year': datosAnomalias['year'],
                                                  'trimestre': datosAnomalias['trimestre'],
                                                  'distancia': datosAnomalias['distancia']}
                x = x + 1
            else:
                if len(acumulado_mes['trimestres']) == len(trimestres):
                    acumulados_meses_distancia[len(acumulados_meses_distancia)] = acumulado_mes
                acumulado_mes = {'acumulado': 0, 'trimestres': {}}
                x = 0

        series = {0: None, 1: None, 2: None}
        for i in acumulados_meses_distancia:
            valor = {'year': acumulados_meses_distancia[i]['trimestres'][0]['year'],
                     'trimestres': {'inicio': acumulados_meses_distancia[i]['trimestres'][0]['trimestre'],
                                    'fin': acumulados_meses_distancia[i]['trimestres'][
                                        len(acumulados_meses_distancia[i]['trimestres']) - 1]['trimestre']},
                     'acumulado': acumulados_meses_distancia[i]['acumulado'],
                     'year_serie': acumulados_meses_distancia[i]['trimestres'][0]['year'] + 1}
            for x in range(0, 3):
                if series[x] is None:
                    series[x] = valor
                    break
                elif valor['acumulado'] < series[x]['acumulado']:
                    v = valor
                    valor = series[x]
                    series[x] = v

        return {'series': series, 'datos_clima': acumulados_meses_distancia}
