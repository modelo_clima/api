from .models import *
from rest_framework import serializers


# SerieClimaticaSerializer
# clase encargarda de serializar el modelo SerieClimatica
# @Jose Oliverio Patiño Castaño  @date 06/03/2020
class SerieCimaticaSerializer(serializers.ModelSerializer):
        model = SerieClimatica
        fields = SerieClimatica.get_Atributos()

# ValoresMediaMovilSerializer
# clase encargarda de serializar el modelo ValoresMediaMovil
# @Mauricio Fonseca  @date 06/03/2020
class ValoresMediaMovilSerializer(serializers.ModelSerializer):
    valor = serializers.FloatField(min_value=-500.0, max_value=500.0)
    class Meta:
        model = ValoresMediaMovil
        fields = ValoresMediaMovil.get_Atributos()

# DatosValidadosSerializer
# clase encargarda de serializar el modelo DatosValidados
# @Mauricio Fonseca  @date 06/03/2020
class DatosValidadosSerializer(serializers.ModelSerializer):
    valor = serializers.FloatField(min_value=-500.0, max_value=500.0)
    class Meta:
        model = DatosValidados
        fields = DatosValidados.get_Atributos()


class PeriodoClimaticoEscenarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeriodoClimaticoEscenario
        fields = PeriodoClimaticoEscenario.get_Atributos()


class ValoresPotencialSerializer(serializers.ModelSerializer):
    class Meta:
        model = ValoresPotencial
        fields = ValoresPotencial.get_Atributos()

class OutputValoresRasSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutputValoresRas
        fields = OutputValoresRas.get_Atributos()

class CategoriaOsiSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriaOsi
        fields = CategoriaOsi.get_Atributos()

class ValoresOsiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ValoresOsi
        fields = ValoresOsi.get_Atributos()