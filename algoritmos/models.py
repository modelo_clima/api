from django.db import models
from configuraciones_basicas.models import *
from mesa_tecnica.models import *


##################################CategoriaOsi#####################
#Jose Oliverio Patiño Castaño

class CategoriaOsi(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=False, null=False)
    descripcion = models.CharField(max_length=500, blank=False, null=False)

    class Meta:
        db_table = 'categorias_osi'

    @staticmethod
    def get_Atributos():
        return (['id', 'nombre', 'descripcion'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data
###################################Valores Osi##########################
class ValoresOsi(models.Model):
    id = models.AutoField(primary_key=True)
    valor = models.FloatField(blank=False, null=False)
    id_categoria_osi = models.ForeignKey(CategoriaOsi, to_field='id', db_column='id_categoria_osi', on_delete=models.DO_NOTHING)
    id_punto_gestion = models.ForeignKey(PuntoGestion, to_field='id', db_column='id_punto_gestion', on_delete=models.DO_NOTHING)
    id_escenario = models.ForeignKey(Escenario, to_field='id', db_column='id_escenario', on_delete=models.DO_NOTHING)
    id_variable = models.ForeignKey(Variable, to_field='id', db_column='id_variable', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'valores_osi'

    @staticmethod
    def get_Atributos():
        return (['id', 'valor', 'id_categoria_osi', 'id_punto_gestion', 'id_escenario', 'id_variable'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data

#################################Series Climaticas################################################
# Series Climáticas
# Clase encargada de definir las propiedades de las series climáticas
# @Jose Oliverio Patiño @date 06/03/2020
class SerieClimatica(models.Model):
    id = models.AutoField(primary_key=True)
    fecha_inicio = models.DateField(blank=False, null=False)
    fecha_fin = models.DateField(blank=False, null=False)
    id_mesa_tecnica = models.ForeignKey(MesaTecnica, to_field='id', db_column='id_mesa_tecnica',
                                        on_delete=models.DO_NOTHING)
    id_categoria_clima = models.ForeignKey(CategoriaSerie, to_field='id', db_column='id_categoria_clima',
                                           on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'series_climaticas'
        ordering = ['fecha_inicio']

    @staticmethod
    def get_Atributos():
        return (['id', 'fecha_inicio', 'fecha_fin', 'id_mesa_tecnica', 'id_categoria_clima'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


#################################Periodo Climmático Escenario#########################################
# Período climático Escenario
# Clase encargada de definir las propiedades de los períodos climáticos correspondientes a cada escenario
# @Jose Oliverio Patiño Castaño  @date 06/03/2020

class PeriodoClimaticoEscenario(models.Model):
    id = models.AutoField(primary_key=True)
    ejecutado = models.IntegerField(blank=False, null=False)
    id_periodo_climatico = models.ForeignKey(SerieClimatica, to_field='id', db_column='id_periodo_climatico',
                                             on_delete=models.DO_NOTHING)
    id_escenario = models.ForeignKey(Escenario, to_field='id', db_column='id_escenario', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'periodo_climatico_escenarios'
        ordering = ['ejecutado']

    @staticmethod
    def get_Atributos():
        return (['id', 'ejecutado', 'id_periodo_climatico', 'id_escenario'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


##############################    Valores Potenciales  ################################################

# Valores Potenciales
# clase encargada de definir las propiedades de la valores potenciales en la base de datos
# @Jose Oliverio Patiño Castaño  @date 06/03/2020
class ValoresPotencial(models.Model):
    id = models.AutoField(primary_key=True)
    fecha = models.DateField(blank=False, null=False)
    valor = models.FloatField(blank=False, null=False)
    id_punto_gestion = models.ForeignKey(PuntoGestion, to_field='id', db_column='id_punto_gestion',
                                         on_delete=models.DO_NOTHING)
    id_periodo_climatico_escenario = models.ForeignKey(PeriodoClimaticoEscenario, to_field='id',
                                                       db_column='id_periodo_climatico_escenario',
                                                       on_delete=models.DO_NOTHING)
    id_variable = models.ForeignKey(Variable, to_field='id', db_column='id_variable', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'valores_potenciales'
        # ordering = ['fecha']

    @staticmethod
    def get_Atributos():
        return (['id', 'fecha', 'valor', 'id_punto_gestion', 'id_periodo_climatico_escenario', 'id_variable'])

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


class OutputValoresRas(models.Model):
    id = models.AutoField(primary_key=True)
    fecha = models.DateField(blank=False, null=False)
    valor = models.FloatField(blank=False, null=False)
    id_periodo_climatico_escenario = models.ForeignKey(PeriodoClimaticoEscenario, to_field='id',
                                                       db_column='id_periodo_climatico_escenario',
                                                       on_delete=models.DO_NOTHING)
    id_seccion = models.ForeignKey(Seccion, to_field='id', db_column='id_seccion', on_delete=models.DO_NOTHING)
    id_variable = models.ForeignKey(Variable, to_field='id', db_column='id_variable', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'output_valores_ras'
        # ordering = ['fecha']

    @staticmethod
    def get_Atributos():
        return ['id', 'fecha', 'valor', 'id_periodo_climatico_escenario', 'id_seccion', 'id_variable']

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


##############################    Valores Media Movil  ################################################

# Valores media movil
# clase encargada de definir las propiedades de los valores de media movil del Inidice ONI, Temperatura y presipitación en la base de datos
# @Mauricio Fonseca  
# @date 06/03/2020
class ValoresMediaMovil(models.Model):
    id = models.AutoField(primary_key=True)
    mes = models.IntegerField(blank=False, null=False)
    temporada = models.IntegerField(blank=False, null=False)
    valor = models.DecimalField(decimal_places=50, max_digits=100, blank=False, null=False)
    id_variable = models.ForeignKey(Variable, to_field='id', db_column='id_variable', on_delete=models.DO_NOTHING)
    id_estacion = models.ForeignKey(Estacion, to_field='id', db_column='id_estacion', on_delete=models.DO_NOTHING)
    id_trimestre_media_movil = models.ForeignKey(TrimestreMediaMovil, to_field='id',
                                                 db_column='id_trimestre_media_movil', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'valores_media_movil'
        ordering = ['id_variable', 'temporada', 'mes']

    @staticmethod
    def get_Atributos():
        return ['id', 'mes', 'temporada', 'valor', 'id_variable', 'id_estacion',
                'id_trimestre_media_movil']

    @staticmethod
    def get_Filter(params):
        data = {}
        for param in params:
            if param == 'variable':
                data['id_variable__nombre'] = params.get(param)
            else:
                data[param + '__contains'] = params.get(param)
        return data


# Datos validadps
# clase encargada de definir las propiedades de los datos validados.
# @Mauricio Fonseca
# @date 06/03/2020
class DatosValidados(models.Model):
    id = models.AutoField(primary_key=True)
    fecha = models.DateField(blank=False, null=False)
    valor = models.DecimalField(decimal_places=50, max_digits=100, blank=False, null=False)
    id_variable = models.ForeignKey(Variable, to_field='id', db_column='id_variable', on_delete=models.DO_NOTHING)
    id_estacion = models.ForeignKey(Estacion, to_field='id', db_column='id_estacion', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'datos_validados'
        ordering = ['fecha']

    @staticmethod
    def get_Atributos():
        return ['id', 'fecha', 'valor', 'id_variable', 'id_estacion']

    @staticmethod
    def get_Filter(params):
        data = {}
        for param in params:
            if not params.get(param) is None and not params.get(param) == '':
                if param == 'fechaUno':
                    data['fecha__gte'] = params.get(param)
                elif param == 'fechaDos':
                    data['fecha__lte'] = params.get(param)
                elif param == 'id_variable' or param == 'id_estacion' or param == 'fecha':
                    data[param] = params.get(param)
                else:
                    data[param + '__contains'] = params.get(param)
        return data
