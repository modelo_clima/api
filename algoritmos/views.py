import base64
import io
from datetime import date, timedelta
from rest_framework import status
from rest_framework.decorators import api_view, action
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from .serializers import *
from configuraciones_basicas.views import *
from mesa_tecnica.serializers import *
from mesa_tecnica.views import *
from general_view_set import GeneralModelViewSet

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from algoritmos.modelos_bio.clima import CalculosDatosValidados, ModeloClima


class LimiteViewSet(GeneralModelViewSet):
    model_db = Limite
    queryset = Limite.objects.all()
    serializer_class = LimiteSerializer

    def buscarLimites(self, puntoGestion):
        queryset = self.queryset.filter(id_punto_gestion=puntoGestion)
        serializer = self.serializer_class(queryset, many=True)
        return serializer.data

    def asignarLimites(self, id_punto_gestion, nombre_variable, nombreLimite1, nombreLimite2, nombreLimite3):
        # Consulto los límites con base en el nombre de lavariable y el punto de gestión
        limites = Limite.objects.filter(nombre__contains=nombre_variable, id_punto_gestion=id_punto_gestion)
        serializerLimites = LimiteSerializer(limites, many=True)
        listaLimites = []
        # Obtengo cada uno de los límites
        for m in serializerLimites.data:
            if m["nombre"] == nombreLimite1:
                Limite1 = m["valor"]
                listaLimites.append(Limite1)
            elif m["nombre"] == nombreLimite2:
                Limite2 = m["valor"]
                listaLimites.append(Limite2)
            elif m["nombre"] == nombreLimite3:
                Limite3 = m["valor"]
                listaLimites.append(Limite3)
        return (listaLimites)


class InformeUnoViewSet(GeneralModelViewSet):
    def list(self, request):
        if not 'id_mesa_tecnica' in request.query_params:
            return Response({'detail': 'Debe seleccionar una mesa tecnica'},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            if not 'escenarios' in request.query_params:
                data = MesaTecnicaViewSet.listarMesaTecnicaInforme(MesaTecnicaViewSet,
                                                                   request.query_params["id_mesa_tecnica"])
                data["datos"] = []
            else:
                # genero lista escenarios
                escenarios = []
                # convierto a lista string de request escenarios
                lista = request.query_params["escenarios"].split(",")
                for item in lista:
                    if item != ",":
                        escenarios.append(int(item))

                # Obtengo datos de la mesa tecnica para el informe
                data = MesaTecnicaViewSet.listarMesaTecnicaInforme(MesaTecnicaViewSet,
                                                                   request.query_params["id_mesa_tecnica"])
                # Genero lista datos
                datos = []
                # Consulto los 7 puntos de gestión
                puntosGestion = PuntoGestionViewSet.listarTodosPuntosGestion(PuntoGestionViewSet)
                # Consulto cada uno de los escenarios con base en la lista generada
                for escenario in escenarios:
                    escenarioQuery = EscenarioViewSet.listarEscenarioPorId(EscenarioViewSet, escenario)
                    # Reseteo diccionarioDatos y arrayValores cada vez que consulte un escenario1
                    diccionarioDatos = {}
                    arrayValores = []
                    # Genero ciclo para evaluar valores con base en un punto de gestión y un escenario específico
                    for puntogestion in puntosGestion:
                        # Reseteo diccionario cada vez que consulto un punto de gestión
                        diccionario = {}
                        # Asigno nombre del punto de gestión a la clave punto_gestion
                        diccionario["punto_gestion"] = puntogestion.nombre
                        # Llamo método clasificarOSI
                        self.clasificarOSI(diccionario, puntogestion, escenarioQuery)
                        arrayValores.append(diccionario)
                    diccionarioDatos["nombre_escenario"] = escenarioQuery.codigo
                    diccionarioDatos["valores"] = arrayValores
                    datos.append(diccionarioDatos)

                data["datos"] = datos
            return Response(data)

    def clasificarOSI(self, diccionario, puntogestion, escenario):
        # Consulto valores OSI con base en el punto de gestión y el escenario
        serializerValoresOsi = ValoresOSIViewSet.listarValoresOSIPorPuntoGestionEscenario(ValoresOSIViewSet,
                                                                                          puntogestion.id, escenario.id)
        # Clasifico las categorías con base en los ids de los valores_osi en Bajo, Medio, Alto, MuyAlto
        for item2 in serializerValoresOsi.data:
            if item2["id_categoria_osi"] == 1:
                diccionario["bajo"] = item2["valor"]
            elif item2["id_categoria_osi"] == 2:
                diccionario["medio"] = item2["valor"]
            elif item2["id_categoria_osi"] == 3:
                diccionario["alto"] = item2["valor"]
            elif item2["id_categoria_osi"] == 4:
                diccionario["muy_alto"] = item2["valor"]


class InformeDosViewSet(GeneralModelViewSet):
    def list(self, request):
        if not 'id_mesa_tecnica' in request.query_params:
            return Response({'detail': 'Debe seleccionar una mesa tecnica'},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            if (not 'escenarios' in request.query_params) or (not 'puntos_gestion' in request.query_params):
                data = MesaTecnicaViewSet.listarMesaTecnicaInforme(MesaTecnicaViewSet,
                                                                   request.query_params["id_mesa_tecnica"])
                data["datos"] = []
            else:
                # Defino arrays para guardar los puntos de gestión y escenarios
                puntosGestion = []
                escenarios = []
                # Convierto a lista el string de los request de puntos_gestion y escenarios
                listaPG = request.query_params["puntos_gestion"].split(",")
                listaESCE = request.query_params["escenarios"].split(",")
                # Evalúo cada uno de los ids con base en las listas que se generaron de los strings
                for item in listaPG:
                    if item != ",":
                        puntosGestion.append(int(item))
                for item in listaESCE:
                    if item != ",":
                        escenarios.append(int(item))

                # obtengo datos de la mesa tecnica para el informe
                data = MesaTecnicaViewSet.listarMesaTecnicaInforme(MesaTecnicaViewSet,
                                                                   request.query_params["id_mesa_tecnica"])

                # Consulto las variables que tengan por nombre PR, PS y PWd
                serializerVariable = VariableViewSet.listarVariablesPotenciales(VariableViewSet)

                # Creo lista datos y arregloImagenes
                datos = []
                arregloImagenes = []

                # Hago un ciclo con base en cada uno de los puntos de gestión
                for puntogestion in puntosGestion:
                    # Consulto el punto de gestión con base en la posición del array
                    puntosGestionQuery = PuntoGestionViewSet.listarPuntoGestionPorId(PuntoGestionViewSet, puntogestion)
                    # Reseteo diccionario diccionarioDatos cada vez que consulto un punto de gestión
                    diccionarioDatos = {}
                    # Reseteo lista arrayValores cada vez que consulto un punto de gestión
                    arrayValores = []
                    # Consulto cada uno de los escenarios en un ciclo
                    for escenario in escenarios:
                        escenarioQuery = EscenarioViewSet.listarEscenarioPorId(EscenarioViewSet, escenario)

                        # Reseteo diccionario cada vez que consulto un nuevo escenario
                        diccionario = {}
                        diccionario["nombre_escenario"] = escenarioQuery.codigo

                        # Hago un ciclo con base en las variables consultadas
                        for variable in serializerVariable.data:
                            # Consulto valores_potenciales con base en la variable, punto de gestión y escenario
                            serializerValoresPotencial = ValoresPotencialViewSet.listarValoresPotencialPorEscenarioPuntoGestion(
                                ValoresPotencialViewSet,
                                escenarioQuery.id, puntosGestionQuery.id,
                                variable["id"])
                            # Invoco función para dividir los ValoresPotenciales en sus respectivas series
                            diccionarioSeriesClasificadas = self.dividirSeries(serializerValoresPotencial)

                            # Asigno valores retornados de la función a variables
                            serie1Year = diccionarioSeriesClasificadas["serie1Year"]
                            serie2Year = diccionarioSeriesClasificadas["serie2Year"]
                            serie3Year = diccionarioSeriesClasificadas["serie3Year"]
                            serie1 = diccionarioSeriesClasificadas["serie1"]
                            serie2 = diccionarioSeriesClasificadas["serie2"]
                            serie3 = diccionarioSeriesClasificadas["serie3"]
                            coordenadasY = diccionarioSeriesClasificadas["coordenadasY"]

                            # Genero listas para las categorías para cada una de las series
                            serie1Bajo = []
                            serie1Moderado = []
                            serie1Alto = []
                            serie1MuyAlto = []

                            serie2Bajo = []
                            serie2Moderado = []
                            serie2Alto = []
                            serie2MuyAlto = []

                            serie3Bajo = []
                            serie3Moderado = []
                            serie3Alto = []
                            serie3MuyAlto = []

                            # Evalúo el filtro de datos con base en el nombre de la variable para graficar
                            if variable["nombre"] == "PR":
                                # Busco y asigno cada uno de los límites con base en el nombre de la variable
                                listaLimites = LimiteViewSet.asignarLimites(LimiteViewSet, puntosGestionQuery.id, 'PR',
                                                                            'PR1',
                                                                            'PR2', 'PR3')
                                Limite1 = listaLimites[0]
                                Limite2 = listaLimites[1]
                                Limite3 = listaLimites[2]
                                # Clasifico cada una da las series en su categorías con base en los límites
                                # Una vez obtenidas las listas llenas se procede a graficar
                                self.clasificarSeries(serie1, Limite1, Limite2, Limite3, serie1Bajo, serie1Moderado,
                                                      serie1Alto,
                                                      serie1MuyAlto)
                                self.clasificarSeries(serie2, Limite1, Limite2, Limite3, serie2Bajo, serie2Moderado,
                                                      serie2Alto,
                                                      serie2MuyAlto)
                                self.clasificarSeries(serie3, Limite1, Limite2, Limite3, serie3Bajo, serie3Moderado,
                                                      serie3Alto,
                                                      serie3MuyAlto)

                                # Genero datos para graficar
                                titulo = 'Figuras Potencial de renovación series: '
                                labelx = 'Ciclo de gestión'
                                labely = 'Potencial de renovación'
                                potencial = "potencial_renovacion"
                                # Genero gráfica
                                self.graficarPotencial(coordenadasY, serie1Year, serie2Year, serie3Year,
                                                       serie1MuyAlto, serie1Alto, serie1Moderado, serie1Bajo,
                                                       serie2MuyAlto, serie2Alto, serie2Moderado, serie2Bajo,
                                                       serie3MuyAlto, serie3Alto, serie3Moderado, serie3Bajo,
                                                       titulo, labelx, labely, diccionario, potencial, arregloImagenes)
                            elif variable["nombre"] == "PS":
                                # Busco y asigno cada uno de los límites con base en el nombre de la variable
                                listaLimites = LimiteViewSet.asignarLimites(LimiteViewSet, puntosGestionQuery.id, 'PS',
                                                                            'PS1',
                                                                            'PS2', 'PS3')
                                Limite1 = listaLimites[0]
                                Limite2 = listaLimites[1]
                                Limite3 = listaLimites[2]
                                # Clasifico cada una da las series en su categorías con base en los límites
                                # Una vez obtenidas las listas llenas se procede a graficar
                                self.clasificarSeriesSalinizacion(serie1, Limite1, Limite2, Limite3, serie1Bajo,
                                                                  serie1Moderado,
                                                                  serie1Alto,
                                                                  serie1MuyAlto)
                                self.clasificarSeriesSalinizacion(serie2, Limite1, Limite2, Limite3, serie2Bajo,
                                                                  serie2Moderado,
                                                                  serie2Alto,
                                                                  serie2MuyAlto)
                                self.clasificarSeriesSalinizacion(serie3, Limite1, Limite2, Limite3, serie3Bajo,
                                                                  serie3Moderado,
                                                                  serie3Alto,
                                                                  serie3MuyAlto)

                                titulo = 'Figuras Potencial de salinización series: '
                                labelx = 'Ciclo de gestión'
                                labely = 'Potencial de salinización'
                                potencial = "potencial_salinizacion"
                                self.graficarPotencial(coordenadasY, serie1Year, serie2Year, serie3Year,
                                                       serie1MuyAlto, serie1Alto, serie1Moderado, serie1Bajo,
                                                       serie2MuyAlto, serie2Alto, serie2Moderado, serie2Bajo,
                                                       serie3MuyAlto, serie3Alto, serie3Moderado, serie3Bajo,
                                                       titulo, labelx, labely, diccionario, potencial, arregloImagenes)
                            elif variable["nombre"] == "PWd":
                                # Busco y asigno cada uno de los límites con base en el nombre de la variable
                                listaLimites = LimiteViewSet.asignarLimites(LimiteViewSet, puntosGestionQuery.id, 'PWd',
                                                                            'PWd1',
                                                                            'PWd2', 'PWd3')
                                Limite1 = listaLimites[0]
                                Limite2 = listaLimites[1]
                                Limite3 = listaLimites[2]
                                # Clasifico cada una da las series en su categorías con base en los límites
                                # Una vez obtenidas las listas llenas se procede a graficar

                                self.clasificarSeries(serie1, Limite1, Limite2, Limite3, serie1Bajo, serie1Moderado,
                                                      serie1Alto,
                                                      serie1MuyAlto)
                                self.clasificarSeries(serie2, Limite1, Limite2, Limite3, serie2Bajo, serie2Moderado,
                                                      serie2Alto,
                                                      serie2MuyAlto)
                                self.clasificarSeries(serie3, Limite1, Limite2, Limite3, serie3Bajo, serie3Moderado,
                                                      serie3Alto,
                                                      serie3MuyAlto)

                                titulo = 'Figuras Potencial de Déficit hídrico series: '
                                labelx = 'Ciclo de gestión'
                                labely = 'Potencial de déficit hídrico'
                                potencial = "potencial_deficit_hidrico"
                                self.graficarPotencial(coordenadasY, serie1Year, serie2Year, serie3Year,
                                                       serie1MuyAlto, serie1Alto, serie1Moderado, serie1Bajo,
                                                       serie2MuyAlto, serie2Alto, serie2Moderado, serie2Bajo,
                                                       serie3MuyAlto, serie3Alto, serie3Moderado, serie3Bajo,
                                                       titulo, labelx, labely, diccionario, potencial, arregloImagenes)
                        # Obtengo datos de OSI con base en el punto de gestión y el escenario
                        InformeUnoViewSet.clasificarOSI(InformeUnoViewSet, diccionario, puntosGestionQuery,
                                                        escenarioQuery)
                        # Agrego diccionario a arrayValores
                        arrayValores.append(diccionario)
                    # agrego nombre de punto de gestión a clave punto_gestión de diccionarioDatos
                    diccionarioDatos["punto_gestion"] = puntosGestionQuery.nombre
                    # agrego gráficas y valores OSI a clave gráficas de diccionarioDatos
                    diccionarioDatos["graficas"] = arrayValores
                    # Agrego diccionarioDatos a lista datos
                    datos.append(diccionarioDatos)
                # Por último agrego lista datos a diccionario data
                data["datos"] = datos
        return Response(data)
        # return Response(arregloImagenes)
        # return Response(serie3MuyAlto)

    def dividirSeries(self, serializerValoresPotencial):
        # Reseteo las series cada vez que consulto los valores potenciales, En total tendré 1095 datos por las tres serie
        serie1 = []
        serie2 = []
        serie3 = []
        # genero contador para dividir los datos en las tres series
        longitud = 1
        # Genero las lista de coordenadas Y para graficar
        coordenadasY = []
        # Divido los 365 días en los 12 meses
        cantidad = 12 / 365
        sumatoriaMeses = cantidad
        # Con un ciclo for empiezo a dividir cada una de las series
        diccionarioSeries = {}
        for serie in serializerValoresPotencial.data:
            # Obtengo la fecha de cada una de las series
            fecha = pd.to_datetime(serie["fecha"])
            # Genero primera serie
            if longitud >= 1 and longitud <= 365:
                # Obtengo año de la serie
                serie1Year = fecha.year
                # agrego valor a Serie1
                serie1.append(serie["valor"])
                sumatoriaMeses += cantidad
                # coordenadasY.append(longitud)
                coordenadasY.append(sumatoriaMeses)
            # Genero segunda serie
            elif longitud > 365 and longitud <= 730:
                serie2Year = fecha.year
                serie2.append(serie["valor"])
            # Genero tercera serie
            elif longitud > 730:
                serie3Year = fecha.year
                serie3.append(serie["valor"])
            longitud += 1
        diccionarioClasificacionSeries = {"serie1Year": serie1Year, "serie2Year": serie2Year, "serie3Year": serie3Year,
                                          "serie1": serie1, "serie2": serie2, "serie3": serie3,
                                          "coordenadasY": coordenadasY, "fecha": fecha.year}
        return diccionarioClasificacionSeries

    def clasificarSeriesSalinizacion(self, serie, Limite1, Limite2, Limite3, serieBajo, serieModerado, serieAlto,
                                     serieMuyAlto):
        # Leo cada uno de los valores de la serie
        for valorSerie in serie:
            # Condicional en el caso de que el valor esté en límite Muy alto, agrego valor en array serieMuyAlto
            # Y en los demás arrays dejo el valor Nulo
            if valorSerie >= Limite1:
                serieBajo.append(None)
                serieModerado.append(None)
                serieAlto.append(None)
                serieMuyAlto.append(valorSerie)
            # Condicional en el caso de que el valor esté en límite Alto, agrego valor en array serieAlto
            # Y en los demás arrays dejo el valor Nulo
            elif valorSerie >= Limite1 and valorSerie < Limite2:
                serieBajo.append(None)
                serieModerado.append(None)
                serieAlto.append(valorSerie)
                serieMuyAlto.append(None)
            # Condicional en el caso de que el valor esté en límite Moderado, agrego valor en array serieModerado
            # Y en los demás arrays dejo el valor Nulo
            elif valorSerie >= Limite2 and valorSerie < Limite3:
                serieBajo.append(None)
                serieModerado.append(valorSerie)
                serieAlto.append(None)
                serieMuyAlto.append(None)
            # Condicional en el caso de que el valor esté en límite Bajo, agrego valor en array serieBajo
            # Y en los demás arrays dejo el valor Nulo
            elif valorSerie < Limite3:
                serieBajo.append(valorSerie)
                serieModerado.append(None)
                serieAlto.append(None)
                serieMuyAlto.append(None)

    def clasificarSeries(self, serie, Limite1, Limite2, Limite3, serieBajo, serieModerado, serieAlto, serieMuyAlto):
        # Leo cada uno de los valores de la serie
        for valorSerie in serie:
            # Condicional en el caso de que el valor esté en límite Muy alto, agrego valor en array serieMuyAlto
            # Y en los demás arrays dejo el valor Nulo
            if valorSerie <= Limite1:
                serieBajo.append(None)
                serieModerado.append(None)
                serieAlto.append(None)
                serieMuyAlto.append(valorSerie)
            # Condicional en el caso de que el valor esté en límite Alto, agrego valor en array serieAlto
            # Y en los demás arrays dejo el valor Nulo
            elif valorSerie > Limite1 and valorSerie <= Limite2:
                serieBajo.append(None)
                serieModerado.append(None)
                serieAlto.append(valorSerie)
                serieMuyAlto.append(None)
            # Condicional en el caso de que el valor esté en límite Moderado, agrego valor en array serieModerado
            # Y en los demás arrays dejo el valor Nulo
            elif valorSerie > Limite2 and valorSerie <= Limite3:
                serieBajo.append(None)
                serieModerado.append(valorSerie)
                serieAlto.append(None)
                serieMuyAlto.append(None)
            # Condicional en el caso de que el valor esté en límite Bajo, agrego valor en array serieBajo
            # Y en los demás arrays dejo el valor Nulo
            elif valorSerie >= Limite3:
                serieBajo.append(valorSerie)
                serieModerado.append(None)
                serieAlto.append(None)
                serieMuyAlto.append(None)

    def graficarPotencial(self, coordenadasY, serie1Year, serie2Year, serie3Year,
                          serie1MuyAlto, serie1Alto, serie1Moderado, serie1Bajo,
                          serie2MuyAlto, serie2Alto, serie2Moderado, serie2Bajo,
                          serie3MuyAlto, serie3Alto, serie3Moderado, serie3Bajo,
                          titulo, labelx, labely, diccionario, potencial, arregloImagenes):
        # generar figura con su tamaño respectivo
        fig = plt.figure(figsize=[15, 9])
        # SubPlot Gráfica Muy alto
        self.generarsubPlot('#FF0000', coordenadasY, serie1MuyAlto, serie2MuyAlto, serie3MuyAlto, "Muy alto", 1)
        # Generar título de la gráfica
        plt.title(str(titulo) + str(serie1Year) + ", " + str(serie2Year) + ", " + str(serie3Year))
        # SubPlot Gráfica Alto
        self.generarsubPlot('#FF8000', coordenadasY, serie1Alto, serie2Alto, serie3Alto, "Alto", 2)
        # SubPlot Gráfica Moderado
        self.generarsubPlot('#FDFD96', coordenadasY, serie1Moderado, serie2Moderado, serie3Moderado, "Moderado", 3)
        # SubPlot Gráfica Bajo
        self.generarsubPlot('#00AAE4', coordenadasY, serie1Bajo, serie2Bajo, serie3Bajo, "Bajo", 4)
        # Generar label 'Ciclo de gestión'
        plt.xlabel(labelx)

        # Llamado a función para generar gráfca encriptada
        self.encriptarGrafica(diccionario, arregloImagenes, potencial, fig)

    def generarsubPlot(self, color, coordenadasY, serie1, serie2, serie3, labelY, numerosubPlot):
        plt.subplot(4, 1, numerosubPlot).set_facecolor(color)
        plt.plot(coordenadasY, serie1, '.k')
        plt.plot(coordenadasY, serie2, '.w')
        plt.plot(coordenadasY, serie3, '.g')
        plt.ylabel(labelY, fontsize=10)

    def encriptarGrafica(self, diccionario, arregloImagenes, potencial, fig):
        buf = io.BytesIO()
        fig.savefig(buf, format="png")
        data = base64.b64encode(buf.getbuffer()).decode("ascii")
        diccionario[potencial] = data
        arregloImagenes.append(f"<img src='data:image/png;base64,{data}'/>")


class InformeTresViewSet(GeneralModelViewSet):
    model_db = ValoresPotencial
    queryset = ValoresPotencial.objects.all()
    serializer_class = ValoresPotencialSerializer


class ValoresPotencialViewSet(GeneralModelViewSet):
    model_db = ValoresPotencial
    queryset = ValoresPotencial.objects.all()
    serializer_class = ValoresPotencialSerializer

    def listarValoresPotencialPorEscenarioPuntoGestion(self, id_escenario, id_punto_gestion, id_variable):
        valoresPotenciales = ValoresPotencial.objects.filter(
            id_periodo_climatico_escenario__id_escenario=id_escenario,
            id_punto_gestion=id_punto_gestion, id_variable=id_variable)

        serializerValoresPotencial = ValoresPotencialSerializer(valoresPotenciales, many=True)
        return serializerValoresPotencial

    def clasificarPotencial(self, MuyAlto, Alto, Moderado, Bajo,
                            Limite1, Limite2, Limite3, Serie):
        longitudSerie = len(Serie)

        for posicion in range(longitudSerie):
            if Serie[posicion] <= Limite1:
                MuyAlto.append(posicion)
            elif Serie[posicion] > Limite1 and Serie[posicion] <= Limite2:
                Alto.append(posicion)
            elif Serie[posicion] > Limite2 and Serie[posicion] <= Limite3:
                Moderado.append(posicion)
            elif Serie[posicion] > Limite3:
                Bajo.append(posicion)

    def clasificarPotencialSalinizacion(self, MuyAlto, Alto, Moderado, Bajo,
                                        Limite1, Limite2, Limite3, Serie):
        longitudSerie = len(Serie)

        for posicion in range(longitudSerie):
            if Serie[posicion] >= Limite1:
                MuyAlto.append(posicion)
            elif Serie[posicion] >= Limite1 and Serie[posicion] < Limite2:
                Alto.append(posicion)
            elif Serie[posicion] >= Limite2 and Serie[posicion] < Limite3:
                Moderado.append(posicion)
            elif Serie[posicion] < Limite3:
                Bajo.append(posicion)


class ValoresOSIViewSet(GeneralModelViewSet):
    model_db = ValoresOsi
    queryset = ValoresOsi.objects.all()
    serializer_class = ValoresOsiSerializer

    def listarValoresOSIPorPuntoGestionEscenario(self, id_punto_gestion, id_escenario):
        valores_osi = ValoresOsi.objects.filter(id_punto_gestion=id_punto_gestion, id_escenario=id_escenario,
                                                id_variable__nombre="OSI")
        serializerValoresOsi = self.serializer_class(valores_osi, many=True)
        return serializerValoresOsi

    def hallarPorcentajeCategoria(self, MuyAlto, Alto, Moderado, Bajo, longitudSerie):
        listaPorcentajes = []
        PorcentajeMuyAlto = (100 * len(MuyAlto)) / longitudSerie
        listaPorcentajes.append(PorcentajeMuyAlto)
        PorcentajeAlto = (100 * len(Alto)) / longitudSerie
        listaPorcentajes.append(PorcentajeAlto)
        PorcentajeModerado = (100 * len(Moderado)) / longitudSerie
        listaPorcentajes.append(PorcentajeModerado)
        PorcentajeBajo = (100 * len(Bajo)) / longitudSerie
        listaPorcentajes.append(PorcentajeBajo)

        return (listaPorcentajes)

    def hallarConceptoGlobal(self, PorcentajeMuyAlto, PorcentajeAlto, PorcentajeModerado, PorcentajeBajo,
                             conceptoGlobalPotencial):
        if ((PorcentajeMuyAlto >= 25 and PorcentajeMuyAlto <= 100) &
                (PorcentajeAlto >= 0 and PorcentajeAlto <= 50) &
                (PorcentajeModerado >= 0 and PorcentajeModerado < 50) &
                (PorcentajeBajo >= 0 and PorcentajeBajo < 25)):
            conceptoGlobalPotencial = 1
        elif ((PorcentajeMuyAlto >= 0 and PorcentajeMuyAlto < 25) &
              (PorcentajeAlto > 25 and PorcentajeAlto <= 100) &
              (PorcentajeModerado >= 0 and PorcentajeModerado <= 50) &
              (PorcentajeBajo >= 0 and PorcentajeBajo < 25)):
            conceptoGlobalPotencial = 2
        elif ((PorcentajeMuyAlto >= 0 and PorcentajeMuyAlto <= 25) &
              (PorcentajeAlto >= 0 and PorcentajeAlto <= 50) &
              (PorcentajeModerado >= 25 and PorcentajeModerado <= 100) &
              (PorcentajeBajo >= 0 and PorcentajeBajo <= 75)):
            conceptoGlobalPotencial = 3
        elif ((PorcentajeMuyAlto >= 0 and PorcentajeMuyAlto < 25) &
              (PorcentajeAlto >= 0 and PorcentajeAlto < 25) &
              (PorcentajeModerado >= 0 and PorcentajeModerado < 25) &
              (PorcentajeBajo > 75 and PorcentajeBajo <= 100)):
            conceptoGlobalPotencial = 4
        return (conceptoGlobalPotencial)

    def insertarDatosPotencialOSI(self, MuyAltoPotencial, AltoPotencial, ModeradoPotencial, BajoPotencial):
        OSIPotencial = []
        try:
            for i in range(len(MuyAltoPotencial)):
                OSIPotencial.insert(MuyAltoPotencial[i], 1)
            for i in range(len(AltoPotencial)):
                OSIPotencial.insert(AltoPotencial[i], 2)
            for i in range(len(ModeradoPotencial)):
                OSIPotencial.insert(ModeradoPotencial[i], 3)
            for i in range(len(BajoPotencial)):
                OSIPotencial.insert(BajoPotencial[i], 4)
        except:
            OSIPotencial = []
        return (OSIPotencial)

    def generarDatosOSI(self, OPR, OPs, OPd):
        OSI = []
        longitudOSI = len(OPR)
        for i in range(longitudOSI):
            if (OPR[i] == 1 and OPs[i] == 1 and OPd[i] == 1):
                OSI.insert(i, 1)  # susceptibilidad conjunta muy alta
            elif (OPR[i] >= 2 and OPs[i] <= 2 and OPd[i] <= 2):
                OSI.insert(i, 2)  # susceptibilidad conjunta alta
            elif (OPR[i] <= 2 and OPs[i] >= 2 and OPd[i] <= 2):
                OSI.insert(i, 2)  # susceptibilidad conjunta alta
            elif (OPR[i] <= 2 and OPs[i] <= 2 and OPd[i] >= 2):
                OSI.insert(i, 2)  # susceptibilidad conjunta alta
            elif (OPR[i] >= 3 and OPs[i] >= 3 and OPd[i] <= 3):
                OSI.insert(i, 3)  # susceptibilidad conjunta moderada
            elif (OPR[i] >= 3 and OPs[i] <= 3 and OPd[i] >= 3):
                OSI.insert(i, 3)  # susceptibilidad conjunta moderada
            elif (OPR[i] <= 3 and OPs[i] >= 3 and OPd[i] >= 3):
                OSI.insert(i, 3)  # susceptibilidad conjunta moderada
            elif (OPR[i] == 4 and OPs[i] == 4 and OPd[i] == 4):
                OSI.insert(i, 4)  # susceptibilidad conjunta baja
            else:
                OSI.insert(i, 0)
        return (OSI)

    def list(self, request):
        # Consulto escenarios relacionados con base en la mesa técnica asignada
        serializerEscenarios = EscenarioViewSet.getEscenarioPorMesaTecnica(EscenarioViewSet,
                                                                           request.query_params["id_mesa_tecnica"])
        # Consulto todos los puntos de gestion
        puntosGestion = PuntoGestionViewSet.listarTodosPuntosGestion(PuntoGestionViewSet)
        for escenario in serializerEscenarios.data:
            for puntoGestion in puntosGestion:
                # Consulto las variables que tengan por nombre PR, PS y PWd
                serializerVariable = VariableViewSet.listarVariablesPotenciales(VariableViewSet)
                for variable in serializerVariable.data:
                    # Consulto valores_potenciales con base en la variable, punto de gestión y escenario
                    serializerValoresPotencial = ValoresPotencialViewSet.listarValoresPotencialPorEscenarioPuntoGestion(
                        ValoresPotencialViewSet, 35, puntoGestion.id, variable["id"])

                    if variable["nombre"] == "PR":
                        listaPorcentajes = []
                        conceptoGlobalPR = 0
                        # Busco y asigno cada uno de los límites con base en el nombre de la variable
                        listaLimites = LimiteViewSet.asignarLimites(LimiteViewSet, 1, 'PR', 'PR1', 'PR2',
                                                                    'PR3')
                        Limite1 = listaLimites[0]
                        Limite2 = listaLimites[1]
                        Limite3 = listaLimites[2]

                        seriePotencial = []
                        for potencial in serializerValoresPotencial.data:
                            seriePotencial.append(potencial["valor"])

                        longitudSerie = len(seriePotencial)

                        # Definición de categorias
                        MuyAltoPR = []
                        AltoPR = []
                        ModeradoPR = []
                        BajoPR = []

                        # Invoco función clasificarPotencial
                        ValoresPotencialViewSet.clasificarPotencial(ValoresPotencialViewSet, MuyAltoPR, AltoPR,
                                                                    ModeradoPR, BajoPR, Limite1, Limite2, Limite3,
                                                                    seriePotencial)

                        # Invoco función hallar porcentaje por categoría
                        porcentajes = self.hallarPorcentajeCategoria(MuyAltoPR, AltoPR, ModeradoPR, BajoPR,
                                                                     longitudSerie)

                        # Asigno cada porcentaje
                        PorcentajeMuyAltoPR = round(porcentajes[0], 2)
                        PorcentajeAltoPR = round(porcentajes[1], 2)
                        PorcentajeModeradoPR = round(porcentajes[2], 2)
                        PorcentajeBajoPR = round(porcentajes[3], 2)

                        # InvocoFunción para hallar concepto Global
                        self.hallarConceptoGlobal(PorcentajeMuyAltoPR, PorcentajeAltoPR, PorcentajeModeradoPR,
                                                  PorcentajeBajoPR, conceptoGlobalPR)
                        # Invoco función para insertarDatosOSI
                        OPR = self.insertarDatosPotencialOSI(MuyAltoPR, AltoPR, ModeradoPR, BajoPR)

                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id,
                                               PorcentajeMuyAltoPR, 4)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, PorcentajeAltoPR,
                                               3)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id,
                                               PorcentajeModeradoPR, 2)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, PorcentajeBajoPR,
                                               1)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, conceptoGlobalPR,
                                               5)
                    elif variable["nombre"] == "PS":
                        conceptoGlobalPS = 0
                        # Busco y asigno cada uno de los límites con base en el nombre de la variable
                        listaLimites = LimiteViewSet.asignarLimites(LimiteViewSet, 1, 'PS', 'PS1',
                                                                    'PS2', 'PS3')
                        Limite1 = listaLimites[0]
                        Limite2 = listaLimites[1]
                        Limite3 = listaLimites[2]

                        seriePotencial = []
                        for potencial in serializerValoresPotencial.data:
                            seriePotencial.append(potencial["valor"])

                        longitudSerie = len(seriePotencial)

                        MuyAltoPS = []
                        AltoPS = []
                        ModeradoPS = []
                        BajoPS = []

                        # Invoco función clasificarPotencial
                        ValoresPotencialViewSet.clasificarPotencialSalinizacion(ValoresPotencialViewSet, MuyAltoPS,
                                                                                AltoPS, ModeradoPS, BajoPS, Limite1,
                                                                                Limite2, Limite3,
                                                                                seriePotencial)

                        # Invoco función hallar porcentaje por categoría
                        porcentajes = self.hallarPorcentajeCategoria(MuyAltoPS, AltoPS, ModeradoPS, BajoPS,
                                                                     longitudSerie)

                        # Asigno cada porcentaje
                        PorcentajeMuyAltoPS = round(porcentajes[0], 2)
                        PorcentajeAltoPS = round(porcentajes[1], 2)
                        PorcentajeModeradoPS = round(porcentajes[2], 2)
                        PorcentajeBajoPS = round(porcentajes[3], 2)

                        # InvocoFunción para hallar concepto Global
                        self.hallarConceptoGlobal(PorcentajeMuyAltoPS, PorcentajeAltoPS, PorcentajeModeradoPS,
                                                  PorcentajeBajoPS, conceptoGlobalPS)
                        # Invoco función para insertar datos PotencialesOSI
                        OPs = self.insertarDatosPotencialOSI(MuyAltoPS, AltoPS, ModeradoPS, BajoPS)

                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id,
                                               PorcentajeMuyAltoPS, 4)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, PorcentajeAltoPS,
                                               3)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id,
                                               PorcentajeModeradoPS, 2)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, PorcentajeBajoPS,
                                               1)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, conceptoGlobalPS,
                                               5)

                    elif variable["nombre"] == "PWd":
                        conceptoGlobalPWd = 0
                        # Busco y asigno cada uno de los límites con base en el nombre de la variable
                        listaLimites = LimiteViewSet.asignarLimites(LimiteViewSet, 1, 'PWd', 'PWd1',
                                                                    'PWd2', 'PWd3')
                        Limite1 = listaLimites[0]
                        Limite2 = listaLimites[1]
                        Limite3 = listaLimites[2]

                        serie = []
                        for potencial in serializerValoresPotencial.data:
                            serie.append(potencial["valor"])

                        longitudSerie = len(seriePotencial)

                        # Definición de categorias
                        MuyAltoPWd = []
                        AltoPWd = []
                        ModeradoPWd = []
                        BajoPWd = []

                        # Invoco función clasificarPotencial
                        ValoresPotencialViewSet.clasificarPotencial(ValoresPotencialViewSet, MuyAltoPWd, AltoPWd,
                                                                    ModeradoPWd, BajoPWd, Limite1, Limite2, Limite3,
                                                                    seriePotencial)

                        # Invoco función hallar porcentaje por categoría
                        porcentajes = self.hallarPorcentajeCategoria(MuyAltoPWd, AltoPWd, ModeradoPWd, BajoPWd,
                                                                     longitudSerie)

                        # Asigno cada porcentaje
                        PorcentajeMuyAltoPWd = round(porcentajes[0], 2)
                        PorcentajeAltoPWd = round(porcentajes[1], 2)
                        PorcentajeModeradoPWd = round(porcentajes[2], 2)
                        PorcentajeBajoPWd = round(porcentajes[3], 2)

                        # InvocoFunción para hallar concepto Global
                        self.hallarConceptoGlobal(PorcentajeMuyAltoPWd, PorcentajeAltoPWd, PorcentajeModeradoPWd,
                                                  PorcentajeBajoPWd, conceptoGlobalPWd)
                        # Invoco función para insertarDatosPotencialesOSI
                        OPd = self.insertarDatosPotencialOSI(MuyAltoPWd, AltoPWd, ModeradoPWd, BajoPWd)

                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id,
                                               PorcentajeMuyAltoPWd, 4)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, PorcentajeAltoPWd,
                                               3)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id,
                                               PorcentajeModeradoPWd, 2)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, PorcentajeBajoPWd,
                                               1)
                        self.guardarValoresOSI(variable["nombre"], escenario["id"], puntoGestion.id, conceptoGlobalPWd,
                                               5)

                conceptoGlobalOSI = 0
                # Invoco generarOSI
                serieOSI = self.generarDatosOSI(OPR, OPs, OPd)

                longitudSerie = len(serieOSI)

                # Definición de categorias
                MuyAltoOSI = []
                AltoOSI = []
                ModeradoOSI = []
                BajoOSI = []

                # Invoco función clasificarPotencial
                ValoresPotencialViewSet.clasificarPotencial(ValoresPotencialViewSet, MuyAltoOSI, AltoOSI, ModeradoOSI,
                                                            BajoOSI, 1, 2, 3, serieOSI)

                # Invoco función hallar porcentaje por categoría
                porcentajes = self.hallarPorcentajeCategoria(MuyAltoOSI, AltoOSI, ModeradoOSI, BajoOSI, longitudSerie)

                # Asigno cada porcentaje
                PorcentajeMuyAltoOSI = round(porcentajes[0], 2)
                PorcentajeAltoOSI = round(porcentajes[1], 2)
                PorcentajeModeradoOSI = round(porcentajes[2], 2)
                PorcentajeBajoOSI = round(porcentajes[3], 2)

                # InvocoFunción para hallar concepto Global
                self.hallarConceptoGlobal(PorcentajeMuyAltoOSI, PorcentajeAltoOSI, PorcentajeModeradoOSI,
                                          PorcentajeBajoOSI, conceptoGlobalOSI)

                self.guardarValoresOSI("OSI", escenario["id"], puntoGestion.id, PorcentajeMuyAltoOSI, 4)
                self.guardarValoresOSI("OSI", escenario["id"], puntoGestion.id, PorcentajeAltoOSI, 3)
                self.guardarValoresOSI("OSI", escenario["id"], puntoGestion.id, PorcentajeModeradoOSI, 2)
                self.guardarValoresOSI("OSI", escenario["id"], puntoGestion.id, PorcentajeBajoOSI, 1)
                self.guardarValoresOSI("OSI", escenario["id"], puntoGestion.id, conceptoGlobalOSI, 5)

                OSIMA = []
                OSIA = []
                OSIM = []
                OSIB = []
                # Genero las lista de coordenadas Y para graficar
                coordenadasY = []
                # Divido los 1095 días en los 36 meses
                cantidad = 36 / 1095
                sumatoriaMeses = cantidad
                for i in range(longitudSerie):
                    sumatoriaMeses += cantidad
                    # coordenadasY.append(longitud)
                    coordenadasY.append(sumatoriaMeses)

                    OSIMA.append(1)
                    OSIA.append(2)
                    OSIM.append(3)
                    OSIB.append(4)

        return Response(PorcentajeMuyAltoOSI)
        # return Response(self.graficarOSI(serieOSI, OSIMA, OSIA, OSIM, OSIB, coordenadasY))

    def graficarOSI(self, serieOSI, OSIMA, OSIA, OSIM, OSIB, coordenadasY):
        plt.style.use(['seaborn-paper'])  # Fondo oscuro
        plt.figure(figsize=(15, 9))
        plt.plot(coordenadasY, serieOSI, 'ok', label='OSI')
        plt.plot(coordenadasY, OSIMA, color='r', label='Muy alta')
        plt.plot(coordenadasY, OSIA, color='#FF8000', label='Alta')
        plt.plot(coordenadasY, OSIM, color='y', label='Media')
        plt.plot(coordenadasY, OSIB, color='b', label='Baja')
        # Coloco la leyenda y la ubico con un tamaño específico
        plt.legend(loc='upper right', bbox_to_anchor=(1.3, 0.65), fontsize=10)

        plt.xlabel('Ciclo de gestión (d)', fontsize=10)
        plt.ylabel('Índice de susceptibilidad conjunta (OSI)', fontsize=10)
        plt.title('Figura número 4', fontsize=10)
        # plt.grid(True)

        plt.show()

    def guardarValoresOSI(self, nombre_variable, id_escenario, id_punto_gestion, valor, id_categoria_osi):
        variable = Variable.objects.get(nombre=nombre_variable)
        categoriaOsi = CategoriaOsi.objects.get(id=id_categoria_osi)
        puntoGestion = PuntoGestion.objects.get(id=id_punto_gestion)
        escenario = Escenario.objects.get(id=id_escenario)
        VOSI = ValoresOsi(
            valor=valor,
            id_categoria_osi=categoriaOsi,
            id_punto_gestion=puntoGestion,
            id_escenario=escenario,
            id_variable=variable
        )
        VOSI.save()


class OutputValoresRasViewSet(GeneralModelViewSet):
    model_db = OutputValoresRas
    queryset = OutputValoresRas.objects.all()
    serializer_class = OutputValoresRasSerializer


class ValoresMediaMovilViewSet(GeneralModelViewSet):
    model_db = ValoresMediaMovil
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = ValoresMediaMovil.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = ValoresMediaMovilSerializer

    def create(self, request):
        if ('actualizar_media_movil_variable' in request.data and request.data['actualizar_media_movil_variable']):
            # return Response(request.data['nombre'])
            modeloClima = ModeloClima()
            modeloClima.guardarMediaMovil(request.data['variable'], request.data['estacion'])

            queryset = ValoresMediaMovil.objects.filter(id_variable__nombre=request.data['variable'],
                                                        id_estacion__nombre=request.data['estacion'])
            serializer = self.serializer_class(queryset, many=True)
            return Response(serializer.data)

        else:
            return super().create(self, request)


class DatosValidadosViewSet(GeneralModelViewSet):
    model_db = DatosValidados
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = DatosValidados.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = DatosValidadosSerializer

    @action(methods=['get'], detail=False)
    def resources(self, request):
        estaciones = Estacion.objects.all()
        variables = Variable.objects.filter(id_tipo_variable__nombre='Climatológicas')
        estacionesData = []
        variablesData = []
        for item in estaciones:
            estacionesData.append({'id': item.id, 'nombre': item.nombre})
        for item in variables:
            variablesData.append({'id': item.id, 'nombre': item.nombre})
        return JsonResponse({'estaciones': estacionesData, 'variables': variablesData})

    def list(self, request):
        queryset = self.filterObjects(request)
        serializer = self.serializer_class(queryset, many=True)
        datos = serializer.data
        grafica = {'img': '', 'estado': False}
        if 'id_variable' in request.query_params:
            grafica = CalculosDatosValidados.getGraficarDatos(request.query_params['id_variable'], datos)
        return Response({'datos': datos, 'grafica': grafica})

    def create(self, request):
        try:
            my_file = request.FILES['file_name']
            lineas = my_file.readlines()
            encabezado = {'fecha': None, "valor": None, "variable": None, "estacion": None}
            listaDatos = []
            variablesMM = {}
            i = 0
            for linea in lineas:
                l = str(linea).replace("\\r\\n'", "")
                l = str(l).replace("b'", "")
                data = l.split(";")
                if i == 0:
                    for item in range(0, len(data)):
                        encabezado[data[item]] = item
                    for item in encabezado:
                        if encabezado[item] is None:
                            raise ValueError('No coincide el encabezado.')
                else:
                    nom_variable = data[encabezado['variable']]
                    nom_variable = str(nom_variable).replace('\\xf3', 'ó')
                    cod_estacion = data[encabezado['estacion']]
                    valor = str(data[encabezado['valor']]).replace(",", ".")
                    variable = Variable.objects.filter(nombre=nom_variable)
                    if len(variable) == 0:
                        raise ValueError('La variable no esta registrada.')
                    estacion = Estacion.objects.filter(codigo=cod_estacion)
                    if len(estacion) == 0:
                        raise ValueError('La estación no esta registrada.')

                    id = None
                    datosValidado = DatosValidados.objects.filter(fecha=data[encabezado['fecha']],
                                                                  id_variable=variable[0].id,
                                                                  id_estacion=estacion[0].id)
                    estadoMM = True
                    if not variable[0].nombre in variablesMM:
                        variablesMM[variable[0].nombre] = []
                    else:
                        for itemMM in variablesMM:
                            if itemMM == variable[0].nombre:
                                estadoMM = False
                                break
                    if estadoMM:
                        variablesMM[variable[0].nombre].append(estacion[0].nombre)

                    if len(datosValidado) > 0:
                        id = datosValidado[0].id
                    listaDatos.append(
                        {'id': id, 'fecha': data[encabezado['fecha']], "valor": float(valor),
                         "id_variable": variable[0].id, "id_estacion": estacion[0].id})
                i = i + 1

            nuevosDatos = []
            for item in listaDatos:
                serializer = self.serializer_class(data=item)
                if not item['id'] is None:
                    datoValidado = DatosValidados.objects.get(pk=item['id'])
                    serializer = self.serializer_class(datoValidado, data=item, partial=False)
                if serializer.is_valid():
                    serializer.save()
                    nuevosDatos.append(serializer.data)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            modeloClima = ModeloClima()
            # return Response({'detail': variablesMM}, status=status.HTTP_404_NOT_FOUND)
            for variable in variablesMM:
                for estacion in variablesMM[variable]:
                    modeloClima.guardarMediaMovil(variable, estacion)

            return Response({'msg': 'Datos guardados.'}, status=status.HTTP_201_CREATED)
        except ValueError as ex:
            return Response({'detail': str(ex)}, status=status.HTTP_404_NOT_FOUND)

    '''
    class ValoresMediaMovilD(GeneralModelViewSet):
        model_db = ValoresMediaMovil
        detail_rows_db = True  # Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
        queryset = ValoresMediaMovil.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
        serializer_class = DatosValidadosSerializer
    
        def list(self, request):
            return super().list(self, request)
            # return JsonResponse(CalculosDatosValidados.getDatosMensual('Precipitación', 'TUNGUAVITA - AUT'))
            # return JsonResponse(CalculosDatosValidados.getDatosMensual('Temperatura', 'TUNGUAVITA - AUT'))
    
            # return JsonResponse(CalculosDatosValidados.getArrayDatosMensual('Precipitación', 'TUNGUAVITA - AUT'))
            # return JsonResponse(CalculosDatosValidados.getArrayDatosMensual('Temperatura', 'TUNGUAVITA - AUT'))
    
            # return JsonResponse(CalculosDatosValidados.getMediaMovil('Precipitación', 'TUNGUAVITA - AUT'))
            # return JsonResponse(CalculosDatosValidados.getMediaMovil('Temperatura', 'TUNGUAVITA - AUT'))
    
            modeloClima = ModeloClima()
            # return JsonResponse(modeloClima.getAnomalias('Temperatura', 'TUNGUAVITA - AUT'))
            # return JsonResponse(modeloClima.getAnomalias('Precipitación', 'TUNGUAVITA - AUT'))
            # return JsonResponse(modeloClima.getAnomalias('ONI'))
    
            # return JsonResponse(modeloClima.getArrayAnomalias('Temperatura', 'TUNGUAVITA - AUT'))
            # return JsonResponse(modeloClima.getArrayAnomalias('Precipitación', 'TUNGUAVITA - AUT'))
            # return JsonResponse(modeloClima.getArrayAnomalias('ONI', None))
            # return Response(modeloClima.getCalcularMesesDist(2018, 2))
            return Response(modeloClima.getSeries(2012, 1))
    '''


class ModeloClimaViewSet(GeneralModelViewSet):

    @action(methods=['get'], detail=False)
    def resources(self, request):
        estaciones = Estacion.objects.all()
        estacionesData = []
        for item in estaciones:
            estacionesData.append({'id': item.id, 'nombre': item.nombre})
        return JsonResponse({'estaciones': estacionesData})

    @action(methods=['get'], detail=False)
    def datos_mesuales(self, request):
        nom_v = request.query_params['variable']
        nom_e = request.query_params['estacion']
        return JsonResponse(CalculosDatosValidados.getArrayDatosMensual(nom_variable=nom_v, nom_estacion=nom_e))

    @action(methods=['get'], detail=False)
    def media_movil(self, request):
        nom_v = request.query_params['variable']
        nom_e = request.query_params['estacion']
        return JsonResponse(CalculosDatosValidados.getMediaMovil(nom_variable=nom_v, nom_estacion=nom_e))

    @action(methods=['get'], detail=False)
    def anomalias(self, request):
        nom_v = request.query_params['variable']
        nom_e = None
        if 'estacion' in request.query_params:
            nom_e = request.query_params['estacion']
        modeloClima = ModeloClima()
        return JsonResponse(modeloClima.getArrayAnomalias(nom_variable=nom_v, nom_estacion=nom_e))

    @action(methods=['get'], detail=False)
    def series(self, request):
        try:
            y = int(request.query_params['year'])
            m = int(request.query_params['month'])
            id_estacion = int(request.query_params['id_estacion'])
            estacion = Estacion.objects.get(pk=id_estacion)
            periodo = 'mensual'
            if 'periodo' in request.query_params:
                periodo = request.query_params['periodo']
            modeloClima = ModeloClima()
            datosClima = modeloClima.getSeries(year_date=y, month_date=m, nom_estacion=estacion.nombre);
            series = datosClima['series']
            fechas = []
            for x in series:
                serie = series[x]
                fecha_inicio = date(serie['year_serie'], m, 1)
                fechas.append(fecha_inicio)

            fecha_inicio_simulacion = date(y, m, 1)
            fecha_fin_simulacion = fecha_inicio_simulacion + timedelta(days=364)
            simulacion = [fecha_inicio_simulacion.strftime('%Y-%m-%d'), fecha_fin_simulacion.strftime('%Y-%m-%d')]

            graficas = {
                'temp': CalculosDatosValidados.getGrafica(nom_variable='Temperatura',
                                                          nom_estacion=estacion.nombre, periodo=periodo,
                                                          fechas=fechas, simulacion=simulacion),
                'precip': CalculosDatosValidados.getGrafica(nom_variable='Precipitación',
                                                            nom_estacion=estacion.nombre, periodo=periodo,
                                                            fechas=fechas, simulacion=simulacion),
                'oni': CalculosDatosValidados.getGraficaONI(fechas=fechas)
            }

            return Response({'resultados': datosClima, 'graficas': graficas})
        except ValueError as ex:
            return Response({'detail': str(ex)}, status=status.HTTP_404_NOT_FOUND)

    @action(methods=['get'], detail=False)
    def graficas(self, request):
        periodo = request.query_params['periodo']
        m = int(request.query_params['mes'])
        id_estacion = int(request.query_params['id_estacion'])
        estacion = Estacion.objects.get(pk=id_estacion)
        graficas = []
        for x in range(0, 3):
            s = 'serie' + str(x + 1)
            if not s in request.query_params:
                graficas.append({'temp': 'No aplica', 'Precip': 'No aplica'})
            else:
                y = int(request.query_params[s])
                fecha_inicio = date(y, m, 1)
                graficas.append({
                    'temp': CalculosDatosValidados.getGrafica(nom_variable='Temperatura',
                                                              nom_estacion=estacion.nombre, periodo=periodo,
                                                              fechaInicio=fecha_inicio),
                    'precip': CalculosDatosValidados.getGrafica(nom_variable='Precipitación',
                                                                nom_estacion=estacion.nombre, periodo=periodo,
                                                                fechaInicio=fecha_inicio)
                })
        return Response({'graficas': graficas})
