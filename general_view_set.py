from rest_framework import status, viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.db import IntegrityError
from administracion_basica.permisos_api import PermisosApi

class GeneralModelViewSet(viewsets.ModelViewSet):
    model_db = None
    detail_rows_db = False
    seccion= ""

    def filterObjects(self, request):
        if (((self.model_db is None) or (not request.query_params)) and self.detail_rows_db is False):
            return self.model_db.objects.all()
        elif (self.detail_rows_db and (not request.query_params)):
            return []
        else:
            params = self.model_db.get_Filter(request.query_params)
            return self.model_db.objects.filter(**params)

    def list(self, request):
        if PermisosApi.apiAutorizacion(request.user, self.seccion,'consultar')==False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion

        queryset = self.filterObjects(request)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        if PermisosApi.apiAutorizacion(request.user, self.seccion, 'consultar') == False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion
        data = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(data)
        return Response(serializer.data)

    def create(self, request):
        if PermisosApi.apiAutorizacion(request.user, self.seccion, 'crear') == False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion
        serializer = self.serializer_class(data=request.data)  # request.datos son los datos de la petición
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo

            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def update(self, request, pk=None, *args, **kwargs):
        if PermisosApi.apiAutorizacion(request.user, self.seccion, 'modificar') == False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion
        partial = kwargs.pop('partial', False)
        data = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(data, data=request.data, partial=partial)
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def parcial_update(self, request, pk=None, *args, **kwargs):

        kwargs['partial'] = True
        return self.update(request, pk, *args, **kwargs)

    def destroy(self, request,pk=None):  # def destroy encargada de eliminar registro Unidad de medida en relacion con su id
        if PermisosApi.apiAutorizacion(request.user, self.seccion, 'eliminar') == False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion
        try:
            data = get_object_or_404(self.queryset, pk=pk)
            data.delete()
            return Response({"detail": "Registro Eliminado."}, status=status.HTTP_202_ACCEPTED)
        except IntegrityError as ide:
            return Response({"detail": "No se puede eliminar el regitro porque esta relacionado. "},
                            status=status.HTTP_409_CONFLICT)