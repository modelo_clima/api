from django.db import models
from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager)
from rest_framework_jwt.settings import api_settings
from configuraciones_basicas.models import Entidad

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


# Rol
# clase encargada de definir las propiedades de la tabla roles en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 30/01/2020
class Rol(models.Model):
    id = models.AutoField(primary_key=True)  # @param primarykey id
    nombre = models.CharField(max_length=100, blank=False, null=False)  # @param string nombre

    class Meta:
        db_table = 'roles'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'nombre'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


class UserManager(BaseUserManager):
    def create_user(self, email, username, first_name, last_name, id_rol, id_entidad, estado, password=None,
                    is_active=True, is_staff=False, is_admin=False):
        if not email:
            raise ValueError("ususario debe tener un email")
        if not password:
            raise ValueError("usuario debe tener una contraseña")
        if not username:
            raise ValueError("ususario debe tener un usernanme")
        if not first_name:
            raise ValueError("ususario debe tener un nombre")
        if not last_name:
            raise ValueError("ususario debe tener un apellido")

        user_obj = self.model(email=email)
        user_obj.set_password(password)  # cambio user password
        user_obj.staff = is_staff
        user_obj.username = username
        user_obj.first_name = first_name
        user_obj.last_name = last_name
        user_obj.id_rol = id_rol
        user_obj.id_entidad = id_entidad
        user_obj.estado = estado
        user_obj.admin = is_admin
        user_obj.active = is_active
        user_obj.save()
        return user_obj

    def create_staffuser(self, email, password=None):
        user = self.create_user(email, password=password, is_staff=True)
        return user

    # def create_superuser(self,email, username,first_name,last_name, password = None):
    #   user = self.create_user(email, username, first_name,last_name,id_rol=1,password=password,  is_staff=True, is_admin=True)
    #   return user
    def create(self, email, username, first_name, last_name, id_rol, id_entidad, estado, password=None):
        user = self.create_user(email, username, first_name, last_name, id_rol, id_entidad, estado, password=password,
                                is_staff=True, is_admin=True)
        return user


# User
# clase encargada de definir las propiedades de la tabla user en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 30/01/2020
class User(AbstractBaseUser):
    email = models.EmailField(max_length=255, unique=True, null=False, blank=False)  # @param string email
    username = models.CharField(max_length=255, unique=True, null=False, blank=False)  # @param string username
    first_name = models.CharField(max_length=255, blank=False, null=False)  # @param string first_name
    last_name = models.CharField(max_length=255, blank=False, null=False)  # @param string last_name
    id_rol = models.ForeignKey(Rol, to_field='id', db_column='id_rol', on_delete=models.DO_NOTHING,
                               default=1)  # @param foreignkey id_entidad
    id_entidad = models.ForeignKey(Entidad, to_field='id', db_column='id_entidad', on_delete=models.DO_NOTHING,
                                   default=1)  # @param foreignkey id_entidad
    estado = models.BooleanField(default=True)  # @param boolean estado
    active = models.BooleanField(default=True)  # @param boolean active
    staff = models.BooleanField(default=False)  # @param boolean staff
    admin = models.BooleanField(default=False)  # @param boolean admin
    timestamp = models.DateTimeField(auto_now_add=True)  # @param timestamp timestamp
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name']  # ['full_name'] #python manage.py createsuperuser
    objects = UserManager()

    class Meta:
        db_table = 'administracion_basica_user'  # vv@param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    @property
    def is_anonymous(self):
        return True

    @property
    def is_authenticated(self):
        return False

    def has_module_perms(self, app_label):
        return True

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'first_name', 'last_name', 'username', 'email', 'password', 'id_rol', 'id_entidad',
                 'estado'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active

    def getMenu(self):
        seccionesAppPrincipal = SeccionApp.objects.filter(id_seccion_app=None)
        menu = []
        for item in seccionesAppPrincipal:
            permisos = PermisoApp.objects.filter(id_seccion_app=item.id, id_rol=self.id_rol.id)
            if len(permisos) > 0 and permisos[0].consultar:
                urlApp = UrlsAppFrontend.objects.filter(id_seccion_app=item.id, menu=1)
                urlP = "/"
                if len(urlApp) > 0:
                    urlP = urlApp[0].url
                menuItem = {'nombre': item.nombre, 'url': urlP, 'submenu': []}
                seccionesAppSubs = SeccionApp.objects.filter(id_seccion_app=item.id)
                for itemSub in seccionesAppSubs:
                    permisos_sub = PermisoApp.objects.get(id_seccion_app=item.id, id_rol=self.id_rol.id)
                    if permisos_sub.consultar:
                        urlApp = UrlsAppFrontend.objects.filter(id_seccion_app=itemSub.id, menu=1)
                        url = "/"
                        if len(urlApp) > 0:
                            url = urlApp[0].url
                        menuItem['submenu'].append({'nombre': itemSub.nombre, 'url': url})
                if len(menuItem['submenu']) > 0 or len(urlApp) > 0:
                    menu.append(menuItem)
        return menu


# SeccionApp
# clase encargada de definir las propiedades de la tabla secciones_app en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class SeccionApp(models.Model):
    id = models.AutoField(primary_key=True)  # @param primarykey id
    nombre = models.CharField(max_length=45, blank=False, null=False)  # @param string nombre
    id_seccion_app = models.ForeignKey('self', db_column='id_seccion_app', on_delete=models.DO_NOTHING, blank=True,
                                       null=True)  # @param foreignkey id_seccion_app

    class Meta:
        # abstract = True
        db_table = 'secciones_app'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (
            ['id', 'nombre', 'id_seccion_app'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


class UrlsAppFrontend(models.Model):
    id = models.AutoField(primary_key=True)  # @param primarykey id
    url = models.CharField(max_length=100, blank=False, null=False)  # @param string nombre
    menu = models.IntegerField(blank=True, null=True)
    id_seccion_app = models.ForeignKey(SeccionApp, to_field='id', db_column='id_seccion_app',
                                       on_delete=models.DO_NOTHING)  # @param foreignkey id_seccion_app

    class Meta:
        # abstract = True
        db_table = 'urls_app_frontend'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (
            ['id', 'url', 'id_seccion_app'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data


# PermisoApp
# clase encargada de definir las propiedades de la tabla permisos_app en la base de datos
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class PermisoApp(models.Model):
    id = models.AutoField(primary_key=True)  # @param primarykey id
    consultar = models.BooleanField(default=False)  # @param boolean consultar
    crear = models.BooleanField(default=False)  # @param boolean crear
    modificar = models.BooleanField(default=False)  # @param boolean modificar
    eliminar = models.BooleanField(default=False)  # @param boolean eliminar
    id_rol = models.ForeignKey(Rol, to_field='id', db_column='id_rol',
                               on_delete=models.DO_NOTHING)  # @param foreignkey id_entidad
    id_seccion_app = models.ForeignKey(SeccionApp, to_field='id', db_column='id_seccion_app',
                                       on_delete=models.DO_NOTHING)  # @param foreignkey id_rol

    class Meta:
        db_table = 'permisos_app'  # @param string db_table almacena el nombre de la tabla dentro de la BD de Bio
        ordering = ['id']  # @param list ordernig almacena los campos por los cuales se quiere ordenar los registros

    @staticmethod
    def get_Atributos():  # @def get_Atributos contiene todos los campos que se utilizan en los serializer
        return (['id', 'consultar', 'crear', 'modificar', 'eliminar', 'id_rol',
                 'id_seccion_app'])  # @return list  con todos los campos que se utilizan en los serializer

    @staticmethod
    def get_Filter(params):
        data = {};
        for param in params:
            data[param + '__contains'] = params.get(param)
        return data
