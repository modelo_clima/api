import jwt
import uuid
import warnings

from django.contrib.auth import get_user_model

from calendar import timegm
from datetime import datetime

from rest_framework_jwt.compat import get_username
from rest_framework_jwt.compat import get_username_field
from rest_framework_jwt.settings import api_settings


def jwt_response_payload_handler(token, user=None, request=None):
    """
    Returns the response data for both the login and refresh views.
    Override to return a custom response such as including the
    serialized representation of the User.

    Example:

    def jwt_response_payload_handler(token, user=None, request=None):
        return {
            'token': token,
            'user': UserSerializer(user, context={'request': request}).data
        }

    """
    if not user.estado:
        return {'error': 'Ususario inactivo'}

    name = user.first_name
    if not user.last_name == '' and not user.last_name is None:
        name += ' ' + user.last_name
    return {
        'token': token,
        'name': name,
        'username': user.username,
        'id': user.id,
        'id_rol': user.id_rol.id,
        'menu': user.getMenu()
    }
