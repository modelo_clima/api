from administracion_basica.models import PermisoApp


# PermisosApi
# clase encargada de dar permiso a un rol para realizar una petición
# @Johan Mauricio Fonseca Molano y Brayan Hernan Castillo Rodriguez  @date 21/03/2020
class PermisosApi():
    @staticmethod
    def apiAutorizacion(user, seccion, permiso):
        #return True
        if (user.id is None):
            return False
        permisosApi = PermisoApp.objects.filter(id_rol=user.id_rol, id_seccion_app__nombre=seccion)
        if permisosApi is None:
            return False
        elif permiso == 'consultar':
            return permisosApi[0].consultar
        elif permiso == 'crear':
            return permisosApi[0].crear
        elif permiso == 'modificar':
            return permisosApi[0].modificar
        elif permiso == 'eliminar':
            return permisosApi[0].eliminar
        else:
            return False
