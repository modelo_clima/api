from .models import *
from .models import User
from rest_framework import serializers
# UserSerializer
# clase encargarda de serializar el modelo User
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = User.get_Atributos()
        extra_kwargs = {'password': {'required': False}}

# RolSerializer
# clase encargarda de serializar el modelo Rol
# @Brayan Hernan Castillo Rodriguez  @date 23/01/2020
class RolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rol
        fields = Rol.get_Atributos()

# SeccionAppSerializer
# clase encargarda de serializar el modelo SeccionApp
# @Brayan Hernan Castillo Rodriguez  @date 24/02/2020
class SeccionAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeccionApp
        fields = SeccionApp.get_Atributos()
        
# PermisoAppSerializer
# clase encargarda de serializar el modelo SeccionApp
# @Brayan Hernan Castillo Rodriguez  @date 24/02/2020
class PermisoAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = PermisoApp
        fields = PermisoApp.get_Atributos()


