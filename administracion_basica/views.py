from rest_framework.decorators import action

from general_view_set import GeneralModelViewSet
from .models import *
from .models import User
from django.contrib.auth.hashers import make_password
from .serializers import *
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework_jwt.views import ObtainJSONWebToken
from django.shortcuts import get_object_or_404
from administracion_basica.permisos_api import PermisosApi


class ObtainJSONWebTokenApi(ObtainJSONWebToken):
    model_db = User


# AuthUserViewSet
# clase encarga de generar la vista del modelo User
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class AuthUserViewSet(viewsets.ModelViewSet):
    model_db = User
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = User.objects.all()
    serializer_class = UserSerializer
    seccion = 'Usuarios'

    def list(self, request):
        if PermisosApi.apiAutorizacion(request.user, self.seccion, 'consultar') == False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion
        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        if PermisosApi.apiAutorizacion(request.user, self.seccion, 'crear') == False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion
        serializer = self.serializer_class(data=request.data)  # request.datos son los datos de la petición
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            serializer.save()  # .save() guarda el registro en el modulo

            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def update(self, request, pk=None, *args, **kwargs):
        if PermisosApi.apiAutorizacion(request.user, self.seccion, 'modificar') == False:
            return Response({"detail": "no tiene permisos"},
                            status=status.HTTP_401_UNAUTHORIZED)  # return Response(data) devuelve un error durante ejecucion
        partial = kwargs.pop('partial', False)
        data = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(data, data=request.data, partial=partial)
        if serializer.is_valid():  # .is_valid() valida todos los campos del serializable
            if 'password' in request.data:
                serializer.save(password=make_password(request.data["password"]))
            else:
                serializer.save()

            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)  # return Response(data) devuelve los datos guardados, ademas un status HTTP201 creacion en UnidadMedida
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)  # return Response(data) devuelve un error durante ejecucion

    def parcial_update(self, request, pk=None, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, pk, *args, **kwargs)

    @action(methods=['get'], detail=False)
    def existe_username(self, request):
        username = request.query_params["username"]
        respuesta = User.objects.filter(username= username)
        if len(respuesta)>0:
            return  Response(True)
        else:
            return  Response(False)

    @action(methods=['get'], detail=False)
    def existe_email(self, request):
        email = request.query_params["email"]
        respuesta = User.objects.filter(email=email)
        if len(respuesta) > 0:
            return Response(True)
        else:
            return Response(False)


# RolViewSet
# clase encarga de generar la vista del modelo roles
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class RolViewSet(GeneralModelViewSet):
    model_db = Rol
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = Rol.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo UnidadMedida ordenado por nombre
    serializer_class = RolSerializer
    seccion = 'Roles'


# SeccionAppViewSet
# clase encarga de generar la vista del modelo secciones_app
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class SeccionAppViewSet(GeneralModelViewSet):
    model_db = SeccionApp
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = SeccionApp.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo permisoapp ordenado por nombre
    serializer_class = SeccionAppSerializer
    seccion = 'Seccion App'

    @action(methods=['get'], detail=False)
    def permisosmenu(self, request):
        menus = SeccionApp.objects.filter(id_seccion_app=None)
        datosMenu = SeccionAppSerializer(menus, many=True).data
        for i in range(0, len(datosMenu)):
            permisos = PermisoApp.objects.filter(id_seccion_app=datosMenu[i]['id'], id_rol=request.user.id_rol.id)
            datosMenu[i]['permisos'] = {'consultar': permisos[0].consultar}
            submenus = SeccionApp.objects.filter(id_seccion_app=datosMenu[i]['id'])
            datosMenu[i]['submenus'] = SeccionAppSerializer(submenus, many=True).data
            for x in range(0, len(datosMenu[i]['submenus'])):
                item = datosMenu[i]['submenus'][x]
                permisos_sub = PermisoApp.objects.filter(id_seccion_app=item['id'], id_rol=request.user.id_rol.id)
                datosMenu[i]['submenus'][x]['permisos'] = {'consultar': permisos_sub[0].consultar}

        return Response(datosMenu)


# PermisoAppViewSet
# clase encarga de generar la vista del modelo PermisoApp
# @Brayan Hernan Castillo Rodriguez  @date 30/02/2020
class PermisoAppViewSet(GeneralModelViewSet):
    model_db = PermisoApp
    # detail_rows_db = True #Se habilita cuando la tabla es un detalle (Estructura maestro-detalle)
    queryset = PermisoApp.objects.all()  # @param queryset encargarda de almacenar todos los registros del modulo permisoapp ordenado por nombre
    serializer_class = PermisoAppSerializer
    seccion = 'Permisos App'

    @action(methods=['get'], detail=False)
    def permisosUrl(self, request):
        code_error_user = status.HTTP_401_UNAUTHORIZED
        try:
            user = request.user
            if user.id is None:
                code_error_user = status.HTTP_423_LOCKED
                raise ValueError('Debe iniciar sesión.')
            else:
                userModel = User.objects.get(pk=user.id)
                if not userModel.estado:
                    code_error_user = status.HTTP_423_LOCKED
                    raise ValueError('Usuario inactivo.')
            url = request.query_params['url']
            nom_permiso = None
            if 'permiso' in request.query_params:
                nom_permiso = request.query_params['permiso']
            estado = False
            urlApp = UrlsAppFrontend.objects.get(url=url)
            if not urlApp is None:
                permisos = PermisoApp.objects.filter(id_seccion_app=urlApp.id_seccion_app, id_rol=user.id_rol.id)
                permisoSerializer = PermisoAppSerializer(permisos, many=True).data
                if len(permisoSerializer) > 0:
                    if nom_permiso is None or nom_permiso == 'all':
                        return Response(
                            {'consultar': permisoSerializer[0]['consultar'], 'crear': permisoSerializer[0]['crear'],
                             'modificar': permisoSerializer[0]['modificar'],
                             'eliminar': permisoSerializer[0]['eliminar']})
                    elif permisoSerializer[0]['consultar']:
                        estado = permisoSerializer[0][nom_permiso]
            return Response({'estado': estado})
        except ValueError as ex:
            return Response({'detail': str(ex)}, status=code_error_user)
        except Exception as ex:
            return Response({"detail": "Se presentó un error."}, status=status.HTTP_404_NOT_FOUND)
