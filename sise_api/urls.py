"""sise_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from django.urls import include, path
from configuraciones_basicas.views import *
from administracion_basica.views import *
from mesa_tecnica.views import *
from administracion_basica.views import *
from rest_framework import routers
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token
#from administracion_basica.views import ObtainJSONWebTokenApi

from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.conf import settings

from configuraciones_basicas.views import *
from administracion_basica.views import *
from mesa_tecnica.views import *
from algoritmos.views import *

from rest_framework import routers
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

router = DefaultRouter()



################         configuración basica        #####################################
#Ruta unidadmedida
router.register(r'unidadmedida', UnidadMedidaViewSet, basename='unidadmedida')
#Ruta tipouso
router.register(r'tipouso', TipoUsoViewSet, basename='tipouso')
#Ruta microcuenca
router.register(r'microcuenca', MicroCuencaViewSet, basename='microcuenca')
#Ruta tipovariable
router.register(r'tipovariable', TipoVariableViewSet, basename='tipovariable')
#Ruta tecnologiaestacion
router.register(r'tecnologiaestacion', TecnologiaEstacionViewSet, basename='tecnologiaestacion')
#Ruta categoriaestacion
router.register(r'categoriaestacion', CategoriaEstacionViewSet, basename='categoriaestacion')
#Ruta puntogestion
router.register(r'puntogestion', PuntoGestionViewSet, basename='puntogestion')
#Ruta modelo
router.register(r'modelo', ModeloViewSet, basename='modelo')
#Ruta categoriaserie
router.register(r'categoriaserie', CategoriaSerieViewSet, basename='categoriaserie')
#Ruta limite
router.register(r'limite', LimiteViewSet, basename='limite')
#Ruta variable
router.register(r'variable', VariableViewSet, basename='variable')
#Ruta estacion
router.register(r'estacion', EstacionViewSet, basename='estacion')
#Ruta variablereglaoperacion
router.register(r'variablereglaoperacion', VariableReglaOperacionViewSet,basename='variablereglaoperacion')
#Ruta seccion
router.register(r'seccion', SeccionViewSet, basename='seccion')
#Ruta tipocaudal
router.register(r'tipocaudal', TipoCaudalViewSet, basename='tipocaudal')
#Ruta estado
router.register(r'estado', EstadoViewSet, basename='estado')

#Ruta municipio
router.register(r'municipio', MunicipioViewSet, basename='municipio')


#############################administracion Basica#####################################
#Ruta authusuario
router.register(r'authusuario',AuthUserViewSet, basename='authusuario')
#Ruta entidad
router.register(r'entidad', EntidadViewSet, basename='entidad')
#Ruta rol
router.register(r'rol', RolViewSet, basename='rol')
#Ruta permisoapp
router.register(r'permisoapp', PermisoAppViewSet, basename='permisoapp')
#Ruta seccionapp
router.register(r'seccionapp', SeccionAppViewSet, basename='seccionapp')

####################################### MESAS TECNICAS ########################################
# Ruta TipoMesaTecnica
router.register(r'tipomesatecnica', TipoMesaTecnicaViewSet, basename='tipomesatecnica')
# Ruta MesaTecnica
router.register(r'mesatecnica', MesaTecnicaViewSet, basename='mesatecnica')
# Ruta Escenarios
router.register(r'escenario', EscenarioViewSet, basename='escenario')
# Ruta VariableReglaOperacion
router.register(r'variablereglaoperacion', VariableReglaOperacionViewSet, basename='variablereglaoperacion')
# Ruta ValoresReglasOperacion
router.register(r'valoresreglasoperacion', ValoresReglasOperacionViewSet, basename='valoresreglasoperacion')
# Ruta CodigoReglaOperacion
router.register(r'codigoreglasoperacion', CodigoReglasOperacionViewSet, basename='codigoreglasoperacion')
# Ruta ReglaOperacionEscenario
router.register(r'reglasoperacionescenario', ReglasOperacionEscenarioViewSet, basename='reglasoperacionescenario')
# Ruta Actas
router.register(r'actasmesatecnica', ActasMesaTecnicaViewSet, basename='actasmesatecnica')
# Ruta Descargar Actas
router.register(r'downloadacta', DownloadActasViewSet, basename='downloadacta')
# router.register(r'reglaoperacionescenario',ReglaOperacionEscenarioViewSet,basename='reglaoperacionescenario')
# router.register(r'escenario',              EscenarioViewSet,              basename='escenario')

# urlpatterns = router.urls

######################### Modelo OSI ###############################
router.register(r'valorespotenciales', ValoresPotencialViewSet, basename='valorespotenciales')
router.register(r'outputvaloresras', OutputValoresRasViewSet, basename='outputvaloresras')
router.register(r'valoresmediamovil', ValoresMediaMovilViewSet, basename='valoresmediamovil')
router.register(r'datosvalidados', DatosValidadosViewSet, basename='datosvalidados')
router.register(r'modelo_clima', ModeloClimaViewSet, basename='modelo_clima')
router.register(r'informeuno', InformeUnoViewSet, basename='informeuno')
router.register(r'informedos', InformeDosViewSet, basename='informedos')
router.register(r'informeuno', InformeTresViewSet, basename='informetres')
router.register(r'valoresOsi', ValoresOSIViewSet, basename='valoresOsi')

urlpatterns = [

    path('admin/', admin.site.urls),
    path('api/token/', obtain_jwt_token),
    path('api/token/verify/', verify_jwt_token),
    path('api/token/refresh/', refresh_jwt_token),
    path('api/', include(router.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
